<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/site');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/admin');
Yii::setAlias('@clinic', dirname(dirname(__DIR__)) . '/clinic');
Yii::setAlias('@cms', dirname(dirname(__DIR__)) . '/cms');
Yii::setAlias('@employee', dirname(dirname(__DIR__)) . '/employee');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
//Yii::setAlias('@rootWeb','http://'.$_SERVER['SERVER_NAME']. ':/berobat/');
Yii::setAlias('@rootWeb','http://'.$_SERVER['SERVER_NAME']. ':');
Yii::setAlias('@picfrontend','@rootWeb/site/web');
Yii::setAlias('@uploadsFolder','@frontend/web/uploads');
Yii::setAlias('@uploadsWeb','@rootWeb/site/web/uploads');
