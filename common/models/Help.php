<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "help".
 *
 * @property integer $id
 * @property string $description
 * @property string $gambar
 * @property string $description2
 */
class Help extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description', 'gambar', 'description2'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'gambar' => 'Gambar',
            'description2' => 'Description2',
        ];
    }
}
