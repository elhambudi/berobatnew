<?php

namespace common\models;

use Yii;
use yii\base\Model;

class ChangePassword extends Model{
    public $oldpass;
    public $newpass;
    public $repeatnewpass;
    
    public function rules(){
        return [
            [['oldpass','newpass','repeatnewpass'],'required'],
            ['oldpass','findPasswords'],
            ['repeatnewpass','compare','compareAttribute'=>'newpass',],
        ];
    }
    
    public function findPasswords($attribute, $params){
        $user = User::findOne(Yii::$app->user->identity->id);
        if( !$user->validatePassword($this->oldpass) )
            $this->addError($attribute,'Password lama tidak sama');
    }
    
    public function attributeLabels(){
        return [
            'oldpass'=>'Password lama',
            'newpass'=>'Password baru',
            'repeatnewpass'=>'Ulangi Password',
        ];
    }
}

