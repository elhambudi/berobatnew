<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "logo_header".
 *
 * @property integer $id
 * @property string $gambar
 */
class LogoHeader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logo_header';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gambar'], 'required'],
            [['gambar'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gambar' => 'Gambar',
        ];
    }
}
