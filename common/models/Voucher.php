<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "voucher".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $min_can_be_purchased
 * @property string $real_price
 * @property string $voucher_price
 * @property string $discount
 * @property string $expire_date
 * @property string $image
 * @property integer $stock
 * @property integer $status_all_office
 * @property string $created_date
 * @property integer $status
 * @property integer $category_id
 * @property integer $clinic_id
 *
 * @property OrderVoucherHasVoucher[] $orderVoucherHasVouchers
 * @property OrderVoucher[] $orderVouchers
 * @property Category $category
 * @property VoucherCode[] $voucherCodes
 * @property VoucherHasOffice[] $voucherHasOffices
 * @property Office[] $offices
 */
class Voucher extends \yii\db\ActiveRecord
{

    public $imageUploaded;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voucher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'min_can_be_purchased', 'real_price', 'voucher_price', 'discount', 'expire_date', 'created_date', 'category_id', 'clinic_id'], 'required'],
            [['description', 'image'], 'string'],
            [['min_can_be_purchased', 'stock', 'status_all_office', 'status', 'category_id', 'clinic_id'], 'integer'],
            [['expire_date', 'created_date'], 'safe'],
            [['name'], 'string', 'max' => 100],
            //[['imageUploaded'],'file','skipOnEmpty' => !$this->isNewRecord],
            [['real_price', 'voucher_price'], 'string', 'max' => 11],
            [['discount'], 'string', 'max' => 45],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'min_can_be_purchased' => 'Min Can Be Purchased',
            'real_price' => 'Real Price',
            'voucher_price' => 'Voucher Price',
            'discount' => 'Discount',
            'expire_date' => 'Expire Date',
            'image' => 'Image',
            'stock' => 'Stock',
            'status_all_office' => 'Status All Office',
            'created_date' => 'Created Date',
            'status' => 'Status',
            'category_id' => 'Category ID',
            'clinic_id' => 'Clinic ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderVoucherHasVouchers()
    {
        return $this->hasMany(OrderVoucherHasVoucher::className(), ['voucher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderVouchers()
    {
        return $this->hasMany(OrderVoucher::className(), ['id' => 'order_voucher_id'])->viaTable('order_voucher_has_voucher', ['voucher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClinic()
    {
        return $this->hasOne(Clinic::className(), ['id' => 'clinic_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoucherCodes()
    {
        return $this->hasMany(VoucherCode::className(), ['voucher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoucherHasOffices()
    {
        return $this->hasMany(VoucherHasOffice::className(), ['voucher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffices()
    {
        return $this->hasMany(Office::className(), ['id' => 'office_id'])->viaTable('voucher_has_office', ['voucher_id' => 'id']);
    }

    public function upload(){
        $this->imageUploaded = \yii\web\UploadedFile::getInstance($this, 'imageUploaded');
        if( !is_null($this->imageUploaded) ){
            $path = Yii::getAlias('@uploadsFolder');
            $name = uniqid().time() . '.' . $this->imageUploaded->extension;
            $this->imageUploaded->saveAs($path.'/' .$name);
            $this->image = $name;
        }
    }
}
