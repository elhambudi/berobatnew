<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "about_us".
 *
 * @property integer $id
 * @property string $description
 * @property string $gambar
 * @property string $description2
 */
class AboutUs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_us';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description', 'gambar', 'description2'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'gambar' => 'Gambar',
            'description2' => 'Description2',
        ];
    }
}
