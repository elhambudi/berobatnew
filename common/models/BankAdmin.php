<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bank_admin".
 *
 * @property integer $id
 * @property string $bank
 * @property string $name
 * @property string $no_rekening
 */
class BankAdmin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank', 'name', 'no_rekening'], 'required'],
            [['bank', 'name'], 'string'],
            [['no_rekening'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank' => 'Bank',
            'name' => 'Name',
            'no_rekening' => 'No Rekening',
        ];
    }
}
