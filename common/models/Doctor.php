<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "doctor".
 *
 * @property integer $id
 * @property string $name
 * @property string $photo
 * @property string $bio
 * @property string $email
 * @property string $phone
 * @property string $proof_of_permission
 * @property string $education
 * @property string $experience
 * @property integer $clinic_id
 *
 * @property CategoryHasDoctor[] $categoryHasDoctors
 * @property Category[] $categories
 * @property Clinic $clinic
 */
class Doctor extends \yii\db\ActiveRecord
{
    public $foto;
    public $proofOfDoctor;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doctor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'clinic_id','bio','education'], 'required'],
            [['photo', 'bio', 'proof_of_permission', 'education', 'experience'], 'string'],
            [['clinic_id'], 'integer'],
            [['foto','proofOfDoctor'],'file','skipOnEmpty' => !$this->isNewRecord],
            [['name', 'email'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
            [['clinic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clinic::className(), 'targetAttribute' => ['clinic_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'photo' => 'Photo',
            'bio' => 'Bio',
            'email' => 'Email',
            'phone' => 'Phone',
            'proof_of_permission' => 'Proof Of Permission',
            'education' => 'Education',
            'experience' => 'Experience',
            'clinic_id' => 'Clinic ID',
            'foto' => 'Foto',
            'proofOfDoctor' => 'Bukti Dokter' 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryHasDoctors()
    {
        return $this->hasMany(CategoryHasDoctor::className(), ['doctor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('category_has_doctor', ['doctor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClinic()
    {
        return $this->hasOne(Clinic::className(), ['id' => 'clinic_id']);
    }


    public function upload()
    {
        $file  =  ['foto' => 'photo','proofOfDoctor' => 'proof_of_permission'];
        foreach($file as $k => $v){
            $this->$k = \yii\web\UploadedFile::getInstance($this, $k);
            if( !is_null($this->$k) ){
                $path = Yii::getAlias('@uploadsFolder');
                $name = uniqid().time() . '.' . $this->$k->extension;
                $this->$k->saveAs($path.'/' .$name);
                $this->$v = $name;
            }
        }
    }
}
