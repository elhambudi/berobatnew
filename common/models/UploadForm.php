<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $uploadedFile;

    public function rules()
    {
        return [
            [['uploadedFile'], 'file', 'skipOnEmpty' => false],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $path = Yii::getAlias('@uploadsFolder');
            $name = uniqid().time() . '.' . $this->uploadedFile->extension;
            $this->uploadedFile->saveAs($path.'/' .$name);
            return $name;
        } else {
            return false;
        }
    }
}