<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "benefit".
 *
 * @property integer $id
 * @property string $judul
 * @property string $description
 * @property string $gambar
 */
class Benefit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'benefit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'description', 'gambar'], 'required'],
            [['judul', 'description', 'gambar'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'description' => 'Description',
            'gambar' => 'Gambar',
        ];
    }
}
