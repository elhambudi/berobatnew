<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Clinic;

/**
 * ClinicSearch represents the model behind the search form about `common\models\Clinic`.
 */
class ClinicSearch extends Clinic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_approval', 'user_id'], 'integer'],
            [['name_contact', 'name_clinic', 'phone', 'logo', 'npwp', 'created_date', 'description', 'proof_of_permission', 'working_hour_start', 'working_hour_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clinic::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status_approval' => $this->status_approval,
            'created_date' => $this->created_date,
            'user_id' => $this->user_id,
            'working_hour_start' => $this->working_hour_start,
            'working_hour_end' => $this->working_hour_end,
        ]);

        $query->andFilterWhere(['like', 'name_contact', $this->name_contact])
            ->andFilterWhere(['like', 'name_clinic', $this->name_clinic])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'npwp', $this->npwp])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'proof_of_permission', $this->proof_of_permission]);

        return $dataProvider;
    }
}