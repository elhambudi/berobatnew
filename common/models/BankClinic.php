<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bank_clinic".
 *
 * @property integer $id
 * @property string $name
 * @property string $number
 * @property integer $bank_id
 * @property integer $clinic_id
 *
 * @property Bank $bank
 * @property Clinic $clinic
 */
class BankClinic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_clinic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'number', 'bank_id', 'clinic_id'], 'required'],
            [['bank_id', 'clinic_id'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['number'], 'string', 'max' => 25],
            [['bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::className(), 'targetAttribute' => ['bank_id' => 'id']],
            [['clinic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clinic::className(), 'targetAttribute' => ['clinic_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'number' => 'Number',
            'bank_id' => 'Bank ID',
            'clinic_id' => 'Clinic ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(Bank::className(), ['id' => 'bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClinic()
    {
        return $this->hasOne(Clinic::className(), ['id' => 'clinic_id']);
    }
}
