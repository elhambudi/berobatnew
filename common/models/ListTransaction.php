<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "list_transaction".
 *
 * @property integer $id
 * @property string $kode_transaksi
 * @property integer $id_user
 * @property integer $id_voucher
 * @property string $kode_pembayaran
 *
 * @property User $idUser
 * @property OrderVoucher $idVoucher
 */
class ListTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_transaksi', 'id_user', 'id_voucher'], 'required'],
            [['id_user', 'id_voucher'], 'integer'],
            [['kode_pembayaran'], 'string'],
            [['kode_transaksi'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_voucher'], 'exist', 'skipOnError' => true, 'targetClass' => OrderVoucher::className(), 'targetAttribute' => ['id_voucher' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_transaksi' => 'Kode Transaksi',
            'id_user' => 'Id User',
            'id_voucher' => 'Id Voucher',
            'kode_pembayaran' => 'Kode Pembayaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVoucher()
    {
        return $this->hasOne(OrderVoucher::className(), ['id' => 'id_voucher']);
    }
}
