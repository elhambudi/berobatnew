<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "clinic_photo".
 *
 * @property integer $id
 * @property string $url
 * @property integer $clinic_id
 *
 * @property Clinic $clinic
 */
class ClinicPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clinic_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'clinic_id'], 'required'],
            [['url'], 'string'],
            [['clinic_id'], 'integer'],
            [['clinic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clinic::className(), 'targetAttribute' => ['clinic_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'clinic_id' => 'Clinic ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClinic()
    {
        return $this->hasOne(Clinic::className(), ['id' => 'clinic_id']);
    }
}
