<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "footer".
 *
 * @property integer $id
 * @property string $deskripsi
 * @property string $alamat
 * @property string $email
 * @property string $telepon
 * @property string $fax
 */
class Footer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'footer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deskripsi', 'alamat', 'email', 'telepon'], 'required'],
            [['deskripsi', 'alamat', 'email', 'telepon', 'fax'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deskripsi' => 'Deskripsi',
            'alamat' => 'Alamat',
            'email' => 'Email',
            'telepon' => 'Telepon',
            'fax' => 'Fax',
        ];
    }
}
