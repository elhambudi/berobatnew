<?php

namespace common\models;

class Constant{
    const ROLE_ADMIN =  1;
    const ROLE_CMS =  2;
    const ROLE_CLINIC =  3;
    const ROLE_EMPLOYEE =  4;
    const ROLE_MEMBER =  5;

    const STATUS_ACTIVE = 1;
    const STATUS_NONACTIVE = 0;
    
    const STEP_APOTIK_PROFILE = 1;
    const STEP_BANK = 2;    
    const STEP_DOCTOR = 3;
    const STEP_OFFICE = 4;

    const ERROR_STEP_APOTIK_PROFILE = "Ayo lengkapi profil Apotikmu";
    const ERROR_STEP_BANK = "Tambahkan setidaknya 1 bank";
    const ERROR_STEP_DOCTOR = "Tambahkan setidaknya 1 dokter";
    const ERROR_STEP_OFFICE = "Tambahkan setidaknya 1 lokasi apotik";
    
}