<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_voucher".
 *
 * @property integer $id
 * @property string $code
 * @property string $grand_total
 * @property integer $status
 * @property integer $status_validate
 * @property integer $user_id
 * @property integer $voucher_id
 * @property string $created_date
 * @property string $validate_date
 *
 * @property ListTransaction[] $listTransactions
 * @property User $user
 * @property Voucher $voucher
 * @property OrderVoucherHasVoucher[] $orderVoucherHasVouchers
 * @property Voucher[] $vouchers
 * @property VoucherCode[] $voucherCodes
 */
class OrderVoucher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_voucher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'status_validate', 'user_id', 'voucher_id'], 'integer'],
            [['status_validate', 'user_id', 'voucher_id', 'created_date'], 'required'],
            [['created_date', 'validate_date'], 'safe'],
            [['code'], 'string', 'max' => 255],
            [['grand_total'], 'string', 'max' => 11],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['voucher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Voucher::className(), 'targetAttribute' => ['voucher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'grand_total' => 'Grand Total',
            'status' => 'Status',
            'status_validate' => 'Status Validate',
            'user_id' => 'User ID',
            'voucher_id' => 'Voucher ID',
            'created_date' => 'Created Date',
            'validate_date' => 'Validate Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListTransactions()
    {
        return $this->hasMany(ListTransaction::className(), ['id_voucher' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoucher()
    {
        return $this->hasOne(Voucher::className(), ['id' => 'voucher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderVoucherHasVouchers()
    {
        return $this->hasMany(OrderVoucherHasVoucher::className(), ['order_voucher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVouchers()
    {
        return $this->hasMany(Voucher::className(), ['id' => 'voucher_id'])->viaTable('order_voucher_has_voucher', ['order_voucher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoucherCodes()
    {
        return $this->hasMany(VoucherCode::className(), ['order_voucher_id' => 'id']);
    }
}
