<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "clinic".
 *
 * @property integer $id
 * @property string $name_contact
 * @property string $name_clinic
 * @property string $phone
 * @property string $logo
 * @property string $npwp
 * @property integer $status_approval
 * @property string $created_date
 * @property integer $user_id
 * @property string $description
 * @property string $proof_of_permission
 * @property string $working_hour_start
 * @property string $working_hour_end
 *
 * @property BankClinic[] $bankClinics
 * @property CategoryHasClinic[] $categoryHasClinics
 * @property Category[] $categories
 * @property User $user
 * @property ClinicPhoto[] $clinicPhotos
 * @property Doctor[] $doctors
 * @property Office[] $offices
 */
class Clinic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clinic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_contact', 'name_clinic', 'phone', 'created_date', 'user_id'], 'required'],
            [['logo', 'description', 'proof_of_permission'], 'string'],
            [['status_approval', 'user_id'], 'integer'],
            [['created_date', 'working_hour_start', 'working_hour_end'], 'safe'],
            [['name_contact', 'name_clinic', 'npwp'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_contact' => 'Name Contact',
            'name_clinic' => 'Name Clinic',
            'phone' => 'Phone',
            'logo' => 'Logo',
            'npwp' => 'Npwp',
            'status_approval' => 'Status Approval',
            'created_date' => 'Created Date',
            'user_id' => 'User ID',
            'description' => 'Description',
            'proof_of_permission' => 'Proof Of Permission',
            'working_hour_start' => 'Working Hour Start',
            'working_hour_end' => 'Working Hour End',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankClinics()
    {
        return $this->hasMany(BankClinic::className(), ['clinic_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryHasClinics()
    {
        return $this->hasMany(CategoryHasClinic::className(), ['clinic_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('category_has_clinic', ['clinic_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClinicPhotos()
    {
        return $this->hasMany(ClinicPhoto::className(), ['clinic_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctors()
    {
        return $this->hasMany(Doctor::className(), ['clinic_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffices()
    {
        return $this->hasMany(Office::className(), ['clinic_id' => 'id']);
    }
}
