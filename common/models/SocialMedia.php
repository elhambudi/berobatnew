<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "social_media".
 *
 * @property integer $id
 * @property string $facebook
 * @property string $instagram
 * @property string $twitter
 * @property string $youtube
 */
class SocialMedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social_media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['facebook', 'instagram', 'twitter', 'youtube'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'twitter' => 'Twitter',
            'youtube' => 'Youtube',
        ];
    }
}
