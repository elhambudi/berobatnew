<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "program_clinic".
 *
 * @property integer $id
 * @property string $judul
 * @property string $description
 */
class ProgramClinic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program_clinic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'description'], 'required'],
            [['judul', 'description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'description' => 'Description',
        ];
    }
}
