<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kebijakan_privasi".
 *
 * @property integer $id
 * @property string $description
 * @property string $gambar
 * @property string $description2
 */
class KebijakanPrivasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kebijakan_privasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'description'], 'required'],
            [['id'], 'integer'],
            [['description', 'gambar', 'description2'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'gambar' => 'Gambar',
            'description2' => 'Description2',
        ];
    }
}
