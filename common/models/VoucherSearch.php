<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Voucher;

/**
 * VoucherSearch represents the model behind the search form about `common\models\Voucher`.
 */
class VoucherSearch extends Voucher
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'min_can_be_purchased', 'stock', 'status_all_office', 'status', 'category_id', 'clinic_id'], 'integer'],
            [['name', 'description', 'real_price', 'voucher_price', 'discount', 'expire_date', 'image', 'created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Voucher::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'min_can_be_purchased' => $this->min_can_be_purchased,
            'expire_date' => $this->expire_date,
            'stock' => $this->stock,
            'status_all_office' => $this->status_all_office,
            'created_date' => $this->created_date,
            'status' => $this->status,
            'category_id' => $this->category_id,
            'clinic_id' => $this->clinic_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'real_price', $this->real_price])
            ->andFilterWhere(['like', 'voucher_price', $this->voucher_price])
            ->andFilterWhere(['like', 'discount', $this->discount])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
