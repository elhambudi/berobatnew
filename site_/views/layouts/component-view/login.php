<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

?>

<!-- Modal -->
<div class="modal fade" id="login-panel" tabindex="-1" role="dialog" aria-labelledby="lr-panel-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="lr-title">
			<div class="lr-title__big">Login</div>
			<div class="lr-title__desc">
				Belum memiliki akun? Silakan <a href="#" class="iframe-register">daftar</a>, Gratis!
			</div>
		</div>
      </div>
      <div class="modal-body">
        <div class="lr-box">
			<div class="lr-input">
				<?php $form = ActiveForm::begin([
						'action' => Url::to(['site/loginmember']),
						'method' => 'POST'
				]); ?>
				<div class="form-group">
					<div class="input-group">
					  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
					  	<input type="text" name="LoginForm[username]" class="form-control" placeholder="Email Anda" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  	<span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock"></i></span>
					  	<input type="password" name="LoginForm[password]" class="form-control" placeholder="Password Anda" aria-describedby="basic-addon2">
					</div>
				</div>
				<div class="lr-input__desc">
					<div class="pull-right">
						<a href="#">Forgot Password</a>
					</div>
					<div class="clear"></div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-full">Login</button>
				</div>
				<?php ActiveForm::end(); ?> 
			</div>
		</div>
		<!--<div class="sosmed-way">
			<div class="seperator">
				<span class="seperator__text">ATAU</span>
			</div>
			<div class="sosmed-way__place">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<a class="btn btn-default">
							<img alt="Facebook Icon" class="btn__farleft-icon" height="24" src="<?= Yii::$app->request->baseUrl ?>/frontend/web/images/fb-logo.png" width="24">
							Login dengan Facebook
						</a>
					</div>
					<div class="col-md-6 col-sm-12">
						<a class="btn btn-default">
							<img alt="Facebook Icon" class="btn__farleft-icon" height="24" src="<?= Yii::$app->request->baseUrl ?>/frontend/web/images/google-logo.png" width="24">
							Login dengan Google
						</a>
					</div>
				</div>
			</div>
		</div>-->
      </div>
    </div>
  </div>
</div>