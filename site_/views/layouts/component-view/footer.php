<?php

use yii\widgets\Menu;
$footer = \common\models\Footer::findOne(1);
$p = \common\models\SocialMedia::findOne(1);
?>

<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-6 about">
                <div class="logo"><img src="<?= Yii::$app->request->baseUrl ?>/site/web/images/logo.png"></div>
                <p><?=$footer->deskripsi?></p>
            </div>
            <div class="col-md-3 col-sm-6 list-categories">
                <?php 
                   // $list = ['Dermatologist','Gynecologist','Dentist','Ear-Nose-Throat','Orthopedist','Pediatrician','General Surgeon','Urologist','Cardiologist','General Physician'];
                    $data = \common\models\Category::find()->orderBy(['name' => SORT_ASC])->all();
                    foreach($data as $d){
                        $list[] = $d['name'];
                    }
                    $totalList = count($list);
                    $totalRow = $totalList/2;
                ?>
                <ul>
                    <?php 
                    $item = 0;
                    for ($i=0; $i < $totalRow; $i++): 
                    ?>
                    <li>
                        <div class="row">
                            <?php for ($j=0; $j < 2; $j++): ?>
                            <div class="col-sm-6">
                                <a href="#"><?= $list[$item] ?></a>
                            </div>
                            <?php 
                            $item++;
                            if($item >= $totalList) break;
                            endfor; 
                            ?>
                        </div>
                    </li>
                    <?php endfor; ?>
                </ul>
            </div>
            <div class="col-md-2 col-sm-6 company-menu">
                <?php
                echo Menu::widget([
                    'items' => [
                        ['label' => 'About Us', 'url' => ['site/aboutus']],
                        ['label' => '<strong>Join as Partner</strong>', 'url' => ['site/become-clinic-partner']],
                        ['label' => 'Blog', 'url' => ['site/blog']],
                        ['label' => 'Help', 'url' => ['site/help']],
                        ['label' => 'FAQ', 'url' => ['site/faq']],
                        ['label' => 'Privacy Policy', 'url' => ['site/privacypolicy']],
                    ],
                    'encodeLabels' => false,
                ]);
                ?>
            </div>
            <div class="col-md-4 col-sm-6 location-contact">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="location"><?=$footer->alamat?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 the-contact">
                        
                            <div class="item">
                                <a href="mailto:ask@berobat.id"><i class="fa fa-envelope"></i><?=$footer->email?></a>
                            </div>
                            <div class="item">
                                <a href="tel:0822 3102 3315"><i class="fa fa-phone"></i><?=$footer->telepon?></a>
                            </div>
                        
                    </div>
                    <div class="col-md-6 the-sosmed">
                        <a target="_blank" href='<?= $p['facebook'];?>'><i class="fa fa-facebook-square"></i></a>
                        <a target="_blank" href='<?= $p['instagram'];?>'><i class="fa fa-instagram"></i></a>
                        <a target="_blank" href='<?= $p['twitter'];?>'><i class="fa fa-twitter-square"></i></a>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <div class="copyright">
        <div class="container-fluid">
            &copy; <?= date('Y') ?> Hak Cipta Terpelihara. Berobat.id
        </div>
    </div>
</footer>