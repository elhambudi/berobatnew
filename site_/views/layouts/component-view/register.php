<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use common\models\Country;
use common\models\Province;
use yii\helpers\ArrayHelper;

$model = new common\models\Member();
$country = Country::find()->all();

$countries = ArrayHelper::map(
	$country,'id','name'
);
?>

<!-- Modal -->
<div class="modal fade" id="register-panel" tabindex="-1" role="dialog" aria-labelledby="register-panel-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="lr-title">
			<div class="lr-title__big">Register</div>
		</div>
      </div>
      <div class="modal-body">
        <div class="lr-box">
			<div class="lr-input">
				<?php $form = ActiveForm::begin([
						'action' => Url::to(['site/daftarmember']),
						'method' => 'POST'
				]); ?>
				<div class="form-group">
					<div class="input-group">
					  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
					  	<input type="text" name="User[username]" class="form-control" placeholder="Full Name" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  	<span class="input-group-addon" id="basic-addon2"><i class="fa fa-envelope"></i></span>
					  	<input type="email" name="User[email]" class="form-control" placeholder="Email. Contoh: name@example.com" aria-describedby="basic-addon2">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  	<span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock"></i></span>
					  	<input type="password" name="User[password_hash]" class="form-control" placeholder="Password. Minimal 6 karakter" aria-describedby="basic-addon2">
					</div>
				</div>
				<div class="form-group">
					<div class="radio">
						<?php 
							$code = common\models\Gender::find()->all();
							foreach($code as $data){
								echo '<label><input type="radio" name="Member[gender_id]" id="optionsRadios1" value="'.$data['id'].'" checked>'.$data['value'].'</label>&nbsp;&nbsp;&nbsp;';
								//echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
							}
						?><!--
					  	<label>
					    	<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
					    	Laki - Laki
					  	</label>
					  	&nbsp;&nbsp;&nbsp;
					  	<label>
					    	<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
					    	Perempuan
					  	</label>-->
					</div>
				</div>
				<div class="form-group">
<!--					<div class="input-group"> -->
						<?= $form->field($model, 'birtdate')->widget(DatePicker::classname(), [
									'options' => ['placeholder' => 'Enter birth date ...'],
									'pluginOptions' => [
										'autoclose'=>true,
										'format' => 'yyyy-mm-dd'
									]
								])->label(false);		
						?>
	<!--				  	<span class="input-group-addon" id="basic-addon2"><i class="fa fa-calendar"></i></span>
					  	<input type="text" id="datepicker" class="form-control" placeholder="Tanggal lahir" aria-describedby="basic-addon2"> -->
<!--					</div> -->
				</div>
				<div class="form-group">
					<fieldset class="group-address">
  						<legend>Address</legend>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<?= 								
										Html::dropDownList(null,'',$countries,[
											'id' => 'country-list',
											'class' => 'form-control',
											'prompt'=>'Select Country...'
										]);
									?>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
								    <?=
										DepDrop::widget([
											'name' => 'province-list',
											'options' => [
												'id' => 'province-list',
												'class' => 'form-control',
											],
											'pluginOptions' => [
												'placeholder' => 'Select Province',
												'url' => Url::to(['location/get-province-by-country']),
												'depends' => ['country-list'],
											]
										])									
									?>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<?=
										$form->field($model,'city_id')->widget(DepDrop::className(),[
											'pluginOptions' => [
												'placeholder' => 'Select City',
												'url' => Url::to(['location/get-city-by-province']),
												'depends' => ['province-list'],
											]
										])->label(false);
									?>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
				<div class="lr-input__desc">
					Dengan klik Register, kamu telah menyetujui <a href="#">Aturan Penggunaan</a> dan <a href="#">Kebijakan Privasi</a> dari Berobat.id
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-full">Register</button>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
		<div class="sosmed-way">
			<div class="seperator">
				<span class="seperator__text">ATAU</span>
			</div>
			<div class="sosmed-way__place">
				<!--<div class="row">
					<div class="col-md-6 col-sm-12">
						<a class="btn btn-default">
							<img alt="Facebook Icon" class="btn__farleft-icon" height="24" src="<?= Yii::$app->request->baseUrl ?>/frontend/web/images/fb-logo.png" width="24">
							Daftar dengan Facebook
						</a>
					</div>
					<div class="col-md-6 col-sm-12">
						<a class="btn btn-default">
							<img alt="Facebook Icon" class="btn__farleft-icon" height="24" src="<?= Yii::$app->request->baseUrl ?>/frontend/web/images/google-logo.png" width="24">
							Daftar dengan Google
						</a>
					</div>
				</div>-->
				<div class="row mt-30">
					<div class="col-xs-12 text-center">
						Sudah punya akun? Silakan <a href="#" class="iframe-login">login</a>
					</div>
				</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
  <script type="text/javascript">
//   $( function() {
//     $( "#datepicker" ).datepicker();
//   } );
  </script>