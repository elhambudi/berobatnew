<?php 

use yii\helpers\Html;
use yii\helpers\Url;

$listCategory = \common\models\Category::find()->all();

?>

<header class="navbar-fixed-top">
    <nav class="navbar navbar-top-small">
        <div class="container-fluid">
            <div class="pull-right">
                <ul class="nav navbar-nav">
                    <li id="iframe-partner">
                        <?= Html::a( "Join as Partner", ['/site/become-clinic-partner']) ?>
                    </li>
                    <li>
                        <?= Html::a( "Blog", ['/site/blog']) ?>
                    </li>
					<li>
                        <?= Html::a( "About Us", ['/site/aboutus']) ?>
                    </li>
                    <li>
                        <?= Html::a( "Help", ['/site/help']) ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
				<?php
					$banner = \common\models\LogoHeader::find()->all();
					foreach($banner as $b){
				?>
				<a class="navbar-brand" href="<?=Url::to(['site/index']);?>"><img src="<?=Url::to('@rootWeb/site/web/logo_header/'.$b['gambar']);?>" /></a>
				<?php
					}
				?>
                <!-- <a class="navbar-brand" href="">
                    <img src="<?= Yii::$app->request->baseUrl ?>/frontend/web/images/logo.png">
                </a> -->
            </div>
            <div class="pull-left lokasi mr-15">
                <span>Lokasi Saya di</span>
                <i class="fa fa-map-marker"></i>
                <a href="#">
                    <span id="current-location-text">Surabaya</span>
                    <i class="fa fa-angle-down"></i>
                    <div class="clear"></div>
                </a>
                <div class="clear"></div>
                <div id="lokasi-opsi-wrapper">
                    <div class="lokasi-opsi-content">
                        <ul>
                            <li><a href="#">Jakarta</a></li>
                            <li class="active"><a href="#">Surabaya</a></li>
                            <li><a href="#">Bandung</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="pull-right account-menu">
                <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button> -->
                <ul class="nav navbar-nav">
					<?php
						//$gambar = Yii::$app->user->identity->gambar;
						 if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role_id == 5) {
						 //var_dump(Yii::$app->user->identity->gambar);die();
					?>
                    <li id="iframe-cart" class="ml-15"><a href="<?=Url::to(['member/index','id'=>Yii::$app->user->identity->id]);?>"><i class="fa fa-shopping-cart"></i></a></li>
                    <li id="iframe-cart"><a href="<?=Url::to(['member/editprofiluser','id'=>Yii::$app->user->identity->id]);?>"><img src="<?php
						if(Yii::$app->user->identity->gambar==NULL){
							echo Yii::getAlias('@uploadsWeb/person.png') ;
						}else{
							echo Yii::getAlias('@uploadsWeb/'.Yii::$app->user->identity->gambar) ;
						}
					?>" class="img-circle" height="30px" width="30px" alt="User Image"></a></li>
                    <li id="iframe-login"><a href="<?= Url::to(['site/logout']);?>"><i class="fa fa-sign-in"></i><span>Logout</span></a></li>
					<?php } else { ?>
                    <li id="iframe-register" class="iframe-register" class="mr-15"><a href="#Daftar">Register</a></li>
                    <li id="iframe-login" class="iframe-login"><a href="#Login"><i class="fa fa-sign-in"></i><span>Login</span></a></li>
					<?php }?>
                </ul>
            </div>
            <form id="navbar-search" class="navbar-search" action="<?= \yii\helpers\Url::to(['site/search-voucher'])  ?>" method="get">
                <div class="search-parent-older">
                    <div class="searchform search-parent">
                        <i class="fa fa-search icon-search"></i>
                        <input name="q" class="input-wrapper ui-autocomplete-input" id="search-keyword" type="text" placeholder="Cari voucher treatment atau klinik" autocomplete="off">
                        <div class="cat-wrapper permanent-active">
                            <div class=" cat-result-wrapper">
                                <ul class="inline cat-result">
                                    <li class="cat-result-toggle">Semua Kategori</li>
                                    <li><i class="fa fa-angle-down"></i></li>
                                </ul>
                            </div>
                            <select name="sc" name="cat" id="cat-select" class="cat-select absolute" style="display: block;">
                                <option value="0">Semua Kategori</option>
                                <?php foreach($listCategory as $val): ?>
                                    <option value="<?=$val->id?>"><?= $val->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <span class="btn-search-wrapper">
                            <button class="btn new-btn-search" type="submit">
                                <i class="fa fa-search"></i>
                                <span>Cari</span>
                            </button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </nav>
</header>