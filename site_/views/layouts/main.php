<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
use common\widgets\Alert;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="app">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?= Yii::$app->request->baseUrl ?>/site/web/images/logo.png">
    <?= Html::csrfMetaTags() ?>
    <title><?= ($this->title) ? Html::encode($this->title)." | " : "" ?> Berobat.id</title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="outer-wrapper">
    <?= $this->render('component-view/header.php'); ?>
    <?= $this->render('component-view/loading.php'); ?>
    <div id="content-container">
        <div class="container-fluid">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
        </div>
        <?= $content ?>
    </div>
</div>
<?= $this->render('component-view/footer.php'); ?>
<?= $this->render('component-view/login.php'); ?>
<?= $this->render('component-view/register.php'); ?>

<div class="js-flyout-overlay"></div>
<script>
	// $(function(){
	// 	$(".datepicker").datepicker({
	// 		dateFormat:"yy-mm-dd"
	// 	});
	// });
</script>
<?php $this->endBody() ?>
<script>
	  $(function () {
		$("#example1").DataTable();
		$('#example2').DataTable({
		  "paging": true,
		  "lengthChange": false,
		  "searching": false,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});
	  });
	  $(function(){
			$(".datepicker").datepicker({
				dateFormat:"yy-mm-dd"
			});
			$(".datepicker2").datepicker({
				dateFormat:"yy-mm-dd"
			});
			$(".datepicker3").datepicker({
				dateFormat:"yy-mm-dd"
			});
		});
	</script>
</body>
</html>
<?php $this->endPage() ?>