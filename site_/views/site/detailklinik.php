<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<div class="voucher-list">
	<div class="container-fluid">
		<section>

      <div class="row">
        <div class="col-xs-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<div class="pull-right">
						<?= Html::a( "<i class='fa fa-angle-left'></i> Back", ['site/index'], ['class'=>"btn-back"]) ?>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-4">
							<div class="row">
								<div class="col-md-12 col-sm-6">
									<div class="detail-klinik__logo"><img class="profile-user-img img-responsive" src="<?= Yii::$app->request->baseUrl; ?>/site/web/clinic_photo/<?=$listClinic['logo']?>" height="100px"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="detail-klinik__name">
								<strong>Clinic Name : <br /></strong>
								<p><?=$listClinic->name_clinic?></p>
							</div>		
							<div class="detail-klinik__name">
								<strong>Categories</strong> : <?php 
									$code = common\models\CategoryHasClinic::find()->where(['clinic_id'=>$listClinic->id])->all();
									foreach($code as $co){
										$code1 = common\models\Category::find()->where(['id'=>$co['category_id']])->all();
										foreach($code1 as $c){
											echo '<span class="detail-klinik__status label label-primary">'.$c['name'].'</span> ';
										}
									}
								?>
							</div>
							<div class="detail-klinik__name">
								<strong>Deskripsi Detail : <br /></strong>
								<?=$listClinic->description?> 
							</div>			
							<div class="detail-klinik__name">
								<table>
									<tr>
										<th width="50">Owner</th>
										<td width="10">:</td>
										<td><?=$listClinic->name_contact?></td>
									</tr>
									<tr>
										<th>Email</th>
										<td>:</td>
										<td><?php 
											$code = common\models\User::find()->where(['id'=>$listClinic->user_id])->all();
											foreach($code as $co){
												echo $co['email'];
											}
										?></td>
									</tr>
									<tr>
										<th>Phone</th>
										<td>:</td>
										<td><?=$listClinic->phone?></td>
									</tr>
								</table>
							</div>
							<div class="detail-klinik__office">
								<div class="the-title">Office : </div>
								<ul>
									<?php 
										$code = common\models\Office::find()->where(['clinic_id'=>$listClinic->id])->all();
										foreach($code as $co){
											$code1 = common\models\City::find()->where(['id'=>$co['city_id']])->all();
											foreach($code1 as $c){
												echo '<li>
														<div class="the-mark"><i class="fa fa-map-marker"></i>
														<span>'.$co['address'].'</span><br>
															<span>'.$c['name'].'</span><br>
														</div>
													<div class="clear"></div>
												</li>';
											}
										}
									?>
								</ul>
							</div>							
						</div>
					</div>
				</div>
			</div>

		</div>
				</section>
	</div>
</div>