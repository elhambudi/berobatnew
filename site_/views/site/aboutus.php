<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use common\models\Category;
use common\models\Country;
use common\models\Province;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'About Us';

?>

<div class="contact-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="ask">
                    <strong>Ada pertanyaan? </strong>
                    Kami siap membantu Anda.
                </div>
            </div>
            <div class="col-md-6">
                <div class="the-contact">
                    <div class="item">
                        <a href="mailto:ask@berobat.id"><i class="fa fa-envelope"></i> ask@berobat.id</a>
                    </div>
                    <div class="item">
                        <a href="tel:0822 3102 3315"><i class="fa fa-phone"></i> 0822 3102 3315</a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="become-clinic-partner">
	<section class="about">
		<div class="container">
			<h2>About Us</h2>
			<?=$model->description?><br>
		</div>
	</section>
</div>