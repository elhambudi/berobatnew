<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Situs Beli Voucher Treatment Kesehatan Online Mudah Dan Terpecaya";
?>
<div id="banner-slide-home" class="carousel slide" data-ride="carousel">
  	<!-- Indicators -->
  	<ol class="carousel-indicators">
		<?php
			$dataslider = \common\models\Slider ::find()->count();
			//var_dump($dataslider);die();
			for($s=0;$s<(int)$dataslider;$s++){
				if($s==0){
		?>
			<li data-target="#banner-slide-home" data-slide-to="<?=$s?>" class="active"></li>
		<?php
				}else{
		?>
			<li data-target="#banner-slide-home" data-slide-to="<?=$s?>"></li>
		<?php
				}
			}
		?> 
  	</ol>

  	<!-- Wrapper for slides -->
  	<div class="carousel-inner" role="listbox">
		<?php
			$slider = \common\models\Slider ::find()->all();
			$i=0;
			foreach($slider as $b){
				if($i==0){
		?>
			<div class="item active">
				<a href="#" target="blank"><img src="<?=Url::to('@rootWeb/site/web/slider/'.$b['gambar']);?>" alt="..."></a>
			</div>
		<?php
			}else{
		?>
			<div class="item">
				<a href="#" target="blank"><img src="<?=Url::to('@rootWeb/site/web/slider/'.$b['gambar']);?>" alt="..."></a>
			</div>
		<?php
			}
			$i++;
			}
		?>
	   
	   
  	</div>

  	<!-- Controls -->
  	<a class="left carousel-control" href="#banner-slide-home" role="button" data-slide="prev">
    	<i class="fa fa-chevron-left" aria-hidden="true"></i>
    	<span class="sr-only">Previous</span>
  	</a>
  	<a class="right carousel-control" href="#banner-slide-home" role="button" data-slide="next">
    	<i class="fa fa-chevron-right" aria-hidden="true"></i>
    	<span class="sr-only">Next</span>
  	</a>
</div>
<div id="categories">
	<section class="container-fluid">
		<div class="section-title">
			<div class="section-title__border"></div>
			<div class="section-title__header"><span>Category</span></div>
		</div>
		<div class="category-section-content">
			<?php foreach($category as $c){?>
			<a href="<?= Url::to(['site/searchkategori','id'=>$c['id']]); ?>" class="category-section-content__box">
				<img src="<?= Yii::$app->request->baseUrl ?>/site/web/category_photo/<?= $c['image']?>">
				<span><?= $c['name']?></span>
			</a>
			<?php
				}
			?>
		</div>
	</section>
</div>
<div id="clinic-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Our Partners</span></div>
			</div>
			<div class="section-action">
				<div class="pull-right">
					<?= Html::a( "Join as Partner", ['/site/become-clinic-partner'], ['class'=>"btn btn-primary btn-sm"]) ?>
					<?= Html::a( "See All", ['/site/semuaklinik'], ['class'=>"btn btn-default btn-sm"]) ?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="row">
				<?php foreach($clinic as $cli){?>
				<div class="col-md-2 col-sm-3 col-xs-12">
					<a href="<?=Url::to(['site/detailklinik','id'=>$cli['id']]);?>" class="the-clinic">
						<div class="logo-clinic" style="background-image: url('<?= Yii::$app->request->baseUrl ?>/site/web/uploads/<?= $cli['logo']?>')"></div>
					</a>
				</div>
				<?php }?>
			</div>
		</section>
	</div>
</div>
<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Best Deal</span></div>
			</div>
			<div class="section-action">
				<div class="pull-right">
					<?= Html::a( "See All", ['/site/semuavoucher'], ['class'=>"btn btn-default btn-sm"]) ?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="row">
				<?php foreach( $listVoucher as $v ): ?>
				<div class="col-md-3 col-sm-6">
					<div class="item">
						<div class="box">
							<div class="image" style="background-image:url('<?= Yii::$app->request->baseUrl; ?>/site/web/voucher_photo/<?=$v['image']?>')">
								<div class="wrap-hover"><a href="<?=Url::to(['site/detail','id'=>$v['id']]);?>">View Detail</a></div>
								
							</div>
							<div class="price">
								<div class="the-price">
									<div class="voucher">Rp <?= $v->voucher_price ?></div>
									<div class="real">Rp <?= $v->real_price ?></div>
								</div>
								<div class="the-discount">
									<div class="text">Save up to</div>
									<div class="nominal"><?= round($v->discount) ?><span class="the-percent">%</span></div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</section>
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Recommendation</span></div>
			</div>
			<div class="section-action">
				<div class="pull-right">
					<a class="btn btn-default btn-sm">See All</a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="row">
				<?php foreach( $listVoucher as $v ): ?>
				<div class="col-md-3 col-sm-6">
					<div class="item">
						<div class="box">
							<div class="image" style="background-image:url('<?= Yii::$app->request->baseUrl; ?>/site/web/voucher_photo/<?=$v['image']?>')">
								<div class="wrap-hover"><a href="#">View Detail</a></div>
								
							</div>
							<div class="price">
								<div class="the-price">
									<div class="voucher">Rp <?= $v->voucher_price ?></div>
									<div class="real">Rp <?= $v->real_price ?></div>
								</div>
								<div class="the-discount">
									<div class="text">Save up to</div>
									<div class="nominal"><?= round($v->discount) ?><span class="the-percent">%</span></div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</section>
	</div>
</div>