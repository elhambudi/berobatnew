<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<div class="voucher-list">
	<div class="container-fluid">
		<section>

      <div class="row">
        <div class="col-xs-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<div class="pull-right">
						<?= Html::a( "<i class='fa fa-angle-left'></i> Back", ['site/index'], ['class'=>"btn-back"]) ?>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-4">
							<div class="row">
								<div class="col-md-12 col-sm-6">
									<div class="detail-klinik__logo"><img class="profile-user-img img-responsive" src="<?= Yii::$app->request->baseUrl; ?>/site/web/voucher_photo/<?=$listVoucher['image']?>" height="100px"></div>
								</div>
								<div class="border-space-15"></div>
								<?php
									 if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role_id == 5) {
								?>
								<div class="col-md-12 col-sm-6">
									<a href="<?=Url::to(['site/purchase','id_voucher'=>$listVoucher->id,'id_user'=>Yii::$app->user->identity->id]);?>" class="btn btn-success btn-block btn-pad">Purchase Voucher</a>
								</div>
								<?php
									}
								?>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="detail-klinik__name">
								<strong>Voucher Name : <br /></strong>
								<p><?=$listVoucher->name?></p>
							</div>
							<div class="detail-klinik__desc">
								<strong>Deskripsi Detail : <br /></strong>
								<p><?=$listVoucher->description?></p>
							</div>
							<div class="detail-klinik__desc">
								<strong>Real Price : <br /></strong>
								<p><?='Rp '.number_format($listVoucher->real_price)?></p>
							</div>		
							<div class="detail-klinik__desc">
								<strong>Voucher Price : <br /></strong>
								<p><?='Rp '.number_format($listVoucher->voucher_price)?></p>
							</div>		
							<div class="detail-klinik__desc">
								<strong>Discount : <br /></strong>
								<!--<p><?='Rp '.number_format($listVoucher->discount)?></p>-->
								<p><?=ceil($listVoucher->discount).'%'?></p>
							</div>	
							<div class="detail-klinik__desc">
								<strong>Stock : <br /></strong>
								<p><?=number_format($listVoucher->stock)?></p>
							</div>		
						</div>
					</div>
				</div>
			</div>

		</div>
				</section>
	</div>
</div>