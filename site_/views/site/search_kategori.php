<?php

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = "Situs Beli Voucher Treatment Kesehatan Online Mudah Dan Terpecaya";
?>
<div id="categories">
	<section class="container-fluid">
		<div class="section-title">
			<div class="section-title__border"></div>
			<div class="section-title__header"><span>Kategori</span></div>
		</div>
		<div class="category-section-content">
			<?php foreach($category as $c){?>
			<a href="#" class="category-section-content__box">
				<img src="<?= Yii::$app->request->baseUrl ?>/site/web/category_photo/<?= $c['image']?>">
				<span><?= $c['name']?></span>
			</a>
			<?php
				}
			?>
		</div>
	</section>
</div><div id="clinic-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Klinik Patner</span></div>
			</div>
			<div class="section-action">
				<div class="pull-right">
					<?= Html::a( "Menjadi Klinik Patner Kami", ['/site/become-clinic-partner'], ['class'=>"btn btn-primary btn-sm"]) ?>
					<!--<a class="btn btn-default btn-sm">Lihat Semua</a>-->
				</div>
				<div class="clear"></div>
			</div>
			<div class="row">
				<?php 
					foreach($clinic as $cli){
				?>
				<div class="col-md-2 col-sm-3 col-xs-12">
					<a href="#" class="the-clinic">
						<div class="logo-clinic" style="background-image: url('<?= Yii::getAlias('@uploadsWeb') ?>/<?=  $cli['logo'] ?>')"></div>
					</a>
				</div>
				<?php }?>
			</div>
		</section>
	</div>
</div>
<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Voucher Hari Ini</span></div>
			</div>
			<div class="section-action">
				<div class="pull-right">
					
				</div>
				<div class="clear"></div>
			</div>
			<div class="row">
				<?php
					foreach($voucher as $v){
				?>
				<div class="col-md-3 col-sm-6">
					<div class="item">
						<div class="box">
							<div class="image" style="background-image:url('<?= Yii::$app->request->baseUrl; ?>/site/web/voucher_photo/<?=$v['image']?>')">
								<div class="wrap-hover"><a href="#">Lihat Detail</a></div>
								
							</div>
							<div class="price">
								<div class="the-price">
									<div class="voucher">Rp <?=number_format($v['voucher_price'])?></div>
									<div class="real">Rp <?=number_format($v['real_price'])?></div>
								</div>
								<div class="the-discount">
									<div class="text">Save up to</div>
									<div class="nominal">50<span class="the-percent">%</span></div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
				<?php
					}
				?>
			</div>
		</section>
	</div>
</div>