<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
				'id' => 'form-signup',
				'action' => Url::to(['site/daftar']),
                'method' => 'POST'
				]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password_hash')->passwordInput() ?>

                <div class="form-group">
                    
					<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</button>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
