<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use common\models\Category;
use common\models\Country;
use common\models\Province;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Ayo Gabung Menjadi Klinik Patner Kami';
$categories = ArrayHelper::map(
	Category::find()->all(),'id','name'
);

$country = Country::find()->all();

$countries = ArrayHelper::map(
	$country,'id','name'
);

?>

<div class="contact-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="ask">
                    <strong>Ada pertanyaan? </strong>
                    Kami siap membantu Anda.
                </div>
            </div>
            <div class="col-md-6">
                <div class="the-contact">
                    <div class="item">
                        <a href="mailto:ask@berobat.id"><i class="fa fa-envelope"></i> ask@berobat.id</a>
                    </div>
                    <div class="item">
                        <a href="tel:0822 3102 3315"><i class="fa fa-phone"></i> 0822 3102 3315</a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="become-clinic-partner">
	<section class="about">
		<div class="container">
		    <?php
		        $programclinic=\common\models\ProgramClinic::findOne(1);
		    ?>
			<h2><?=$programclinic->judul;?></h2>
			<?=$programclinic->description;?><br>
			<a class="btn btn-primary" id="to-register">Join as Partner</a>
		</div>
	</section>
	<section class="benefit">
		<div class="container">
			<h2>Benefit to Become Our Clinic Partner</h2>
			<div class="row the-benefit">
			    <?php
			        $benefit=\common\models\Benefit::find()->all();
			        foreach($benefit as $b){
			    ?>
				<div class="col-md-12 col-sm-6">
					<img src="<?=Url::to('@rootWeb/site/web/benefit/'.$b['gambar']);?>" clas="icon" />
					<div class="desc">
						<div class="title"><?=$b['judul']?></div>
						<?=$b['description']?>
					</div>
				</div>
				<?php
			        }
				?>
			</div><div id="ret-regis"></div>
		</div>
	</section>
	<?php if(!$success){ ?>
	<section class="form-register">
		<div class="container">
			<h2>Register Your Clinic</h2>
			<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
				<div class="form-group">
					<?= $form->field($model,'name_clinic')->textInput([
						'class' => 'form-control',
						'placeholder' => 'Nama Klinik Anda'
					])->label('Clinic Name') ?>
				</div>
				<div class="form-group">
				    <label>Category</label>
				    <div class="select2-style">
						<?=
							Select2::widget([
								'name' => 'categories',
								'value' => '',
								'data' => $categories,
								'options' => [
									'multiple' => true, 
									//'placeholder' => 'Pilih Kategori',
									'prompt' => 'Pilih Kategori'
								]
							]);						
						?>
						<?php if(!is_null($errors['categories'])){ ?>
						<div class="text-danger">
							<?= $errors['categories'] ?>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="form-group">
					<fieldset class="group-address">
  						<legend>Clinic Address</legend>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
				    				<label>Country</label>
									<?= 								
										Html::dropDownList(null,'',$countries,[
											'id' => 'country-list',
											'class' => 'form-control',
											'prompt'=>'Select Country...'
										]);
									?>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label>Province</label>
								    <?=
										DepDrop::widget([
											'name' => 'province-list',
											'options' => [
												'id' => 'province-list',
												'class' => 'form-control',
											],
											'pluginOptions' => [
												'placeholder' => 'Select Province',
												'url' => Url::to(['location/get-province-by-country']),
												'depends' => ['country-list'],
											]
										])									
									?>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<?=
										$form->field($modelOffice,'city_id')->widget(DepDrop::className(),[
											'pluginOptions' => [
												'placeholder' => 'Select City',
												'url' => Url::to(['location/get-city-by-province']),
												'depends' => ['province-list'],
											]
										])->label('City');
									?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<?= $form->field($modelOffice,'address')->textArea([
									'class' => 'form-control',
									'rows' => 7,
									'placeholder' => 'Alamat Lengkap Klinik'
								]) ?>
							</div>
						</div>
					</fieldset>
				</div>
				<div class="form-group">
					<?=
						$form->field($model,'name_contact')->textInput([
							'class' => 'form-control',
							'placeholder' => 'Contact Name (Clinic Owner)'
						])->label('Nama Pemilik Klinik');
					?>
				</div>
				<div class="form-group">
					<?=
						$form->field($modelUser,'email')->textInput([
							'class' => 'form-control',
							'placeholder' => 'Email'							
						])->label('Email Klinik');
					?>
				</div>
				<div class="form-group">
					<?=
						$form->field($modelUser,'password_hash')->passwordInput([
							'class' => 'form-control',
							'placeholder' => 'Password'							
						])->label('Password');
					?>
				</div>
				<div class="form-group">
					<?=
						$form->field($model,'phone')->textInput([
							'placeholder' => 'Nomor Telepon Klinik (+6221-567890)',
							'class' => 'form-control'
						])->label('Phone Number')
					?>
				</div>
				<div class="form-group">
					<?=
						$form->field($uploadModel,'uploadedFile')->fileInput([
							//'class' => "form-control",
							'accept' => 'image/jpg,image/jpeg,image/png,application/pdf'
						])->label('File SIUP Klinik')
					?>
				    <p class="help-block">Format file (JPG / PNG / PDF).</p>
					<?php if(!is_null($errors['upload'])){ ?>
						<div class="text-danger">
							<?= $errors['upload'] ?>
						</div>
					<?php } ?>
				</div>
				<div class="form-group">
					<?=
						$form->field($model,'description')->textArea([
							'rows' => 5,
							'class' => 'form-control',
							'placeholder' => 'Deskripsikan klinik Anda'
						])->label('Detail Information')
					?>
				</div>
				<div class="form-group">
					<p><!-- <input type="checkbox" ng-model="termAndCondition" checked="checked" /> -->
					Dengan klik register, Anda telah menyetujui <?= Html::a( "Aturan Penggunaan", ['/site/term-of-use']) ?> dan <?= Html::a( "Kebijakan Privasi", ['/site/privacy-policy']) ?> dari Berobat.id
					</p>
				</div>
				<div class="form-group">
					<button class="btn btn-primary">										
						<span>Register My Clinic</span>
					</button>
				</div>
			<?php ActiveForm::end() ?>
		</div>
	</section>
	<?php } ?>
	<?php if( $success){ ?>
	<section class="success-register">
		<div class="container">
			<div class="row">
				<div class="col-sm-7 message">
					<h2>Congratulation! your request to join into our clinic partner partner was successfully sent</h2>
					<p>We will conduct a review within 2 x 24 hours. If we accept you request, you will receive an email from us. If there are any question, please feel free to contact us.</p>
					<div class="contact">
						<div class="row">
							<div class="col-sm-4">
								<div class="the-contact">
									<div class="item">
			                            <i class="fa fa-envelope"></i> ask@berobat.id
			                        </div>
			                        <div class="item">
			                            <i class="fa fa-phone"></i> 0822 3102 3315
			                        </div>
								</div>
							</div>
							<div class="col-sm-8">
								<div class="location">
									Komplek Perkantoran Sentra Bisnis Tanjung Duren. Blok C No: 9. Jalan Tanjung Duren Utara I, Jakarta Barat
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="img-nurse">
						<img src="<?= Yii::$app->request->baseUrl ?>/site/web/images/nurse.png">
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php } ?>
</div>