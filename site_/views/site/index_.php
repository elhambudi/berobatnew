<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Situs Beli Voucher Treatment Kesehatan Online Mudah Dan Terpecaya";
?>

<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Voucher Hari Ini</span></div>
			</div>
			<div class="section-action">
				<div class="pull-right">
					<?= Html::a( "Lihat Semua", ['/site/semuavoucher'], ['class'=>"btn btn-primary btn-sm"]) ?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
          <div class="box box-info">
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Name</th>
				  <th>Kategori</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
					<th>ID</th>
					<th>Name</th>
					<th>Kategori</th>
					<th>Action</th>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>
</div>