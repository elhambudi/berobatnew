<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="become-clinic-partner">
	<section class="form-register">
		<div class="container">
			<h2>Upload Bukti Transfer</h2>
			<?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['member/upload','id'=>$model->id]),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
				<div class="form-group">
					<?= $form->field($uploadModel,'uploadedFile')->fileInput([
							'class' => "form-control",
							'accept' => 'image/jpg,image/jpeg,image/png,application/pdf'
						])->label('Bukti Transfer') ?>
				</div>
				<div class="form-group">
					<button class="btn btn-primary">										
						<span><i class="fa fa-upload"></i> Upload</span>
					</button>
					<a href="<?=Url::to(['member/index'])?>" class="btn btn-danger">										
						<span><i class="fa fa-times"></i> Cancel</span>
					</a>
				</div>
			<?php yii\widgets\ActiveForm::end(); ?>
		</div>
	</section>
</div>