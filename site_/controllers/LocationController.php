<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Country;
use common\models\City;
use common\models\Province;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class LocationController extends Controller{

    public function actionGetCountry(){
        $out = ArrayHelper::map(Country::find()->all(),'id','name');        
        $selected = '';
        foreach( $out as $k => $v ){
            if( strtolower($v) == 'indonesia' ){
                $selected = $k;
                break;
            }
        }

        return Json::encode([
            'output' => $out,
            'selected' => $selected
        ]);
    }

    public function actionGetProvinceByCountry(){
        $out = [];
        $postData = Yii::$app->request->post('depdrop_parents');
        if( isset($postData) ){
            $id = isset($postData[0]) ? $postData[0] : 0;
            $list = Province::find()->where([
                'country_id' => $id
            ])->all();

            foreach($list as $k => $v){
                $out[] = [
                    'id' => $v['id'],
                    'name' => $v['name']
                ];
            }
        }

        return Json::encode([
            'output' => $out,
            'selected' => ''
        ]);
    }

    public function actionGetCityByProvince(){
        $out = [];
        $postData = Yii::$app->request->post('depdrop_parents');
        if( isset($postData) ){
            $id = isset($postData[0]) ? $postData[0] : 0;
            $list = City::find()->where([
                'province_id' => $id
            ])->all();

            foreach($list as $k => $v){
                $out[] = [
                    'id' => $v['id'],
                    'name' => $v['name']
                ];
            }
        }

        return Json::encode([
            'output' => $out,
            'selected' => ''
        ]);
    }

}