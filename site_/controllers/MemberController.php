<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use common\models\LoginForm;
use common\models\User;
use common\models\Member;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\data\Pagination;
use common\models\Clinic;
use common\models\Office;
use common\models\UploadForm;
use common\models\Constant;
use common\models\Voucher;
use common\models\OrderVoucher;
use common\models\ListTransaction;
use common\models\Bank;
use common\models\BankClinic;

/**
 * Site controller
 */
class MemberController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$voucher = OrderVoucher::find()->where(['user_id'=>Yii::$app->user->identity->id])->orderBy(['id'=>SORT_DESC])->all();
        return $this->render('index',[
			'voucher' =>$voucher,
		]);
    }
	public function actionInfo($id)
    {
		$order_voucher = OrderVoucher::findOne($id);
		$listVoucher = Voucher::findOne($order_voucher['voucher_id']); 
		$clinic = Clinic::findOne($listVoucher['clinic_id']);
		$bank_clinic = BankClinic::find()->where(['clinic_id'=>$clinic['id']])->all();
		$transaction = ListTransaction::find()->where(['id_voucher'=>$order_voucher['id']])->all(); 
		//var_dump($clinic['id']);die();
		$category = \common\models\Category::find()->all();
		$voucher = OrderVoucher::find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
        return $this->render('info',[
			'voucher' =>$voucher,
			'ordervoucher' =>$order_voucher,
			'category'=>$category,
            'listVoucher' => $listVoucher,
            'bank_clinic' => $bank_clinic,
            'transaction' => $transaction,
		]);
    }
	public function actionEdit($id)
    {
		$order_voucher = OrderVoucher::findOne($id);
		$uploadModel = new UploadForm();
        return $this->render('edit',[
			'model' =>$order_voucher,
			'uploadModel' =>$uploadModel,
		]);
    }
	public function actionEditprofiluser($id)
    {
		$user = User::findOne($id);
		$uploadModel = new UploadForm();
        return $this->render('profil',[
			'model' =>$user,
			'uploadModel' =>$uploadModel,
		]);
    }
	public function actionTambah($id)
    {
		$order_voucher = OrderVoucher::findOne($id);
		$uploadModel = new UploadForm();
        return $this->render('tambah',[
			'model' =>$order_voucher,
			'uploadModel' =>$uploadModel,
		]);
    }
	public function actionEditprofil($id) {
            $model = User::findOne($id);
			$postData = Yii::$app->request->post();
			if (Yii::$app->request->isPost) {
				$uploadModel = new UploadForm();
				$uploadModel->uploadedFile = UploadedFile::getInstance($uploadModel, 'uploadedFile');
				$name = $uploadModel->upload();
				//var_dump( $name);die();
				if( $name ){
					$model->gambar = $name;
					$model->username = $postData['User']['username'];
					$model->email = $postData['User']['email'];
					$model->save();
					
					return $this->redirect(['index']);                                  
				}else{
					$errors['upload'] = "Upload Gagal";                                      
				}
			} else {
			   var_dump( $model->errors);
               die();
			}
			var_dump( "gagal");
               die();
           return $this->redirect(['index']);
    } 
	public function actionUpload($id) {
            $model = OrderVoucher::findOne($id);
			if (Yii::$app->request->isPost) {
				$uploadModel = new UploadForm();
				$uploadModel->uploadedFile = UploadedFile::getInstance($uploadModel, 'uploadedFile');
				$name = $uploadModel->upload();
				//var_dump( $name);die();
				if( $name ){
					$model->image = $name;
					$model->save();
					
					return $this->redirect(['index']);                                  
				}else{
					$errors['upload'] = "Upload Gagal";                                      
				}
			} else {
			   var_dump( $model->errors);
               die();
			}
			var_dump( "gagal");
               die();
           return $this->redirect(['index']);
    }
}
