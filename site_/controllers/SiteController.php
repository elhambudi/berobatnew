<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use common\models\LoginForm;
use common\models\User;
use common\models\Member;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\data\Pagination;
use common\models\Clinic;
use common\models\Office;
use common\models\UploadForm;
use common\models\Constant;
use common\models\Voucher;
use common\models\OrderVoucher;
use common\models\ListTransaction;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionSearchVoucher($q="",$sc=0){
        $listVoucher = \common\models\Voucher::find()
            ->where(['like','name',$q]);

        if( $sc != 0 ){
            $listVoucher = $listVoucher->andWhere(['category_id' => $sc]);
        }
        return $this->render('search-voucher',[
            'listVoucher' => $listVoucher->all()
        ]);
    } 
	public function actionSemuaklinik()
    {
		$model = new LoginForm();
		$category = \common\models\Category::find()->all();
		$clinic = \common\models\Clinic::find()->all();
		$countclinic = \common\models\Clinic::find();
		$pages = new Pagination([ 'defaultPageSize' => 10,'totalCount' => $countclinic->count()]);
		$clinics = $countclinic->offset($pages->offset)
					->limit($pages->limit)
					->all();
        return $this->render('semuaklinik',[
			'model'=>$model,
			'category'=>$category,
			'clinic'=>$clinics,
			'pages' => $pages
		]);
    }
	
	public function actionSemuavoucher()
    {
		$model = new LoginForm();
		$category = \common\models\Category::find()->all();
		$voucher = \common\models\Voucher::find()->all();
		$countvoucher = \common\models\Voucher::find();
		$pages = new Pagination([ 'defaultPageSize' => 8,'totalCount' => $countvoucher->count()]);
		$vouchers = $countvoucher->offset($pages->offset)
					->limit($pages->limit)
					->all();
        return $this->render('semuavoucher',[
			'model'=>$model,
			'category'=>$category,
			'voucher'=>$vouchers,
			'pages' => $pages
		]);
    }
	public function actionSearchkategori($id)
    {
		$category = \common\models\Category::find()->where(['id'=>$id])->all();
		$category_clinic = \common\models\CategoryHasClinic::find()->where(['category_id'=>$id])->all();
		$id_clinic = array();
		foreach($category_clinic as $c){
			$id_clinic[] = $c['clinic_id'];
		}
		$clinic = \common\models\Clinic::find()->where(['id'=>$id_clinic])->all();
		$voucher = \common\models\Voucher::find()->where(['category_id'=>$id])->all();
		//var_dump($clinic);die();
        return $this->render('search_kategori',[
			'category' => $category,
			'clinic' => $clinic,
			'voucher' => $voucher
		]);
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $listVoucher = Voucher::find()
			->where(['status'=>1])
            ->limit(4)
			->orderBy(['id'=>SORT_DESC])
            ->all(); 
		$category = \common\models\Category::find()->orderBy(['name'=>SORT_ASC])->all();
		$clinic = \common\models\Clinic::find()->orderBy(['id'=>SORT_DESC])->limit(6)->all();
		$model = new LoginForm();
        return $this->render('index',[
			'model'=>$model,
			'category'=>$category,
			'clinic'=>$clinic,
            'listVoucher' => $listVoucher
		]);
    }
    public function actionAboutus()
    {
        $aboutus = \common\models\AboutUs::findOne(1);
        return $this->render('aboutus',[
		    'model'=>$aboutus,
		]);
    }
        public function actionHelp()
    {
        $help = \common\models\Help::findOne(1);
        return $this->render('help',[
		    'model'=>$help
		]);
    }
    public function actionFaq()
    {
        $faq = \common\models\Faq::findOne(1);
        return $this->render('faq',[
		    'model'=>$faq
		]);
    }

	public function actionDetail($id)
    {
        $listVoucher = Voucher::findOne($id); 
		$category = \common\models\Category::find()->all();
		//var_dump($rand);die();
        return $this->render('detailvoucher',[
			'category'=>$category,
            'listVoucher' => $listVoucher
		]);
    }
	public function actionDetailklinik($id)
    {
        $listClinic = Clinic::findOne($id); 
		$category = \common\models\Category::find()->all();
		//var_dump($rand);die();
        return $this->render('detailklinik',[
			'category'=>$category,
            'listClinic' => $listClinic
		]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
		$listVoucher = Voucher::find()
            ->limit(4)
            ->all(); 
			
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
		//var_dump($model);die();
        if ($model->load(Yii::$app->request->post()) && $model->login() && Yii::$app->user->identity->role_id == 5) {
			//var_dump(Yii::$app->user->identity->role_id);die();
            return $this->goBack();
        } else {
            return $this->render('index', [
                'model' => $model,
				'listVoucher' => $listVoucher,
            ]);
        }
    }
	
	public function actionLoginmember()
    {
		$listVoucher = Voucher::find()
            ->limit(4)
            ->all(); 
		
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(\yii\helpers\Url::to(['site/index']));
        }

        $model = new LoginForm();
		//var_dump($model);die();
        if ($model->load(Yii::$app->request->post()) && $model->login() && Yii::$app->user->identity->role_id == 5) {
			//var_dump('test');die();
            return $this->redirect(\yii\helpers\Url::to(['site/index']));
        } else {
			//var_dump($model->errors);die();
			Yii::$app->session->setFlash('danger', 'Gagal Tidak Bisa Login Karena Username atau Password Salah.');
			return $this->redirect(\yii\helpers\Url::to(['site/index']));
            /*return $this->render('index', [
                'model' => $model,
				'listVoucher' => $listVoucher,
            ]);*/
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
   /* public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	*/
    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
	public function actionSignup()
    {
        return $this->render('signup', [
                'model' => new User(),
            ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionDaftar(){
		$listVoucher = Voucher::find()
            ->limit(4)
            ->all(); 
	
		$model = new User();
		$username = Yii::$app->request->post('User');
        $model->username = $username['username'];;
        $model->email = $username['email'];
        $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($username['password_hash']);
		//var_dump($model->password_hash);die();
        $model->generateAuthKey();
		$model->role_id = 2;
		$model->status=0;
		$model->created_at=11111;
		$model->updated_at=11111;
		$model->save();
		if( !$model->save() ){
		   var_dump( $model->errors);
		   die();
		}
		
		return $this->render('index', [
            'model' => $model,
            'listVoucher' => $listVoucher,
        ]);
	}
    public function actionPurchase($id_voucher,$id_user){
		$listVoucher = Voucher::findOne($id_voucher);
		$modelVoucher = Voucher::findOne($id_voucher);
		$transaction = new ListTransaction();
		
		//generate code
		/*$seed = str_split('abcdefghijklmnopqrstuvwxyz'
                     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
		shuffle($seed); // probably optional since array_is randomized; this may be redundant
		$rand = '';*/
		//foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];	 
		//echo $rand;
		if($modelVoucher->stock!=0){
			$model = new OrderVoucher();
			$model->code = NULL;
			$model->user_id = $id_user;
			$model->voucher_id = $id_voucher;
			$model->voucher_id = $id_voucher;
			$model->status = 0;
			$model->status_validate = 0;
			$model->created_date = date("Y-m-d");
			$model->save();
			
			$modelVoucher->stock = ((int)$modelVoucher->stock)-1;
			$modelVoucher->save();
			
			$transaction->id_user = $id_user;
			$transaction->id_voucher = $model->getPrimaryKey();
			$modelForSlip = ListTransaction::find()->orderBy(['id'=> SORT_DESC])->one();
			$transaction->kode_transaksi = (string)(((int)$modelForSlip->kode_transaksi)+1);
			$transaction->save();			
			
			if( !$transaction->save() ){
			   var_dump($transaction->errors);
			   die();
			}
			Yii::$app->session->setFlash('success', 'Terimakasih Sudah purchase Voucher.');
		}else{
			Yii::$app->session->setFlash('error', 'Maaf Stock Voucher Kosong.');
		}
		return $this->redirect(['detail', 'id'=>$id_voucher
            //'model' => $model,
            //'listVoucher' => $listVoucher,
        ]);
	}
	public function actionDaftarmember(){
		$listVoucher = Voucher::find()
            ->limit(4)
            ->all(); 
		$category = \common\models\Category::find()->all();
		$clinic = \common\models\Clinic::find()->limit(6)->all();
		
		$model = new User();
		$username = Yii::$app->request->post('User');
        $model->username = $username['username'];
        $model->email = $username['email'];
        $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($username['password_hash']);
		//var_dump($model->password_hash);die();
        $model->generateAuthKey();
		$model->role_id = Constant::ROLE_MEMBER;
		$model->status=1;
		$model->created_at=11111;
		$model->updated_at=11111;
		$model->save();
		if($model->save()){
		   $modelmember = new Member();
		   $member = Yii::$app->request->post('Member');
		   //var_dump($member);die();
		   $modelmember->name = $username['username'];
		   $modelmember->birtdate = $member['birtdate'];
		   $modelmember->city_id = $member['city_id'];
		   $modelmember->gender_id = $member['gender_id'];
		   $modelmember->user_id = $model->getPrimaryKey();
		   $modelmember->save();
		}else{
			//var_dump( $model->errors);
		   //die();
		   Yii::$app->session->setFlash('danger', 'Gagal Tidak Bisa Mendaftar.');
		   return $this->redirect('index');
		}
		return $this->render('index', [
            'model' => $model,
			'category'=>$category,
			'clinic'=>$clinic,
            'listVoucher' => $listVoucher
        ]);
	}/*
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
	
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionBecomeClinicPartner($status = 0){
        $model = new Clinic(); 
        $modelOffice = new Office();
        $userModel = new User();
        $uploadModel = new UploadForm();
        $postData = Yii::$app->request->post();
        $status = $status == 0 ? false : true;
        $errors = [
            'categories' => null,
            'upload' => null
        ];
        
        if( $postData ){
            //cek kategori
            if( isset($postData['categories']) ){
                $validationStepCount = 0;

                //inisialisasi user data
                $postData['User']['username'] = $postData['User']['email'];
                $postData['User']['password_hash'] = Yii::$app->getSecurity()->generatePasswordHash($postData['User']['password_hash']);
                $postData['User']['role_id'] = Constant::ROLE_CLINIC;
                $postData['User']['status'] = Constant::STATUS_NONACTIVE;
                $postData['User']['created_at'] = time();

                if($userModel->load($postData) && $userModel->validate()){
                    $validationStepCount++;
                }else{
                    var_dump( $userModel->errors );
                    die();
                }

                $postData['Clinic']['created_date'] = date('Y-m-d');
                if( $model->load($postData) && $model->validate(['name_contact', 'name_clinic', 'phone', 'created_date']) ){
                    $validationStepCount++;                    
                }
                
                if( $modelOffice->load($postData) && $modelOffice->validate(['address','city_id']) ){
                    $validationStepCount++;
                }

                if( $validationStepCount == 3 ){
                    
                    $uploadModel->uploadedFile = UploadedFile::getInstance($uploadModel, 'uploadedFile');
                    $name = $uploadModel->upload();
                    if( $name ){
                        $userModel->save();

                        $model->user_id = $userModel->id;
                        $model->proof_of_permission = $name;
                        $model->save();

                        $modelOffice->clinic_id = $model->id;
                        $modelOffice->save();

                        foreach( $postData['categories'] as $v ){
                            $category = new \common\models\CategoryHasClinic();
                            $category->category_id = $v;
                            $category->clinic_id = $model->id;
                            $category->save();
                        }
                        return $this->redirect(\yii\helpers\Url::to(['site/become-clinic-partner','status' => 1]));                                  
                    }else{
                        $errors['upload'] = "Upload Gagal";                                      
                    }

                }

            }else{
                $errors['categories'] = "Setidaknya pilih 1 kategori";
            }
        }

        return $this->render('becomeClinicPartner',[
            'model' => $model,
            'modelOffice' => $modelOffice,
            'modelUser' => $userModel,
            'uploadModel' => $uploadModel,
            'errors' => $errors,
            'success' => $status
        ]);
    }
	
	public function actionLogout()
    {
        if( Yii::$app->request->cookies->has('data') ){
            Yii::$app->response->cookies->remove('data');
        }
		Yii::$app->user->logout();
        //return $this->goHome();
        return $this->redirect(\yii\helpers\Url::to(['site/index']));
    }
	

}
