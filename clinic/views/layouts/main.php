<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
use common\widgets\Alert;
use clinic\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="app">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <link rel="icon" href="<?= Yii::$app->request->baseUrl ?>/../common/assets/images/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <title><?= ($this->title) ? Html::encode($this->title)." | " : "" ?> Berobat.id</title>
    <?php $this->head() ?>
</head>
<body class="berobat">
<?php $this->beginBody() ?>
<div class="outer-wrapper">
    <?= $this->render('component-view/header.php'); ?>
    <?= $this->render('component-view/loading.php'); ?>
    <?php 
    try {
        echo $this->render('component-menu/'.Yii::$app->controller->id.'.php');    
    } catch (Exception $e) {echo "";}
    ?>
    <div id="content-container">
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
        <div class="container pos-relative custom-wrap">
            <div class="wrap-content" style="overflow-y:scroll;">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>
<?= $this->render('component-view/footer.php'); ?>
<?php
// $this->render('component-view/modal.php'); 
?>

<div class="js-flyout-overlay"></div>
<?php $this->endBody() ?>
<script>
	  $(function () {
		$("#example1").DataTable();
		$('#example2').DataTable({
		  "paging": true,
		  "lengthChange": false,
		  "searching": false,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});
	  });
	  $(function(){
			$(".datepicker").datepicker({
				dateFormat:"yy-mm-dd"
			});
			$(".datepicker2").datepicker({
				dateFormat:"yy-mm-dd"
			});
			$(".datepicker3").datepicker({
				dateFormat:"yy-mm-dd"
			});
		});
	</script>

</body>
</html>
<?php $this->endPage() ?>