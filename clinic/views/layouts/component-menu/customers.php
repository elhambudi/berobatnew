<?php 

use yii\helpers\Html;
use yii\widgets\Menu;
?>

<nav class="sub-navbar">
	<div class="container">
		<?php 
        echo Menu::widget([
            'items' => [
                ['label' => 'Data Customers', 'url' => ['customers/list-customers']],
            ],
            'options' => [
                'class' => 'nav navbar-nav',
            ],
        ]);
        ?>
	</div>
</nav>