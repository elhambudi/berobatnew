<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;
use common\models\Clinic;

$clinicDetail = Clinic::findOne(['user_id' => Yii::$app->user->identity->id]);
?>

<nav class="navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand">
                <?= 
                    Html::a( "<img src='". Yii::$app->request->baseUrl ."/images/logo.png'>", 
                        ['/']) ;
                ?>
            </span>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php 
            echo Menu::widget([
                'items' => [
                    ['label' => 'DASHBOARD', 'url' => ['site/index']],
                    [
                        'label' => 'SALES', 
                        'url' => ['sales/'], 
                        'encode'=>false,
                        'active'=>Yii::$app->controller->id=='sales',
                    ],
                    [
                        'label' => 'VOUCHERS', 'url' => ['vouchers/'],
                        'active'=>Yii::$app->controller->id=='vouchers',
                    ],
                    [
                        'label' => 'CUSTOMERS', 'url' => ['customers/'],
                        'active'=>Yii::$app->controller->id=='customers',
                    ],
                    [
                        //'label' => 'EMPLOYEES', 'url' => ['employees/'],
                        //'active'=>Yii::$app->controller->id=='employees',
                    ],
                    [
                        'label' => 'REDEEM VOUCHERS', 'url' => ['sales/validate'],
                        'options' =>['class' => 'redeem'],
                    ],
                ],
                'options' => [
                    'class' => 'nav navbar-nav',
                ],
            ]);
            ?>
            <ul class="nav navbar-nav navbar-right">
                <!-- <li class="dropdown notif-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span>
                            <i class="fa fa-bell"></i>
                            <span class="label label-warning">10</span>
                        </span>
                    </a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="dropdown-header">Nav header</li>
                    <li><a href="#">Separated link</a></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li> -->
				<?php
					 if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role_id == 3) {
				?>
				<li class="dropdown user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span>
                                <i>
                                    
                                        <img src="<?= Yii::getAlias('@uploadsWeb') ?>/<?= !is_null($clinicDetail) ? $clinicDetail->logo : "" ?>"  alt="User Image" style="width:30px; height:30px;">
                                    
                                </i>
                        </span>
                        <!--<span><i class="fa fa-user"></i></span>-->
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="logo">
                                        <img src="<?= Yii::getAlias('@uploadsWeb') ?>/<?= !is_null($clinicDetail) ? $clinicDetail->logo : "" ?>" class="img-circle" alt="User Image">
                                    </div>
                                </div>
                                <div class="col-xs-8">
                                    <div class="name"><?= !is_null($clinicDetail) ? $clinicDetail->name_clinic : "" ?></div>
                                    <div class="email"><?= Yii::$app->user->identity->email ?></div>
                                </div>
                            </div>
                        </li>
                        <li class="user-body">
                            <ul>
                                <li>
                                    <?= Html::a( "Edit Account Info", ['/account/']) ?>
                                </li>                                
                                <li>
                                    <?= Html::a('<i class="fa fa-sign-out"></i> Logout', ['/site/logout'], [
                                        'data' => [
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
				<?php } else { ?>
					<li id="iframe-login" class="iframe-login"><a href="<?=Url::to(['site/login']);?>"><i class="fa fa-sign-in"></i><span> Login</span></a></li>
				<?php }?>
                
            </ul>
        </div><!--/.nav-collapse -->
    </div>        
</nav>