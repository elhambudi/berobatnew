<!-- LOADER ===========================================-->
<div id="loader">
    <div class="loader">
        <div class="position-center-center">
            <div class="sk-wave">
                <div class="sk-rect sk-rect1"></div>
                <div class="sk-rect sk-rect2"></div>
                <div class="sk-rect sk-rect3"></div>
                <div class="sk-rect sk-rect4"></div>
                <div class="sk-rect sk-rect5"></div>
            </div>
            <p class="text-center">Please wait...</p><br><br>
        </div>
    </div>
</div>