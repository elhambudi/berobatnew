<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

?>
<br><br><br>
<div class="login-box" ng-controller="LoginClinicController">
    <div class="login-logo">
        <div class="row">
            <div class="col-sm-5">
                <div class="the-logo">
                    <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png">
                </div>
            </div>
            <div class="col-sm-7">
                <div class="the-desc">
                    <div class="the-desc__top">LOGIN DASHBOARD KLINIK</div>
                    <div class="the-desc__bottom">Sign in to start your session</div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php $form = ActiveForm::begin([
						'action' => Url::to(['site/login']),
						'method' => 'POST'
				]); ?>
            <div class='form-group has-feedback'>
                <input type="text" name="LoginForm[username]" class="form-control" 
                placeholder="Email. Contoh: name@example.com" 
                name="email" required />
                <span class='glyphicon glyphicon-envelope form-control-feedback'></span>
            </div>

            <div class='form-group has-feedback'>
                <input type="password" name="LoginForm[password]" class="form-control" placeholder="Password" name="password" required />
                <span class='glyphicon glyphicon-lock form-control-feedback'></span>                
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        LOG IN                    
                    </button>
                </div>
                <!-- /.col -->
            </div>
            <div class="text-center add-ons-link-help">
                <a href="#">Saya lupa password saya</a><br>
                <a href="<?=Url::to(['site/first']);?>">Kembali</a>
            </div>
        <?php ActiveForm::end(); ?> 
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
