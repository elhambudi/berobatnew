<?php

use common\models\Constant;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use yii\bootstrap\Modal;
?>
<div class="row">
	<div class="col-md-7">
		<div class="box-box">
			<div class="desc">
				<div class="the-title">Selamat datang di Dashboard Klinik berobat.id!</div>
				<div class="the-desc">Kami senang Anda telah bergabung bersama Berobat.id. Untuk mengakses semua fitur, harap melengkapi informasi dibawah ini.</div>
			</div>

			<section class="sub-form">
				<div class="title <?= $step['step'] == Constant::STEP_APOTIK_PROFILE ? 'active' : '' ?>">
					<span class="no">1</span>
					<span class="the-title">Profil Klinik</span>
				</div>
				<?php if( $step['step'] == Constant::STEP_APOTIK_PROFILE ): ?>				
				<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-info">
								<?= $step['msg'] ?>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label class="control-label">Logo Klinik</label>
							    <div class="image-preview-input">
							    	<img alt="Logo Clinic" id="foto-show" src="<?=Yii::getAlias('@uploadsWeb')?>/<?= $clinicModel->logo ?>">
							    </div>
							    <label class="btn btn-default btn-file btn-block">		
									<?= $form->field($uploadForm,'uploadedFile')->fileInput([
										'accept' => 'image/*',
										'hidden' => true,
										'class' => 'file-input',
										'data-container' => '#foto-show'
									])->label('<span class="title"><i class="fa fa-upload"></i> Upload Image</span>') ?>
								</label>
							</div>
						</div>
						<div class="col-md-8">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
									    <label class="control-label">Tentang Klinik Anda</label>
										<?= $form->field($clinicModel,'description')->textArea([
											'rows' => 5,
											'placeholder' => 'Ceritakan mengenai klinik anda',
											'class' => 'form-control'
										]) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="range-input-wrap">
										<div class="first">
											<div class="form-group">
												<label class="control-label">Jam operasi kerja</label>												
												<?= $form->field($clinicModel,'working_hour_start')->widget(TimePicker::className(),[
												'size' => 'xs',
												'pluginOptions' => [
													'showSeconds' => true,
													'showMeridian' => false,
													'autoClose' => true
												]
											])->label(false); ?>
											</div>
										</div>
										<div class="between">-</div>
										<div class="last">
											<div class="form-group">
											    <label>&nbsp;</label>
												<?= $form->field($clinicModel,'working_hour_end')->widget(TimePicker::className(),[
												'size' => 'xs',
												'pluginOptions' => [
													'showSeconds' => true,
													'showMeridian' => false,
													'autoClose' => true													
												]])->label(false); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<?= $form->field($clinicModel,'npwp')->textInput([
									'class' => 'form-control',
									'placeholder' => 'No Npwp',
								])->label('NPWP') ?>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-right">
								<button class="btn btn-primary btn-submit">Simpan</button>
							</div>
						</div>
					</div>
				<?php ActiveForm::end() ?>
				<?php endif; ?>				
			</section>

			<section class="sub-form">
				<div class="title <?= $step['step'] == Constant::STEP_BANK ? 'active' : '' ?>">				
					<span class="no">2</span>
					<span class="the-title">Bank Account Klinik</span>
				</div>
				<?php if( $step['step'] == Constant::STEP_BANK ): ?>				
				<div class="alert alert-info">
					<?= $step['msg'] ?>
				</div>
				<?php $form = ActiveForm::begin() ?>
				<div class="row narrow">
					<div class="col-sm-3">
						<div class="form-group">
							<label class="control-label">Bank Account</label>
							<?= $form->field($modelBankClinic,'bank_id')->dropDownList($listOfBank,[
								'class' => 'form-control'
							])->label(false) ?>
						</div>
					</div>
					<div class="col-sm-4">
						<label class="control-label">&nbsp;</label>						
						<div class="form-group">
							<?= $form->field($modelBankClinic,'number')->textInput([
								'class' => 'form-control',
								'placeholder' => 'No Rekening'
							])->label(false) ?>	
						</div>
					</div>
					<div class="col-sm-5">
						<div class="form-group">
							<label class="control-label">&nbsp;</label>
							<?= $form->field($modelBankClinic,'name')->textInput([
								'class' => 'form-control',
								'placeholder' => 'Atas Nama'
							])->label(false) ?>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<button class="btn btn-primary btn-submit">Tambah</button>
					</div>
				</div>
				<?php ActiveForm::end(); ?>
				<br>
				<?php endif; ?>
			</section>
			<section class="sub-form">
				<div class="title <?= $step['step'] == Constant::STEP_DOCTOR ? 'active' : '' ?>">				
					<span class="no">3</span>
					<span class="the-title">Daftar Dokter</span>
				</div>
				<?php if( $step['step'] == Constant::STEP_DOCTOR ): ?>				
				<div class="alert alert-info">
					<?= $step['msg'] ?>
				</div>
				<div class="desc">
					Informasi dokter sangat dibutuhkan sebagai bukti kredebilitas klinik. Maka dari itu setidaknya masukkan minimal 1 informasi dokter yang berkerja di klinik anda.
				</div>
				<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>				
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<div class="image-preview-input">							
								<img src="" alt="foto-dokter" id="dokter-foto"> 
							</div>
							<label class="btn btn-warning btn-file btn-block">							 
								<?= $form->field($modelDoctor,'foto')->fileInput([
									'hidden' => true,
									'class' => 'file-input',
									'data-container' => '#dokter-foto'
								])->label('<span class="title"><i class="fa fa-upload"></i> Upload Photo</span>') ?>
							</label>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<?= $form->field($modelDoctor,'name')->textInput([
										'class' => 'form-control',
										'placeholder' => "Nama Dokter"
									])->label('Nama *') ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label">Spesialis *</label>
									<?=
										Select2::widget([
											'name' => 'categories',
											'value' => '',
											'data' => $listOfCategory,
											'options' => [
												'multiple' => true, 
												'placeholder' => 'Pilih Kategori',
												'prompt' => 'Pilih Kategori bisa lebih dari 1',
												'required' => true
											]
										]);						
									?>							
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label">Dokumen Bukti Ijin Dokter *</label><br>
									<label class="btn btn-default btn-file">
										<span class="title"><i class="fa fa-file"></i> 
										Upload File
										</span>
										<?= $form->field($modelDoctor,'proofOfDoctor')->fileInput([
											'hidden' => true
										])->label(false) ?>
									</label>
									<span class="help-inline">File yang diperbolehkan (png / jpeg / pdf)</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<?= $form->field($modelDoctor,'bio')->textArea([
								'class' => 'form-control',
								'placeholder' => 'Biodata diri dokter',
								'rows' => 4
							])->label('Biodata *') ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<?= $form->field($modelDoctor,'education')->textArea([
								'class' => 'form-control',
								'rows' => 4
							])->label('Pendidikan') ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<?= $form->field($modelDoctor,'experience')->textArea([
								'class' => 'form-control',
								'rows' => 4
							])->label('Pengalaman') ?>						
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<?= $form->field($modelDoctor,'email')->textInput([
								'class' => 'form-control',
								'placeholder' => 'Email'
							])->label('Email') ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<?= $form->field($modelDoctor,'phone')->textInput([
								'placeholder' => 'No Telp / HP',
								'class' => 'form-control'
							])->label('No Telp.') ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="pull-right">
							<input type="submit" class="btn btn-primary" value="Simpan" />
						</div>
					</div>
				</div>
				<br>
				<?php ActiveForm::end(); ?>
				<?php endif; ?>
			</section>
			<section class="sub-form">
				<div class="title <?= $step['step'] == Constant::STEP_OFFICE ? 'active' : '' ?>">
					<span class="no">4</span>
					<span class="the-title">Lokasi Klinik</span>
				</div>
				<?php if( $step['step'] == Constant::STEP_OFFICE ): ?>	
				<div class="alert alert-info">
					<?= $step['msg'] ?>
				</div>							
				<div class="list-location">
					<div class="box"  >
						<div class="row narrow">
							<div class="col-xs-1">
								<div class="list-style"><i class="fa fa-map-marker"></i></div>
							</div>
							<div class="col-xs-10">
								<div class="address">
									<div class="short">City - Provinsi</div>
								</div>
							</div>
							<div class="col-xs-1">
								<div class="action">
									<button class="btn btn-sm btn-danger">x</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
				<form role="form">
					<div class="row narrow">
						<div class="col-md-4">
							<div class="form-group">
							    <label class="control-label">Negara</label>
								<select class="form-control"></select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label class="control-label">Provinsi</label>
							    <select class="form-control">
									<option>
									</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label class="control-label">Kota</label>
							    <select class="form-control">
									<option>
									</option>							
								</select>
							</div>
						</div>
					</div>
					<div class="row narrow">
						<div class="col-md-6">
							<div class="form-group">
							    <label class="control-label">Latitude</label>
								<input type="text" class="form-control"/>
							</div>
						</div>						
						<div class="col-md-6">
							<div class="form-group">
							    <label class="control-label">Latitude</label>
								<input type="text" class="form-control" />
							</div>
						</div>												
					</div>					
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
							    <label class="control-label">Detail Alamat / Jalan</label>
							    <textarea rows="3" type="text" class="form-control" placeholder="Detail alamat"></textarea>								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="btn btn-primary btn-submit">
								LatLng
							</div>																	
						</div>					
					</div>
					<br />
					<div class="row">
					</div>
					<br />
					<div class="row">
						<div class="col-sm-12">
							<div class="address-maps" id="map" style="height:200px;"></div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-right">
								<button class="btn btn-primary btn-submit">Tambah Lokasi</button>
							</div>
						</div>
					</div>
				</form>
				<?php endif; ?>
			</section>
		</div>
	</div>
	<div class="col-md-5">
		<div class="img-welcome vcenter">
			<img src="<?= Yii::$app->request->baseUrl ?>/images/nurse.png">
		</div>
	</div>	
</div>