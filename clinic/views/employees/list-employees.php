<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span></span></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
          <div class="box box-info">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Name</th>
				  <th>Phone</th>
                  <th>Status</th>
                  <th>Kantor</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
					<?php 
						$i=1;
						foreach($data as $v){
					?>
					<tr>
						<th><?=$i++?></th>
						<th><?=$v['name']?></th>
						<th><?=$v['phone']?></th>
						<th><?php
							$status = \common\models\User::findOne($v['id']);
							if($status['status']==0){
								echo '<span class="label label-danger">Active</span>';
							}else{
								echo '<span class="label label-success">Not Active</span>';
							}?></th>
						<th>
							<?php
								$user = \common\models\Office :: findOne($v['office_id']);
								echo $user->name;
							?>
						</th>
						<th>
						   <a href="<?=Url::to(['vouchers/validate','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-check"></span> Validate</a>
						</th>
					</tr>
					<?php
						}
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>