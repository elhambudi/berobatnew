<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span></span></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
          <div class="box box-info">
            <!-- /.box-header -->
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['employees/edit','id_user'=>$id_user,'id_employee'=>$id_employee]),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
			 <div class="box-body">
				
                <div class="form-group">
                  <label for="namakategori">ID Employee</label>
                  <?= $form->field($model,'id')->textInput([
							'class' => 'form-control',
							'readonly'=>true,
						])->label(false)?>
                </div>
				<div class="form-group">
                  <label for="namakategori">Nama</label>
                  <?= $form->field($model,'name')->textInput([
							'class' => 'form-control'
						])->label(false)?>
                </div>
				<div class="form-group">
                  <label for="namakategori">Phone</label>
                  <?= $form->field($model,'phone')->textInput([
							'class' => 'form-control'
						])->label(false)?>
                </div>
				<div class="form-group">
                  <label for="namakategori">Email</label>
                  <?= $form->field($model2,'email')->textInput([
							'class' => 'form-control'
						])->label(false)?>
                </div>
				<div class="form-group">
					<label class="control-label">Kantor</label>
					<select name="Employee[office_id]" class="form-control" ng-model="form.category_id">
						<?php foreach($office as $o){?>
							<option value="<?=$o['id']?>"><?= $o['name']?> - <?= $o['address']?></option>
						<?php 
							}
						?>
					</select>
				</div>
              </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</button>
				  <a href="<?= Url::to(["employees/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>