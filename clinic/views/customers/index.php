<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Situs Beli Voucher Treatment Kesehatan Online Mudah Dan Terpecaya";
/* @var $this yii\web\View */
?>
<?php
function TanggalIndo($date){
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

	$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
	return($result);
}
?>
<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span></span></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
          <div class="box box-info">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Name</th>
				  <th>Email</th>
				  <th>No. Telepon</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
					<?php 
						$i=1;
						if($user != NULL){
						    foreach($user as $v){
					?>
					<tr>
						<th><?=$i++?></th>
						<th><?=$v['username']
						?></th>
						<th><?=$v['email']?></th>
						<th><?=$v['email']?></th>
						<th><?php
							if($v['status']==0){
								echo '<span class="label label-danger">Not Activated</span>';
							}else{
								echo '<span class="label label-success">Activated</span>';
							}?></th>
					</tr>
					<?php
						    }
						}
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>
</div>