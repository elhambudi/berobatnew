<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span></span></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
          <div class="box box-info">
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
					<th>ID</th>
					<th>Nama</th>
					<th>Kategori</th>
					<th>Harga Voucher</th>
					<th>In Stok</th>
					<th>Expired Date</th>
					<th>Status</th>
					<th>Action</th>
                </tr>
                </thead>
                <tbody>
					<?php 
						$i=1;
						foreach($voucher as $v){
					?>
		            <tr">
		              	<th><?= $i++?></th>
		              	<th><?= $v['name']?></th>
		              	<th><?php
							$kategori = \common\models\Category ::findOne($v['category_id']);
							echo $kategori->name;
						?></th>
		              	<th class="text-right">Rp. <?= number_format($v['voucher_price'])?></th>
		              	<th class="text-center"><?= $v['stock']?></th>
		              	<th class="text-center">
							<?php
								$date = strtotime($v['expire_date']); 
								$check_date = date( 'd-m-Y', $date );
								echo $check_date;
							?> 
						</th>
						<th><?php
							if($v['status']==0){
								echo '<span class="label label-danger">Not Activate</span>';
							}else{
								echo '<span class="label label-success">Activate</span>';
							}?></th>
						<th>
						<a href="<?=Url::to(['vouchers/editform','id'=>$v['id']]);?>" type="button" class="btn active btn-warning"><span class="fa fa-edit"></span> Edit Text</a>
						<a href="<?=Url::to(['vouchers/editform1','id'=>$v['id']]);?>" type="button" class="btn active btn-warning"><span class="fa fa-picture"></span> Edit Gambar</a>
						   <a href="<?=Url::to(['vouchers/delete','id'=>$v['id']]);?>" type="button" class="btn active btn-danger"><span class="fa fa-trash"></span> Delete</a>
						</th>
		            </tr>
					<?php 
						}
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>