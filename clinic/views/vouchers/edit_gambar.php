<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span></span></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
          <div class="box box-info">
            <!-- /.box-header -->
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['vouchers/edittext','id'=>$model->id]),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
			 <div class="box-box v4 vform">
            <section class="sub-form">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <div class="image-preview-input">
							<?php if( $model->isNewRecord ): ?>
		                        <img src="" alt="gambar voucher" id="voucher-show">
							<?php else: ?>
		                        <img src="<?=Yii::getAlias('@uploadsWeb')?>/<?=$model->image?>" alt="gambar voucher" id="voucher-show">
							<?php endif; ?>
                        </div>
                        <label class="btn btn-warning btn-file btn-block">
                          <span class="title"><i class="fa fa-upload"></i> Upload Photo</span> 
						  <?= $form->field($model,'image')->fileInput([
							  'hidden' => true,
							  'class' => 'file-input',
							  'accept' => 'image/*',
							  'data-container' => '#voucher-show'
						  ])->label(false) ?>
                      </label>
                    </div>
                  </div>
                </div>

                <hr class="devider">

                <hr class="devider">

            </section>
        </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i> Save</button>
				  <a href="<?= Url::to(["vouchers/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>