<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span></span></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
          <div class="box box-info">
            <!-- /.box-header -->
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['vouchers/submit2']),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
			 <div class="box-box v4 vform">
            <section class="sub-form">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <div class="image-preview-input">
							<?php if( $model->isNewRecord ): ?>
		                        <img src="" alt="gambar voucher" id="voucher-show">
							<?php else: ?>
		                        <img src="<?=Yii::getAlias('@uploadsWeb')?>/<?=$model->image?>" alt="gambar voucher" id="voucher-show">
							<?php endif; ?>
                        </div>
                        <label class="btn btn-warning btn-file btn-block">
                          <span class="title"><i class="fa fa-upload"></i> Upload Photo</span> 
						  <?= $form->field($model,'image')->fileInput([
							  'hidden' => true,
							  'class' => 'file-input',
							  'accept' => 'image/*',
							  'data-container' => '#voucher-show'
						  ])->label(false) ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-8">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
							<?= $form->field($model,'name')->textInput([
								'class' => 'form-control',
								'placeholder' => 'Nama Voucher'
							])->label('Nama Voucher') ?>							
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
							<?= $form->field($model,'category_id')->dropDownList($listOfCategories)
									->label('Kategori') ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <hr class="devider">

                <div class="row narrow">
                  	<div class="col-sm-4">
	                    <div class="form-group">
							<?= $form->field($model,'real_price')->textInput([
								'class'=> 'form-control',
								'placeholder' => 'Harga Sesungguhnya'
							])->label('Harga Sesungguhnya') ?>
	                    </div>
                  	</div>
                  	<div class="col-sm-5">
	                    <div class="form-group">
							<?= $form->field($model,'voucher_price')->textInput([
								'class'=> 'form-control',
								'placeholder' => 'Harga Voucher'
							])->label('Harga Voucher') ?>							
						  </span>   
	                    </div>
                  	</div>
                  	<div class="col-sm-3">
	                    <div class="form-group">
							<?= $form->field($model,'stock')->textInput([
								'class'=> 'form-control',
								'placeholder' => 'Stock'
							])->label('Stok') ?>														
	                    </div>
                  	</div>
                </div>
				<div class="row narrow">
					<div class="col-sm-12">
                  	    <div class="form-group">
	                        <?= $form->field($model, 'expire_date')->widget(DatePicker::classname(), [
								'options' => ['placeholder' => 'Enter birth date ...'],
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'yyyy-mm-dd'
								]
							])->label("Expired Date")?>
	                    </div>
                  	</div>
				</div>
                <div class="row narrow">
                  	<div class="col-sm-7">
	                    <div class="form-group">
							<?= $form->field($model,'min_can_be_purchased')->textInput([
								'placeholder' => 'H-?',
								'class' => 'form-control',
							])->label('Min berapa hari dipesan?'); ?>
	                    </div>
                  	</div>
                  	<div class="col-sm-5">
                  		<div class="form-group">
	                        <label class="control-label">&nbsp;</label>
	                    	<span class="help">*minimal H- berapa voucher ini dapat dipesan?</span>
	                    </div>
                  	</div>
                </div>

                <hr class="devider">

                <div class="form-group">
                	<label class="control-label">Berlaku pada lokasi klinik:</label>
                	<div class="list-location v2">
						<?php foreach($detailClinic->offices as $o){?>
		                <div class="box">
			                <div class="checkbox">
			                <label>
			                <div class="row narrow">
			                    <div class="col-xs-1">
			                      	<div class="list-style">
										<input type="checkbox" name="office[]" value="<?=$o->id?>" checked>
			                      	</div>
			                    </div>
			                    <div class="col-xs-11">
				                    <div class="address">
										 <div class="short"><?= $o->name?></div>
				                        <div class="detail"><?= $o->address?></div>
				                    </div>
			                    </div>
			                </div>
			                </label>
			                </div>
		                </div>
		                <?php
							}
						?>
		            </div>
                </div>
            </section>
        </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</button>
				  <a href="<?= Url::to(["berita/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>