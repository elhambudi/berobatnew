<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

$model = new \common\models\Voucher();
$model2 = new \common\models\VoucherHasOffice();
?>

<div class="topbar" ng-controller="VoucherController">
	<div class="row narrow">
		<div class="col-sm-2">
			<div class="form-group">
				<select class="form-control">
                    <option>Semua Kategori</option>
                    <option>Dentist</option>
                    <option>Orthopedist</option>
                    <option>Pediatrician</option>
                </select>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<select class="form-control">
                    <option>Semua Status</option>
                    <option>Expired</option>
                    <option>Stok Habis</option>
                    <option>Stok Hampir Habis</option>
                </select>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group">
				<input type="text" name="" placeholder="Pencarian" class="form-control">
			</div>
		</div>
		<div class="col-sm-5">
			<div class="pull-right">
				<div class="btn btn-default">Total : <?= count($voucher)?></div>&nbsp;&nbsp;
				<button type="button" class="btn btn-primary" onclick="openRight()">Buat Voucher</button>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="row operation">
    <div class="col-sm-12 left" ng-controller="ListVoucherController">
        <table class="table table-responsive header-table tb-list-voucher">
          	<thead>
	            <tr>
	              	<th>Nama</th>
	              	<th>Kategori</th>
	              	<th class="text-center">Harga Voucher</th>
	              	<th class="text-center">In Stok</th>
	              	<th class="text-center">Expired Date</th>
	              	<th class="text-center">Status</th>
	            </tr>
          	</thead>
        </table>
        <div class="box-box v3 vtable">
            <table class="table table-responsive table-clickable tb-list-voucher">
	          	<tbody>
					<?php foreach($voucher as $v){?>
		            <tr onclick="openRight()">
		              	<td><?= $v['name']?></td>
		              	<td>Kategori</td>
		              	<td class="text-right">Rp. <?= number_format($v['voucher_price'])?></td>
		              	<td class="text-center"><?= $v['stock']?></td>
		              	<td class="text-center">
							<?php
								$date = strtotime($v['expire_date']); 
								$check_date = date( 'd-m-Y', $date );
								echo $check_date;
							?> 
						</td>
		              	<td>Expired</td>
		            </tr>
					<?php 
						}
					?>
	          	</tbody>
	        </table>
        </div>
    </div>
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="col-sm-6 right">
    	<div class="head-vform">
    		<button type="button" onclick="closeRight()" class="btn pull-right btn-default btn-sm">
    			Tutup <i class="fa fa-angle-right"></i>
    		</button>
    	</div>
        <div class="box-box v4 vform">
            <section class="sub-form">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <div class="image-preview-input">
							<?php if( $model->isNewRecord ): ?>
		                        <img src="" alt="gambar voucher" id="voucher-show">
							<?php else: ?>
		                        <img src="<?=Yii::getAlias('@uploadsWeb')?>/<?=$model->image?>" alt="gambar voucher" id="voucher-show">
							<?php endif; ?>
                        </div>
                        <label class="btn btn-warning btn-file btn-block">
                          <span class="title"><i class="fa fa-upload"></i> Upload Photo</span> 
						  <?= $form->field($model,'imageUploaded')->fileInput([
							  'hidden' => true,
							  'class' => 'file-input',
							  'accept' => 'image/*',
							  'data-container' => '#voucher-show'
						  ])->label(false) ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-8">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
							<?= $form->field($model,'name')->textInput([
								'class' => 'form-control',
								'placeholder' => 'Nama Voucher'
							])->label('Nama Voucher') ?>							
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
							<?= $form->field($model,'category_id')->dropDownList($listOfCategories)
									->label('Kategori') ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <hr class="devider">

                <div class="row narrow">
                  	<div class="col-sm-4">
	                    <div class="form-group">
							<?= $form->field($model,'real_price')->textInput([
								'class'=> 'form-control',
								'placeholder' => 'Harga Sesungguhnya'
							])->label('Harga Sesungguhnya') ?>
	                    </div>
                  	</div>
                  	<div class="col-sm-5">
	                    <div class="form-group">
							<?= $form->field($model,'voucher_price')->textInput([
								'class'=> 'form-control',
								'placeholder' => 'Harga Voucher'
							])->label('Harga Voucher') ?>							
						  </span>   
	                    </div>
                  	</div>
                  	<div class="col-sm-3">
	                    <div class="form-group">
							<?= $form->field($model,'stock')->textInput([
								'class'=> 'form-control',
								'placeholder' => 'Stock'
							])->label('Stok') ?>														
	                    </div>
                  	</div>
                </div>
				<div class="row narrow">
					<div class="col-sm-12">
                  	    <div class="form-group">
	                        <?= $form->field($model, 'expire_date')->widget(DatePicker::classname(), [
								'options' => ['placeholder' => 'Enter birth date ...'],
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'yyyy-mm-dd'
								]
							])->label("Expired Date")?>
	                    </div>
                  	</div>
				</div>
                <div class="row narrow">
                  	<div class="col-sm-7">
	                    <div class="form-group">
							<?= $form->field($model,'min_can_be_purchased')->textInput([
								'placeholder' => 'H-?',
								'class' => 'form-control',
							])->label('Min berapa hari dipesan?'); ?>
	                    </div>
                  	</div>
                  	<div class="col-sm-5">
                  		<div class="form-group">
	                        <label class="control-label">&nbsp;</label>
	                    	<span class="help">*minimal H- berapa voucher ini dapat dipesan?</span>
	                    </div>
                  	</div>
                </div>

                <hr class="devider">

                <div class="form-group">
                	<label class="control-label">Berlaku pada lokasi klinik:</label>
                	<div class="list-location v2">
						<?php foreach($detailClinic->offices as $o){?>
		                <div class="box">
			                <div class="checkbox">
			                <label>
			                <div class="row narrow">
			                    <div class="col-xs-1">
			                      	<div class="list-style">
										<input type="checkbox" name="office[]" value="<?=$o->id?>" checked>
			                      	</div>
			                    </div>
			                    <div class="col-xs-11">
				                    <div class="address">
										 <div class="short"><?= $o->name?></div>
				                        <div class="detail"><?= $o->address?></div>
				                    </div>
			                    </div>
			                </div>
			                </label>
			                </div>
		                </div>
		                <?php
							}
						?>
		            </div>
                </div>
            </section>
        </div>
        <div class="fixed-form-footer">
            <div class="pull-left">
                <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
            </div>
            <div class="pull-right">
                <button onclick="closeRight()" class="btn btn-default">Kembali</button>
                <button class="btn btn-primary btn-submit">Simpan</button>
            </div>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
</div>