<div class="row operation">
    <div class="col-sm-12 left">
        <div class="fixed-title">
            <span class="the-title">List Dokter</span>
        </div>
        <div class="box-box v2">
            <section class="sub-form">
                <div class="list-doctor">
                    <div class="box the-doctor" onclick="openRight()">
                        <div class="photo"><img src="<?= Yii::$app->request->baseUrl ?>/web/images/nurse.png"></div>
                        <div class="name">Dr. Nadia</div>
                    </div>
                    <div class="box add" onclick="openRight()">
                        <div class="icon"><i class="fa fa-user-md"></i></div>
                        <div class="explainer">TAMBAH <i class="fa fa-plus-square"></i></div>
                    </div>
                </div>
                <br>
            </section>
        </div>
    </div>
    <div class="col-sm-6 right">
        <div class="fixed-title">
            <span class="the-title">Tambah Dokter</span>
        </div>
        <div class="box-box v3 vform">
            <section class="sub-form">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <div class="image-preview-input">
                          <!-- <img src="<?= Yii::$app->request->baseUrl ?>/web/images/logo1.jpg"> -->
                          <i class="fa fa-image"></i>
                        </div>
                        <label class="btn btn-warning btn-file btn-block">
                          <span class="title"><i class="fa fa-upload"></i> Upload Photo</span> <input type="file" hidden>
                      </label>
                      <span class="help-block" ng-if=""></span>   
                    </div>
                  </div>
                  <div class="col-sm-8">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Nama *</label>
                            <input type="text" class="form-control" ng-model="" placeholder="Nama Dokter">
                          <span class="help-block" ng-if=""></span>   
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Spesialis *</label>
                            <input type="text" class="form-control" ng-model="" placeholder="Pilih spesialis (bisa lebih dari satu)">
                          <span class="help-block" ng-if=""></span>   
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Dokumen Bukti Ijin Dokter *</label><br>
                            <div class="doc-preview-input">Fileyangdiupload.pdf (sembunyikan dulu)</div><br>
                            <label class="btn btn-default btn-file">
                              <span class="title"><i class="fa fa-file"></i> Upload File</span> <input type="file" hidden>
                            </label>
                            <span class="help-inline">File yang diperbolehkan (png / jpeg / pdf)</span>
                          <span class="help-block" ng-if=""></span>   
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label">Biodata *</label>
                        <textarea rows="4" type="text" class="form-control" ng-model="" placeholder="Biodata diri dokter"></textarea>
                      <span class="help-block" ng-if=""></span>   
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="control-label">Pendidikan</label>
                      <div class="list-input">
                        <input type="text" class="input form-control" ng-model="" placeholder="Pendidikan dokter">
                        <button class="action btn btn-default"><i class="fa fa-minus"></i></button>
                        <span class="help-block" ng-if=""></span>
                      </div> 
                      <div class="list-input">
                        <input type="text" class="input form-control" ng-model="" placeholder="Pendidikan dokter">
                        <button class="action btn btn-default"><i class="fa fa-minus"></i></button>
                        <span class="help-block" ng-if=""></span>
                      </div> 
                      <div class="list-input has-error">
                        <input type="text" class="input form-control" ng-model="" placeholder="Pendidikan dokter">
                        <button class="action btn btn-default"><i class="fa fa-plus"></i></button>
                        <span class="help-block">Something error</span>
                      </div>   
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="control-label">Pengalaman</label>
                      <div class="list-input">
                        <input type="text" class="input form-control" ng-model="" placeholder="Pengalaman kerja / menangani penyakit pasien">
                        <button class="action btn btn-default"><i class="fa fa-plus"></i></button>
                        <span class="help-block" ng-if=""></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input type="email" class="form-control" ng-model="" placeholder="Email">
                      <span class="help-block" ng-if=""></span>   
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label">No Telp.</label>
                        <input type="text" class="form-control" ng-model="" placeholder="No Telp / HP">
                      <span class="help-block" ng-if=""></span>   
                    </div>
                  </div>
                </div>
            </section>
        </div>
        <div class="fixed-form-footer">
            <div class="pull-left">
                <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
            </div>
            <div class="pull-right">
                <button onclick="closeRight()" class="btn btn-default">Kembali</button>
                <button class="btn btn-primary btn-submit">Simpan</button>
            </div>
        </div>
    </div>
</div>