<div class="row">
    <div class="col-md-6">
         <div class="fixed-title">
            <span class="the-title">Profil Publik</span>
        </div>
        <div class="box-box v3">
            <section class="sub-form">
                <form role="form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Nama Klinik</label>
                                <input type="text"  class="form-control" placeholder="Nama klinik" />					
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>No. Telp</label>
                                <input type="text" class="form-control" placeholder="No Telp."/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Dokumen Bukti Ijin Klinik</label><br>
                                <div class="doc-preview-input">
                                        <a href="http://localhost:88/work/berobat_id/api2/web/uploads/{{ detailClinic.proof_of_permission }}">Klik untuk melihat</a>
                                    </div><br>
                                <label class="btn btn-default btn-file">
                                  <span class="title"><i class="fa fa-file"></i> Ganti Dokumen</span> 
                                  <input type="file" hidden upload-file file-model="proofOfClinic">
                                </label>
                                <span class="help-inline">File yang diperbolehkan (png / jpeg / pdf)</span>
                            </div>
                        </div>
                    </div>
                    
                    <hr class="devider"></hr>			
                    	
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Logo Klinik</label>
							    <div class="image-preview-input" ng-if="detailClinic.logo">
							    	<img ng-if="detailClinic.logo" ng-src="<?= "http://localhost:88/work/berobat_id/api2" ?>/web/uploads/{{detailClinic.logo}}">
							    </div>                                    
                                <label class="btn btn-default btn-file btn-block">
                                    <span class="title"><i class="fa fa-upload"></i> Upload Image</span> 
                                    <input type="file" upload-file file-model="clinicLogo" accept="image/*" id="logo-clinic" hidden>    
                                </label>
                                <span class="help-block"></span>       
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Tentang Klinik Anda</label>
                                        <textarea rows="5" type="text" class="form-control" placeholder="Ceritakan mengenai klinik anda">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="range-input-wrap">
                                        <div class="first">
                                            <div class="form-group">
                                                <label class="control-label">Jam operasi kerja</label>
                                                <input type="text" class="form-control" placeholder="Mulai">
                                            </div>
                                        </div>
                                        <div class="between">-</div>
                                        <div class="last">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <input type="text" class="form-control" placeholder="Berakhir">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            

                    <hr class="devider"></hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>NPWP</label>
                                <input type="text" class="form-control" placeholder="No NPWP" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Bank Account</label>
                                <div> <!-- tinggal kasih ng-repeat="dataNewBank in listOfNewBank" -->
                                    <div class="list-input">
                                        <div class="input">
                                            <div class="row narrow">
                                                <div class="col-sm-4">
                                                    <select class="form-control">
                                                        <option ng-repeat="detailBank in listOfBank">
                                                            {{ detailBank.name }}
                                                        </option>
                                                    </select>
                                                    <span class="help-block" ng-if=""></span>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" placeholder="No Rekening" class="form-control" />
                                                    <span class="help-block" ng-if=""></span>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" placeholder="Atas Nama" class="form-control" />
                                                    <span class="help-block" ng-if=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="action btn btn-default"><i class="fa fa-minus"></i></button>
                                    </div> 
                                </div>
                                <div class="list-input">
                                    <div class="input">
                                        <div class="row narrow">
                                            <div class="col-sm-4">
                                                <select class="form-control">
                                                    <option ng-repeat="detailBank in listOfBank">
                                                        {{ detailBank.name }}
                                                    </option>
                                                </select>
                                                <span class="help-block" ng-if=""></span>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" placeholder="No Rekening" class="form-control" />
                                                <span class="help-block" ng-if=""></span>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" placeholder="Atas Nama" class="form-control" />
                                                <span class="help-block" ng-if=""></span>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="action btn btn-default" ng-click="addBankAccount()"><i class="fa fa-plus"></i></button>
                                </div> 
                            </div>
                        </div>
                    </div>		
                </form>
            </section>
        </div>
        <div class="fixed-form-footer">
            <div class="pull-right">
                <button class="btn btn-primary btn-submit">Simpan</button>
            </div>
        </div>
    </div>
</div>