<div class="row operation" ng-controller="ClinicDetailController" style="overflow:scroll;">
    <section  ng-controller="LokasiKlinikController">
    <div class="col-sm-6 left">
        <div class="fixed-title">
            <span class="the-title">List Lokasi</span>
            <button onclick="openRight()" class="btn btn-primary pull-right btn-sm mb-5">Tambah Lokasi</button>
        </div>
        <div class="box-box v2">
            <section class="sub-form">
              <div class="list-location">
                <div class="box" onclick="openRight()" ng-repeat="detail in listOffice">
                  <div class="row narrow">
                    <div class="col-xs-1">
                      <div class="list-style"><i class="fa fa-map-marker"></i></div>
                    </div>
                    <div class="col-xs-10">
                      <div class="address">
                        <div class="short">Kebun Jeruk - Tanggerang</div>
                        <div class="detail">	{{detail.address}} </div>
                      </div>
                    </div>
                    <div class="col-xs-1 text-center">
                      <div class="action"><div class="btn btn-sm btn-danger" ng-click="remove($index)">x</div></div>
                    </div>
                  </div>
                </div>                
              </div>
            </section>
        </div>
    </div>
    <div class="col-sm-6 right" style="display:block">
        <form ng-submit="submit(form)" role="form">      
          <div class="fixed-title">
              <span class="the-title">Tambah Lokasi</span>
          </div>
          <div class="box-box v3 vform">
            <section class="sub-form">
                  <div class="row narrow">
                    <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">Negara</label>
                          <select class="form-control"
                          ng-model="country"
                          ng-change="locationChange(1,country)"
                          ng-options="m.id as m.name for m in listCountry"
                          ></select>               
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">Provinsi</label>
                           <select class="form-control" ng-model="province"
                            ng-change="locationChange(2,province)"
                            ng-selected="$first">
                              <option 
                              value="{{ detailProvince.id }}" ng-repeat="detailProvince in listProvince">
                                {{ detailProvince.name }}
                              </option>
                            </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                          <label class="control-label">Kota</label>
                          <select class="form-control" ng-model="form.city_id">
                            <option 
                            value="{{ detailCity.id }}" ng-repeat="detailCity in listCity">
                              {{ detailCity.name }}
                            </option>							
                          </select>
                          <label class="label label-danger" ng-if="errors.city_id">
                            {{ errors.city_id }}
                          </label>
                      </div>
                    </div>
                  </div>
                  <div class="row narrow">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label class="control-label">Latitude</label>
                        <input type="text" class="form-control" ng-model="form.latitude" ng-readonly="true" />
                        <label class="label label-danger" ng-if="errors.latitude">
                          {{ errors.latitude }}
                        </label>								
                      </div>
                    </div>						
                    <div class="col-md-6">
                      <div class="form-group">
                          <label class="control-label">Latitude</label>
                        <input type="text" class="form-control" ng-model="form.longitude" ng-readonly="true" />
                        <label class="label label-danger" ng-if="errors.longitude">
                          {{ errors.longitude }}
                        </label>								
                      </div>
                    </div>												
                  </div>		                  
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                          <label class="control-label">Detail Alamat / Jalan</label>
                          <textarea rows="3" type="text" class="form-control" ng-model="form.address" placeholder="Detail alamat"></textarea>								
                          <label class="label label-danger" ng-if="errors.address">
                            {{ errors.address }}
                          </label>	
                      </div>
                    </div>
                  </div>
                  <br />
                <div class="row">
                  <div class="col-sm-12">
                    <div class="btn btn-primary btn-submit" ng-click="getLatLng(form.address)">
                      LatLng
                    </div>																	
                  </div>					
                </div>                  
                  <br />
                  <div class="row">
                    <div class="box"  ng-repeat="detail in listResult">
                      <div class="row narrow">
                        <div class="col-xs-1">
                          <div class="list-style"><i class="fa fa-map-marker"></i></div>
                        </div>
                        <div class="col-xs-10">
                          <div class="address">
                            <div class="short">City - Provinsi</div>
                            <div class="detail">
                              {{detail.formatted_address}}
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-1">
                          <div class="action">
                            <div class="btn btn-sm btn-success" ng-click="applyLocation($index)">V</div>
                          </div>
                        </div>
                      </div>
                    </div>					
                  </div>    
                  <br />              
                  <div class="row">
                    <div class="col-sm-12">
        							<div class="address-maps" id="map" style="height:200px;"></div>
                    </div>
                  </div>
            </section>
          </div>
          <div class="fixed-form-footer">
              <div class="pull-left">
                  <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
              </div>
              <div class="pull-right">
                  <div onclick="closeRight()" class="btn btn-default">Kembali</div>
                  <button class="btn btn-primary btn-submit">Simpan</button>
              </div>
          </div>
        </form>
    </div>
    </section>
</div>