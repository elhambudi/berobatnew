<?php

use yii\widgets\ActiveForm;
use common\models\Clinic;
use yii\helpers\Url;

$isHasFlashError = Yii::$app->getSession()->hasFlash('error');
if( $isHasFlashError ){
    $data = Yii::$app->getSession()->getFlash('error');
}
$clinicDetail = Clinic::findOne(['user_id' => Yii::$app->user->identity->id]);
?>
<div class="row">
    <div class="col-md-6">
        <div class="box-box">
            <section class="sub-form">
                <div class="title for-main">
                    <span class="the-title">Foto Akun</span>
                </div>
                <div class="form-group">
                <img src="<?= Yii::getAlias('@uploadsWeb') ?>/<?= !is_null($clinicDetail) ? $clinicDetail->logo : "" ?>" class="img-circle" style="width:300; height:300;" alt="User Image">
                </div>
                <div class="form-group">
                    <?php $form = yii\widgets\ActiveForm::begin([
                        'action' => Url::to(['account/editprofil','id'=>$model->id]),
                        'method' => 'POST',
        				'options' => ['enctype' => 'multipart/form-data'],
        			 ]);  ?>
                    
					<?= $form->field($uploadModel,'uploadedFile')->fileInput([
							'class' => "form-control",
							'accept' => 'image/jpg,image/jpeg,image/png,application/pdf'
						])->label('Gambar Profil') ?>
					<div class="form-group pull-right">
                        <button id="account-btn" type="submit" class="btn btn-primary btn-submit">
                            Simpan
                        </button>
                    </div>
					<?php ActiveForm::end();?>
				</div>
            </section>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box-box">
            <section class="sub-form">
                <div class="title for-main">
                    <span class="the-title">Pemilik Akun</span>
                </div>
                    <?php $form = ActiveForm::begin(['id' => 'form1']);?>
                    <?php if( $isHasFlashError ): ?>
                    <div class="alert alert-<?= $data['error'] ? 'danger' : 'success' ?>">
                        <?= $data['msg'] ?>
                    </div>
                    <?php endif; ?>
                    
                    <div class="form-group">
                        <?= $form->field($clinicModel,'name_contact')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'Nama pemilik klinik'
                        ])->label('Nama') ?>
                        <?= $form->field($clinicModel,'name_clinic')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'Nama klinik'
                        ])->label('Nama Klinik') ?>
                        <?= $form->field($clinicModel,'phone')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'Phone pemilik klinik'
                        ])->label('Phone') ?>
                        <?= $form->field($clinicModel,'npwp')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'NPWP pemilik klinik'
                        ])->label('NPWP') ?>
                        <?= $form->field($clinicModel,'working_hour_start')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'jam mulai klinik'
                        ])->label('Jam Buka Klinik') ?>
                        <?= $form->field($clinicModel,'working_hour_end')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'jam selesai klinik'
                        ])->label('Jam Tutup Klinik') ?>
                        <?= $form->field($clinicModel,'description')->textarea([
                            'class' => 'form-control',
                            'placeholder' => 'deskripsi klinik',
                            'rows' => '6'
                        ])->label('Deskripsi Klinik') ?>
                    </div>

                    <div class="form-group pull-right">
                        <button id="account-btn" type="submit" class="btn btn-primary btn-submit">
                            Simpan
                        </button>
                    </div>       
                    <div class="clear" />

                    <?php ActiveForm::end(); ?>
                        <hr class="devider"></hr>                        
                    <?php $form = ActiveForm::begin(['id' => 'form1']);?>

                    <div class="form-group">
                        <label>Ganti Password</label>
                        <?= $form->field($changePasswordModel,'oldpass')->passwordInput([
                            'class' => 'form-control',
                            'placeholder' => 'Password lama'
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($changePasswordModel,'newpass')->passwordInput([
                                    'class' => 'form-control',
                                    'placeholder' => 'Password lama'
                                ]) ?>
                            </div>
                            <div class="col-md-6">						
                                <?= $form->field($changePasswordModel,'repeatnewpass')->passwordInput([
                                    'class' => 'form-control',
                                    'placeholder' => 'Password lama'
                                ]) ?>
                            </div>
                        </div>
                    </div>				
                    
                    <hr class="devider"></hr>

                    <div class="form-group pull-right">
                        <button id="account-btn" type="submit" class="btn btn-primary btn-submit">
                            Simpan
                        </button>
                    </div>
                <?php ActiveForm::end();?>
            </section>
        </div>
    </div>
</div>