<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\date\DatePicker;
$this->title = "Situs Beli Voucher Treatment Kesehatan Online Mudah Dan Terpecaya";
/* @var $this yii\web\View */
?>
<?php
function TanggalIndo($date){
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

	$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
	return($result);
}
?>
<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span></span></div>
			</div>
		<div class="row">
		<div class="col-xs-12">
          <div class="box box-info">
            <!-- /.box-header -->
			<div class="box-body">
			    <?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
				?>
              <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['sales/index']),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
				]);  ?>
                <div class="form-group">
					<div class="col-sm-3">
						<?php
							echo DatePicker::widget([
								'name' => 'tanggal1',
								'options' => [
									'placeholder' => 'Dari Tanggal ...',
									'required'=>true,
									'class' => 'form-control'
								],
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'yyyy-mm-dd'
								]
							]);
						?>
					</div>
					<div class="col-sm-3">
						<?php
							echo DatePicker::widget([
								'name' => 'tanggal2',
								'options' => [
									'placeholder' => 'Sampai Tanggal ...',
									'required'=>true,
								],
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'yyyy-mm-dd'
								]
							]);
						?>
					</div>
					<div class="col-sm-3">
						<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
					</div>
                </div>
              <?php yii\widgets\ActiveForm::end(); ?>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
				  <th>Nama Pengguna</th>
				  <th>No. Telepon</th>
				  <th>Kode Booking</th>
                  <th>Status Bayar</th>
                  <th>Tanggal Beli</th>
                  <th>Tanggal Penggunaan</th>
                  <th>Status Penggunaan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
					<?php 
						if($data != NULL){
							$i=1;
							foreach($data as $v){
					?>
					<tr>
						<th><?=$i++?></th>
						<th><?php
								$user = \common\models\User::findOne($v['user_id']);
								//echo $data_user->username;
								echo $user->username;
						?></th>
						?></th>
						<th><?php
								$user = \common\models\User::findOne($v['user_id']);
								//echo $data_user->username;
								echo $user->no_telepon;
						?></th>
						<th><?=$v['code']?></th>
						<th><?php
							if($v['status']==0){
								echo '<span class="label label-danger">Belum Dibayar</span>';
							}else{
								echo '<span class="label label-success">Sudah Dibayar</span>';
							}?></th>
						<th><?php echo TanggalIndo($v['created_date']);?></th>
						<th>
						    <?php echo TanggalIndo($v['date_used']);?>
						</th>
						<th><?php
							if($v['status_validate']==0){
								echo '<span class="label label-danger">Belum Digunakan</span>';
							}else{
								echo '<span class="label label-success">Sudah Digunakan</span>';
							}?></th>
						<th>
							<?php
								if($v['status_validate']==0){
							?>
						   <a href="<?=Url::to(['vouchers/validate','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-check"></span> Validate</a>
						   <?php
								}elseif($v['status_validate']==1){
						   ?>
						   <!--<a href="<?=Url::to(['vouchers/unvalidate','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-danger"><span class="fa fa-check"></span> Unvalidate</a>-->
						   <a type="button" class="btn active btn-success"><span class="fa fa-check"></span> Validated</a>
						   <?php
							}
						   ?>
						</th>
					</tr>
					<?php
							}
						}
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>
</div>