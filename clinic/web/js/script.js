function openRight(){
	$(".operation .left").removeClass('col-sm-12');
	$(".operation .left").addClass('col-sm-6');
	$(".operation .right").fadeIn('slow');
}
function closeRight(){
	$(".operation .left").removeClass('col-sm-6');
	$(".operation .left").addClass('col-sm-12');
	$(".operation .right").fadeOut('slow');
}

jQuery( document ).ready(function( $ ) {
	/*-----------------------------------------------------------------------------------*/
	/* 	WINDOW
	/*-----------------------------------------------------------------------------------*/

	$('.box-box').perfectScrollbar({
		suppressScrollX:true,
		wheelPropagation:true,
		swipePropagation:true,
		wheelSpeed:0.5,
	});  
	var window_width = $(window).width();
	var window_height = $(window).height();
	$(window).resize(function() {
  		window_width = $(window).width();
		window_height = $(window).height();
	});

	var lastScrollTop = 0;
	$(window).scroll(function(event){
		setTimeout(function(){ 
			$("header").toggleClass("sticky", ($(window).scrollTop() > 5));
		}, 600);
	   	// var st = $(this).scrollTop();
	   	// if (st > lastScrollTop){
	    //    $("body").toggleClass("down", ($(window).scrollTop() > 100));
	   	// } else {
	    // 	$("body").removeClass("down");
	   	// }
	   	// lastScrollTop = st;
	});
	/*-----------------------------------------------------------------------------------*/
	/* 	END WINDOW
	/*-----------------------------------------------------------------------------------*/

	/*-----------------------------------------------------------------------------------*/
	/* 	LOADER
	/*-----------------------------------------------------------------------------------*/
	$("#loader").delay(500).fadeOut("slow");
	$("#to-register").click(function() {
	    $('html, body').animate({
	        scrollTop: $("#ret-regis").offset().top
	    }, 2000);
	});
	/*-----------------------------------------------------------------------------------*/
	/* 	END LOADER
	/*-----------------------------------------------------------------------------------*/

	/*-----------------------------------------------------------------------------------*/
	/* 	HOVER LOKASI
	/*-----------------------------------------------------------------------------------*/
	var timeoutId;
	$(".lokasi").hover(function(){
	    if (!timeoutId) {
	        timeoutId = window.setTimeout(function() {
	            timeoutId = null; // EDIT: added this line
	            $("#lokasi-opsi-wrapper").fadeIn('fast');
				$(".js-flyout-overlay").fadeIn('fast');
	       }, 600);
	    }
	},
	function () {
	    if (timeoutId) {
	        window.clearTimeout(timeoutId);
	        timeoutId = null;
	    }
	    else {
	       $("#lokasi-opsi-wrapper").fadeOut('fast');
			$(".js-flyout-overlay").fadeOut('fast');
	    }
	});
	// $(".lokasi").hover(function(){
	// 	setTimeout(function(){
	// 		$("#lokasi-opsi-wrapper").show();
	// 		$(".js-flyout-overlay").show();
	// 	},2000);
	// },function(){
	// 	$("#lokasi-opsi-wrapper").hide();
	// 	$(".js-flyout-overlay").hide();
	// });
	/*-----------------------------------------------------------------------------------*/
	/* 	END HOVER LOKASI
	/*-----------------------------------------------------------------------------------*/

	/*-----------------------------------------------------------------------------------*/
	/* 	MODAL ACTION
	/*-----------------------------------------------------------------------------------*/
	$(".iframe-register").click(function(){
		$("#login-panel").modal('hide');
		$("#register-panel").modal('show');
		setTimeout(function(){
			$('body').addClass('modal-open');
		},500);
	});
	$(".iframe-login").click(function(){
		$("#register-panel").modal('hide');
		$("#login-panel").modal('show');
		setTimeout(function(){
			$('body').addClass('modal-open');
		},500);
	});
	/*-----------------------------------------------------------------------------------*/
	/* 	END MODAL ACTION
	/*-----------------------------------------------------------------------------------*/
});