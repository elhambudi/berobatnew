<?php

namespace clinic\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\OrderVoucher;
use common\models\Voucher;
use common\models\Clinic;
use common\models\User;

class SalesController extends \yii\web\Controller
{
    public function actionIndex()
    {
		$postData= Yii::$app->request->post();
		//var_dump($postData);die();
		$clinic = Clinic :: find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
		foreach($clinic as $c){
			$id_clinic = $c['id'];
		}
        //$user = User::find()->where(['id'=>$id_clinic])->all();
        $voucher = Voucher::find()->where(['clinic_id'=>$id_clinic])->all();
        
		//var_dump($id_clinic);die();
		if($voucher==NULL){
			//var_dump('kosong');die();
			$data = NULL;
		}else{
			foreach($voucher as $v){
				$data_voucher[]=$v['id'];
			}
			
			
			
			if($postData==NULL){
				$data = OrderVoucher::find()->where(['in','voucher_id',$data_voucher])->andWhere(['status'=>1])->all();
		
			}else{
				$data = OrderVoucher::find()->where(['in','voucher_id',$data_voucher])->andWhere(['status'=>1])->andWhere(['between', 'created_date', $postData['tanggal1'], $postData['tanggal2'] ])->all();
				
			}
		}
		 return $this->render('index',[
			'data' => $data ,
		]);
    }
	
	public function actionValidate()
    {
		$postData= Yii::$app->request->post();
		$clinic = Clinic :: find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
		foreach($clinic as $c){
			$id_clinic = $c['id'];
		}
        $voucher = Voucher::find()->where(['clinic_id'=>$id_clinic])->all();
		foreach($voucher as $v){
			$data_voucher[]=$v['id'];
		}
		if($postData==NULL){
			$data = OrderVoucher::find()->where(['in','voucher_id',$data_voucher])->andWhere(['status_validate'=>1])->all();
		}else{
			$data = OrderVoucher::find()->where(['in','voucher_id',$data_voucher])->andWhere(['status_validate'=>1])->andWhere(['between', 'created_date', $postData['tanggal1'], $postData['tanggal2'] ])->all();
		}
		return $this->render('validate',[
			'data' => $data ,
		]);
    }

    public function actionListSales()
    {
        return $this->render('list-sales');
    }

    public function actionReport()
    {
        return $this->render('report');
    }
}
