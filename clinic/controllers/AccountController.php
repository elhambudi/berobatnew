<?php

namespace clinic\controllers;
use yii\web\UploadedFile;
use Yii;
use common\models\User;
use common\models\Clinic;
use common\models\UploadForm;

class AccountController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->redirect(['owner']);
    }

    public function actionOwner()
    {
        $userModel = User::findOne( Yii::$app->user->identity->id );        
        $clinicModel = Clinic::findOne(['user_id' => $userModel->id]);
        $uploadModel = new UploadForm();
        
        $changePassModel = new \common\models\ChangePassword();

        if( Yii::$app->request->post() ){
            $errors = [
                'error' => false,
                'msg' => "Data berhasil diubah"
            ];
            $postData = Yii::$app->request->post();
                       

            if( isset($postData['Clinic']) ){
                $clinicModel->load($postData);
                $clinicModel->save();
                Yii::$app->getSession()->setFlash('error',$errors);                
            }else{
                $changePassModel->load($postData);
                if( $changePassModel->validate() ){
                    $userModel->password_hash = Yii::$app->security->generatePasswordHash($changePassModel->newpass);
                    $userModel->save();                    
                    Yii::$app->getSession()->setFlash('error',$errors);
                }
            }

            if( Yii::$app->getSession()->hasFlash('error') ){
                return $this->redirect(['owner']);
            }
        }

        return $this->render('owner',[
            'model' => $userModel,
            'clinicModel' => $clinicModel,
            'changePasswordModel' => $changePassModel,
            'uploadModel' => $uploadModel,
        ]);
    }

    public function actionEditprofil($id) {
        $userModel = User::findOne( Yii::$app->user->identity->id );        
        $clinicModel = Clinic::findOne(['user_id' => $userModel->id]);
			if (Yii::$app->request->isPost) {
				$uploadModel = new UploadForm();
				$uploadModel->uploadedFile = UploadedFile::getInstance($uploadModel, 'uploadedFile');
				$name = $uploadModel->upload();
				//var_dump($model);die();
				if( $name ){
					$clinicModel->logo = $name;
					$clinicModel->save();
					
					return $this->redirect(['index']);                                  
				}else{
					$errors['upload'] = "Upload Gagal";                                      
				}
			} else {
			   //var_dump( $model->errors);die();
			}
			var_dump( "gagal");
               die();
           return $this->redirect(['owner']);
    }

    public function actionPublic()
    {
        return $this->render('public');
    }

    public function actionDoctors()
    {
        return $this->render('doctors');
    }

    public function actionClinicLocation()
    {
        return $this->render('clinic-location');
    }

	public function actionGalleries()
    {
        return $this->render('galleries');
    }    
}
