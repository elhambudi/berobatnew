<?php

namespace clinic\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\OrderVoucher;
use common\models\Voucher;
use common\models\Clinic;
use common\models\User;

class CustomersController extends \yii\web\Controller
{
    public function actionIndex()
    {
		$clinic = Clinic :: find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
		foreach($clinic as $c){
			$id_clinic = $c['id'];
		}
        $voucher = Voucher::find()->where(['clinic_id'=>$id_clinic])->all();
		if($voucher==NULL){
			$user ==NULL;
		}else{
			foreach($voucher as $v){
				$data_voucher[]=$v['id'];
			}
			$data = OrderVoucher::find()->where(['in','voucher_id',$data_voucher])->andWhere(['status'=>1])->all();
			foreach($data as $d){
				$id_user[] = $d['user_id'];
			}
			$user = User::find()->where(['in','id',$id_user])->all();
		}
		//var_dump($user);die();
        return $this->render('index',[
			'user' =>$user,
		]);
    }

    public function actionListCustomers()
    {
        return $this->render('list-customers');
    }
}
