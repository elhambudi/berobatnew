<?php

namespace clinic\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\Employee;
use common\models\Clinic;
use common\models\Office;
use common\models\User;

class EmployeesController extends \yii\web\Controller
{
    public function actionIndex()
    {
		
		$list_clinic = Clinic::find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
		foreach($list_clinic as $li){
			$list_office = Office::find()->where(['clinic_id'=>$li['id']])->all();
		}
		//var_dump($list_office);die();
		foreach($list_office as $l){
			$data = Employee::find()->where(['office_id'=>$l['id']])->all();
		}
		//$data = Employee::find()->all();
		$count_data = Employee::find()->count();
		//$office = \common\models\Office::find()->all();
        return $this->render('index',[
			'data' => $data ,
			'count_data' => $count_data ,
			'office' => $list_office
		]);
        //return $this->redirect(['list-employees']);
    }

    public function actionListEmployees()
    {
		$data = Employee::find()->all();
		$count_data = Employee::find()->count();
		$office = \common\models\Office::find()->all();
        return $this->render('list-employees',[
			'data' => $data ,
			'count_data' => $count_data ,
			'office' => $office
		]);
    }
	
	public function actionActivate($id){
		$model = User :: findOne($id);
		$model->status =1;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Employee has been Activated.');
		return $this->redirect(['employees/index']);
	}

	public function actionUnactivate($id){
		$model = User :: findOne($id);
		$model->status =0;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Employee not been Activated.');
		return $this->redirect(['employees/index']);
	}
	public function actionTambah()
    {
		$model = new Employee();
		$list_clinic = Clinic::find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
		foreach($list_clinic as $li){
			$office = Office::find()->where(['clinic_id'=>$li['id']])->all();
		}
		//$office = \common\models\Office::find()->all();
        return $this->render('tambah',[
			'office' => $office,
			'model' => $model,
		]);
    }
	public function actionEditform($id)
    {
		$model = Employee::findOne($id);
		$model2 = User::findOne($model['user_id']);
		$office = \common\models\Office::find()->all();
		$id_user = $model2['id'];
		$id_employee = $model['id'];
        return $this->render('edit',[
			'office' => $office,
			'model' => $model,
			'model2' => $model2,
			'id_user' => $id_user,
			'id_employee' => $id_employee,
		]);
    }

	public function actionDaftar(){
		$model = new User();
		$username = Yii::$app->request->post('User');
        $model->username = $username['email'];
        $model->email = $username['email'];
        $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash('initial');
		//var_dump($model->password_hash);die();
        $model->generateAuthKey();
		$model->role_id = 4;
		$model->status=1;
		$model->created_at=11111;
		$model->updated_at=11111;
		$model->save();
		if($model->save()){
		   $modelmember = new Employee();
		   $member = Yii::$app->request->post('Employee');
		   //var_dump($member);die();
		   $modelmember->name = $member['name'];
		   $modelmember->phone = $member['phone'];
		   $modelmember->office_id = $member['office_id'];
		   $modelmember->user_id = $model->getPrimaryKey();
		   $modelmember->save();
		}else{
			var_dump( $model->errors);
		   die();
		}
		Yii::$app->session->setFlash('success', 'Data Pegawai baru berhasil ditambahkan.');
		return $this->redirect(['index']);
	}
	public function actionEdit($id_user,$id_employee){
		$model = User::findOne($id_user);
		$username = Yii::$app->request->post('User');
        $model->username = $username['email'];
        $model->email = $username['email'];
        $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash('initial');
		//var_dump($model->password_hash);die();
        $model->generateAuthKey();
		$model->role_id = 4;
		$model->status=0;
		$model->created_at=11111;
		$model->updated_at=11111;
		$model->save();
		if($model->save()){
		   $modelmember = Employee::findOne($id_employee);
		   $member = Yii::$app->request->post('Employee');
		   //var_dump($member);die();
		   $modelmember->name = $member['name'];
		   $modelmember->phone = $member['phone'];
		   $modelmember->office_id = $member['office_id'];
		   $modelmember->user_id = $model->getPrimaryKey();
		   $modelmember->save();
		}else{
			var_dump( $model->errors);
		   die();
		}
		Yii::$app->session->setFlash('success', 'Data Pegawai baru berhasil diubah.');
		return $this->redirect(['index']);
	}
	public function actionDelete($id_user,$id_employee)
    {
        $this->findModel($id_employee)->delete();
        $this->findModel2($id_user)->delete();
		Yii::$app->getSession()->setFlash('success', '<i class="fa fa-trash"></i> Data Pegawai berhasil di hapus.');
        return $this->redirect(['index']);
    }
	
	protected function findModel($id)
    {
        if (($model = Employee::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            echo "error";
        }
    }
	protected function findModel2($id)
    {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            echo "error";
        }
    }
}
