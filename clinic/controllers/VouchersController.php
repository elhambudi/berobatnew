<?php

namespace clinic\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\Voucher;
use common\models\VoucherHasOffice;
use common\models\Office;
use common\models\Clinic;
use common\models\Category;
use common\models\CategoryHasClinic;
use common\models\OrderVoucher;
use yii\web\UploadedFile;
class VouchersController extends \yii\web\Controller
{
    public function actionIndex()
    {	
		$clinic = Clinic::findOne(['user_id'=>Yii::$app->user->identity->id]);
		$office = Office::find()->where(['clinic_id'=>$clinic['id']])->all();
		$id_office = array();
		foreach($office as $o){
			$id_office[] = $o['id'];
		}
		$listOfVoucher = Voucher::find()->where(['clinic_id' => $clinic->id])->all();

		$listOfCategories = [];
		foreach( $clinic->categories as $v ){
			$listOfCategories[$v->id] = $v->name;
		}
        return $this->render('index',[
			'voucher' => $listOfVoucher,
			//'model' => $voucherModel,
			'listOfCategories' => $listOfCategories
		]);
    }

    public function actionListVouchers($id = null)
    {
		//Mendapatkan List Voucher
		$clinic = Clinic::findOne(['user_id'=>Yii::$app->user->identity->id]);
		
		if( is_null($id) ){
			$voucherModel = new Voucher();
		}else{
			$voucherModel = Voucher::findOne($id);
		}

		if( Yii::$app->request->post() ){
			$post = Yii::$app->request->post();
			$voucherModel->load($post);
			$voucherModel->created_date = date('Y-m-d');
			$voucherModel->discount = (($voucherModel->real_price-$voucherModel->voucher_price)/$voucherModel->real_price*100).'';
			$voucherModel->clinic_id = $clinic->id;
			$voucherModel->upload();
			if( $voucherModel->save() ){
				if( isset($post['office']) ){
					foreach( $post['office'] as $k => $v ){
						$voucherHasOffice = new \common\models\VoucherHasOffice();
						$voucherHasOffice->office_id = $v;
						$voucherHasOffice->voucher_id = $voucherModel->id;
						$voucherHasOffice->save();
					}
				}
				$this->redirect('list-vouchers');
			}else{
				var_dump($voucherModel->errors);
				die();
			}

		}

		$office = Office::find()->where(['clinic_id'=>$clinic['id']])->all();
		$id_office = array();
		foreach($office as $o){
			$id_office[] = $o['id'];
		}
		$listOfVoucher = Voucher::find()->where(['clinic_id' => $clinic->id])->all();

		$listOfCategories = [];
		foreach( $clinic->categories as $v ){
			$listOfCategories[$v->id] = $v->name;
		}

        return $this->render('list-vouchers',[
			'detailClinic' => $clinic,
			'voucher' => $listOfVoucher,
			'model' => $voucherModel,
			'listOfCategories' => $listOfCategories
		]);
    }

    public function actionRedeem()
    {
        return $this->render('redeem');
    }
	public function actionTambah()
    {
		$model = new Voucher();
		$clinic = Clinic::findOne(['user_id'=>Yii::$app->user->identity->id]);
		$listOfCategories = [];
		foreach( $clinic->categories as $v ){
			$listOfCategories[$v->id] = $v->name;
		}
        return $this->render('tambah',[
			'model'=>$model,
			'listOfCategories' => $listOfCategories,
			'detailClinic' => $clinic,
		]);
    }
	
	public function actionEditform($id)
    {
		$model = Voucher::findOne($id);
		$clinic = Clinic::findOne(['user_id'=>Yii::$app->user->identity->id]);
		$listOfCategories = [];
		foreach( $clinic->categories as $v ){
			$listOfCategories[$v->id] = $v->name;
		}
        return $this->render('edit',[
			'model'=>$model,
			'listOfCategories' => $listOfCategories,
			'detailClinic' => $clinic,
		]);
    }
	public function actionEditform1($id)
    {
		$model = Voucher::findOne($id);
		$clinic = Clinic::findOne(['user_id'=>Yii::$app->user->identity->id]);
		$listOfCategories = [];
		foreach( $clinic->categories as $v ){
			$listOfCategories[$v->id] = $v->name;
		}
        return $this->render('edit_gambar',[
			'model'=>$model,
			'listOfCategories' => $listOfCategories,
			'detailClinic' => $clinic,
		]);
    }
	
	public function actionSubmit2()
    {
		$model = new Voucher();
		$clinic = Clinic::findOne(['user_id'=>Yii::$app->user->identity->id]);
		if (Yii::$app->request->isPost) {
				if ($model->load(Yii::$app->request->post())) {
					$uploadModel = UploadedFile::getInstance($model, 'image');
					if ($uploadModel) {
						$namaLogo = $uploadModel->baseName;
						if ($uploadModel->extension == "jpg" || $uploadModel->extension == "jpeg" || $uploadModel->extension == "png") {
							$uploadModel->saveAs('../../site/web/voucher_photo/' . $namaLogo . '.' . $uploadModel->extension);
							$model->image = $namaLogo . '.' . $uploadModel->extension;
							$model->created_date = date('Y-m-d');
							$model->discount = (($model->real_price-$model->voucher_price)/$model->real_price*100).'';
							$model->clinic_id = $clinic->id;
							$model->status=0;
						} else {
							return $this->render('index', [
								'model' => $model,
							]);
						}
					}
					
					if ($model->save()) {         
						//var_dump('test');die();
						if( isset($post['office']) ){
							foreach( $post['office'] as $k => $v ){
								$voucherHasOffice = new \common\models\VoucherHasOffice();
								$voucherHasOffice->office_id = $v;
								$voucherHasOffice->voucher_id = $voucherModel->id;
								$voucherHasOffice->save();
							}
						}
						Yii::$app->session->setFlash('success', 'Data Voucher baru berhasil ditambah.');
						return $this->redirect('index');
					} else {                    
						var_dump($model->errors);die();
					}
				}
			} else {
				var_dump($model->errors);die();
			}
		
        return $this->redirect(['tambah']);
    }
	public function actionEdittext($id)
    {
		$model = Voucher::findOne($id);
		$clinic = Clinic::findOne(['user_id'=>Yii::$app->user->identity->id]);
		if (Yii::$app->request->isPost) {
				if ($model->load(Yii::$app->request->post())) {
					$uploadModel = UploadedFile::getInstance($model, 'image');
					if ($uploadModel) {
						$namaLogo = $uploadModel->baseName;
						if ($uploadModel->extension == "jpg" || $uploadModel->extension == "jpeg" || $uploadModel->extension == "png") {
							$uploadModel->saveAs('../../site/web/voucher_photo/' . $namaLogo . '.' . $uploadModel->extension);
							$model->image = $namaLogo . '.' . $uploadModel->extension;
							$model->created_date = date('Y-m-d');
							$model->discount = (($model->real_price-$model->voucher_price)/$model->real_price*100).'';
							$model->clinic_id = $clinic->id;
						} else {
							return $this->render('index', [
								'model' => $model,
							]);
						}
					}
					
					if ($model->save()) {         
						//var_dump('test');die();
						Yii::$app->session->setFlash('success', 'Data Voucher baru berhasil diubah.');
						return $this->redirect('index');
					} else {                    
						var_dump($model->errors);die();
					}
				}
			} else {
				var_dump($model->errors);die();
			}
		
        return $this->redirect(['tambah']);
    }
	/*public function actionSubmit()
    {
		$voucherModel = new Voucher();
		$clinic = Clinic::findOne(['user_id'=>Yii::$app->user->identity->id]);
		if( Yii::$app->request->post() ){
			$post = Yii::$app->request->post();
			$voucherModel->load($post);
			$voucherModel->created_date = date('Y-m-d');
			$voucherModel->discount = (($voucherModel->real_price-$voucherModel->voucher_price)/$voucherModel->real_price*100).'';
			$voucherModel->clinic_id = $clinic->id;
			$voucherModel->upload();
			if( $voucherModel->save() ){
				if( isset($post['office']) ){
					foreach( $post['office'] as $k => $v ){
						$voucherHasOffice = new \common\models\VoucherHasOffice();
						$voucherHasOffice->office_id = $v;
						$voucherHasOffice->voucher_id = $voucherModel->id;
						$voucherHasOffice->save();
					}
				}
				$this->redirect('list-vouchers');
			}else{
				var_dump($voucherModel->errors);
				die();
			}

		}
		Yii::$app->session->setFlash('success', 'Data Voucher baru berhasil ditambah.');
        return $this->redirect(['tambah']);
    }
	*/
	public function actionEdit($id)
    {
		$voucherModel = Voucher::findOne($id);
		$clinic = Clinic::findOne(['user_id'=>Yii::$app->user->identity->id]);
		if( Yii::$app->request->post() ){
			$post = Yii::$app->request->post();
			$voucherModel->load($post);
			$voucherModel->created_date = date('Y-m-d');
			$voucherModel->discount = (($voucherModel->real_price-$voucherModel->voucher_price)/$voucherModel->real_price*100).'';
			$voucherModel->clinic_id = $clinic->id;
			$voucherModel->upload();
			if( $voucherModel->save() ){
				if( isset($post['office']) ){
					foreach( $post['office'] as $k => $v ){
						$voucherHasOffice = new \common\models\VoucherHasOffice();
						$voucherHasOffice->office_id = $v;
						$voucherHasOffice->voucher_id = $id;
						$voucherHasOffice->save();
					}
				}
				$this->redirect('list-vouchers');
			}else{
				var_dump($voucherModel->errors);
				die();
			}

		}
		Yii::$app->session->setFlash('success', 'Data Voucher baru berhasil diubah.');
        return $this->redirect(['index']);
    }
	
	public function actionDelete($id)
    {
        $this->findModel($id)->delete();
		Yii::$app->getSession()->setFlash('success', '<i class="fa fa-trash"></i> Data Voucher berhasil di hapus.');
        return $this->redirect(['index']);
    }
	
	protected function findModel($id)
    {
        if (($model = Voucher::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            echo "error";
        }
    }
    public function actionValidate($id_voucher){
		$model = OrderVoucher :: findOne($id_voucher);
		$model->status_validate =1;
		$model->validate_date =date("Y-m-d");
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher has been Validated.');
		return $this->redirect(['sales/index']);
	}
}	
