-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 10, 2017 at 11:10 AM
-- Server version: 5.6.37
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u4939585_berobat3`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `gambar` text,
  `description2` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `description`, `gambar`, `description2`) VALUES
(1, '<p>Berobat.id adalah sebuah platform kesehatan di bidang pelayanan voucher kesehatan. </p><p>Di Berobat.id, Anda akan mendapatkan berbagai macam voucher kesehatan dengan harga terjangkau. </p><p>Berobat.id akan memberikan informasi detail meliputi harga voucher, fasilitas klinik dan pelayanan yang diberikan.</p><p>Berbagai kategori yang ditawarkan diantaranya gigi, kandungan, jantung, bedah, anak, kulit dan kecantikan, paru, bedah plastik, mata, THT, penyakit dalam serta layanan laboratorium.</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `aturan_penggunaan`
--

CREATE TABLE `aturan_penggunaan` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `gambar` text,
  `description2` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aturan_penggunaan`
--

INSERT INTO `aturan_penggunaan` (`id`, `description`, `gambar`, `description2`) VALUES
(1, '<p style=\"text-align: center;\"><strong>Informasi Umum</strong></p><ul style=\"list-style: none;\"><li>Berobat.id adalah platform penyedia voucher pelayanan kesehatan menjamin keamanan dan kenyamanan member dan klinik partner</li><li><span></span>Berobat.id yakni perantara antara member dan klinik partner dan membantu mengamankan proses pembayaran. Pengguna tidak akan dikenakan biaya tambahan tertentu jika tidak ada dalam informasi keterangan</li><li>Berobat.id tidak bertanggung jawab atas buruknya pelayanan dari klinik partner atau perselisihan antara member dengan klinik partner</li><li>Berobat.id memiliki hak untuk mengambil tindakan kepada akun yang terindikasi melakukan penyalahgunaan, manipulasi ataupun melanggar Aturan Penggunaan.</li><li>Berobat.id berhak meminta data-data pribadi Member dan Klinik Partner jika diperlukan</li><li>Aturan Penggunaan di Berobat.id dapat berubah nsewaktu-waktu tanpa pemberitahuan terlebih dahulu.</li><li>Member dan Klinik Partner dianggap menyetujui perubahan Aturan Penggunaan di Berobat.id</li><li>Hati-hati terhadap penipuan yang mengatasnamakan berobat.id atau PT Tekno Biomedik Indonesia</li></ul><p style=\"text-align: center;\"><strong>Member</strong></p><ul style=\"list-style: none;\"><li>Member wajib mengisi data pribadi secara lengkap dan jujur pada saat mendaftar sebagai member</li><li><span></span>Member bertanggung jawab atas keamanan akun yakni email dan password</li><li>Penggunaan layanan berobat.id menunjukkan bahwa Member telah memahami dan menyetujui segala aturan yang ada di Berobat.id</li><li>Member dilarang menyampaikan segala jenis konten yang bersifat menyesatkan, memfitnah, mengolok, pencemaran nama baik, bersinggungan dengan SARA, diskriminasi atau hal-hal yang menyudutkan pihak tertentu</li><li>Member tidak diperbolehkan melanggar aturan yang ditetapkan oleh hukum yang berlaku di Indonesia</li><li>Member akan mendapatkan beragam informasi promo terbaru dari berobat.id. Namun member dapat berhenti berlangganan (unsubscribe) jika tidak ingin menerima informasi tersebut</li><li>Member dilarang menggunakan kata-kata kasar yang tidak sesuai norma, baik saat berdiskusi ataupun memberikan komentar kepada Klinik. Berobat.id akan melakukan pembekuan atau penonaktifkan Akun</li></ul>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `name`) VALUES
(1, 'Bank Central Asia'),
(2, 'BNI46'),
(3, 'BRI');

-- --------------------------------------------------------

--
-- Table structure for table `bank_admin`
--

CREATE TABLE `bank_admin` (
  `id` int(11) NOT NULL,
  `bank` text NOT NULL,
  `name` text NOT NULL,
  `no_rekening` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_admin`
--

INSERT INTO `bank_admin` (`id`, `bank`, `name`, `no_rekening`) VALUES
(3, 'BCA (Bank Central Asia)', 'PT Tekno Biomedik Indonesia', '333333333333'),
(4, 'Bank Mandiri', 'PT Tekno Biomedik Indonesia', '141 000 12561 55');

-- --------------------------------------------------------

--
-- Table structure for table `bank_clinic`
--

CREATE TABLE `bank_clinic` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `number` varchar(25) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_clinic`
--

INSERT INTO `bank_clinic` (`id`, `name`, `number`, `bank_id`, `clinic_id`) VALUES
(1, 'Suryo Jatmiko', '1563016616', 2, 2),
(2, 'guci keren', '868162491826491', 1, 22);

-- --------------------------------------------------------

--
-- Table structure for table `benefit`
--

CREATE TABLE `benefit` (
  `id` int(11) NOT NULL,
  `judul` text NOT NULL,
  `description` text NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `benefit`
--

INSERT INTO `benefit` (`id`, `judul`, `description`, `gambar`) VALUES
(2, 'Branding', 'Berobat.id akan menawarkan dan memperkenalkan berbagai produk yang dimiliki oleh Klinik Partner', 'hospital.png'),
(3, 'Get New Customer', 'Klinik Partner akan mendapatkan pelanggan baru', 'hospital.png'),
(4, 'Payment', 'Kemudahan pembayaran bagi pelanggan dan klinik partner dengan metode Auto Payment', 'hospital.png'),
(5, 'No Risk', 'Berobat.id menjamin segala proses pembelian dan pembayaran aman', 'hospital.png');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` text,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `image`, `description`) VALUES
(1, 'Umum', 'health-10.png', NULL),
(2, 'Gigi', 'health-2.png', NULL),
(3, 'THT', 'health-3.png', NULL),
(4, 'Kulit & Kecantikan', 'health-1.png', NULL),
(5, 'Tulang Ortopedi', 'tulang.png', NULL),
(6, 'Jantung', 'jantung.png', NULL),
(7, 'Kandungan', 'Bedah.png', NULL),
(8, 'Urologi', 'health-8.png', NULL),
(9, 'Anak', 'Anak.png', NULL),
(10, 'Bedah', 'Kandungan.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_has_clinic`
--

CREATE TABLE `category_has_clinic` (
  `category_id` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_has_clinic`
--

INSERT INTO `category_has_clinic` (`category_id`, `clinic_id`) VALUES
(2, 2),
(4, 2),
(1, 8),
(2, 8),
(1, 14),
(2, 14),
(3, 14),
(4, 14),
(1, 18),
(2, 18),
(3, 18),
(4, 18),
(5, 18),
(6, 18),
(7, 18),
(8, 18),
(9, 18),
(10, 18),
(2, 19),
(2, 20),
(1, 21),
(2, 21),
(3, 21),
(4, 21),
(5, 21),
(6, 21),
(7, 21),
(8, 21),
(9, 21),
(10, 21),
(2, 22),
(5, 23),
(7, 24),
(9, 24);

-- --------------------------------------------------------

--
-- Table structure for table `category_has_doctor`
--

CREATE TABLE `category_has_doctor` (
  `doctor_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_has_doctor`
--

INSERT INTO `category_has_doctor` (`doctor_id`, `category_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `province_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `province_id`) VALUES
(1, 'Surabaya', 1),
(2, 'Sidoarjo', 1),
(3, 'Bandung', 2);

-- --------------------------------------------------------

--
-- Table structure for table `clinic`
--

CREATE TABLE `clinic` (
  `id` int(11) NOT NULL,
  `name_contact` varchar(100) NOT NULL,
  `name_clinic` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `logo` text,
  `npwp` varchar(100) DEFAULT NULL,
  `status_approval` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` text,
  `proof_of_permission` text,
  `working_hour_start` time DEFAULT NULL,
  `working_hour_end` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clinic`
--

INSERT INTO `clinic` (`id`, `name_contact`, `name_clinic`, `phone`, `logo`, `npwp`, `status_approval`, `created_date`, `user_id`, `description`, `proof_of_permission`, `working_hour_start`, `working_hour_end`) VALUES
(2, 'Clinic 2123', 'Clinic in', '089639874801', 'logo3.jpg', '123123213', 0, '2017-04-13', 45, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.k', NULL, '13:30:15', '13:30:15'),
(8, 'Clinic 2', 'Clinic 2', '0818181', 'logo2.jpg', NULL, 1, '2017-05-08', 12, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '59100ce5d5e6b.png', NULL, NULL),
(13, 'Clinic 3', 'Clinic 3', '123123', 'logo3.jpg', NULL, 0, '2017-05-17', 28, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '591bd015b0aca.pdf', NULL, NULL),
(14, 'Clinic 4', 'Clinic 4', '081203812038', 'logo4.jpg', NULL, 1, '2017-05-24', 29, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '592508b13cb52.jpg', NULL, NULL),
(15, 'Clinic 5', 'Clinic 5', '0818181', 'logo2.jpg', NULL, 1, '2017-05-08', 12, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '59100ce5d5e6b.png', NULL, NULL),
(16, 'Clinic 6', 'Clinic 6', '0818181', 'logo2.jpg', NULL, 1, '2017-05-08', 12, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '59100ce5d5e6b.png', NULL, NULL),
(17, 'Clinic 7', 'Clinic 7', '0818181', 'logo2.jpg', NULL, 1, '2017-05-08', 12, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '59100ce5d5e6b.png', NULL, NULL),
(18, 'Test', 'Clinic 5', '1234', '59886c92dfb131502112914.jpg', NULL, 0, '2017-07-20', 58, 'Test', '5970692d57dcf1500539181.pdf', NULL, NULL),
(19, 'Clinic 10', 'Clinic 10', '123456', '59886c92dfb131502112914.jpg', NULL, 1, '2017-08-01', 65, 'asd', '59801be9a01fc1501567977.jpg', NULL, NULL),
(20, 'Ruben', 'Mamora', '087891636123', '59886c92dfb131502112914.jpg', NULL, 0, '2017-08-02', 66, 'detail', '5981916855bfa1501663592.jpg', NULL, NULL),
(21, 'Elham', 'Clinic Mulut', '081223333967', '59886c92dfb131502112914.jpg', NULL, 1, '2017-08-02', 67, 'Test', '5981afc13cc811501671361.png', NULL, NULL),
(22, 'Oki', 'Klinik Oke', '08122211178', '59886c92dfb131502112914.jpg', '758263960361531', 0, '2017-08-07', 68, 'zfafgkae', '59886c1dc21301502112797.jpg', '08:00:00', '20:30:00'),
(23, 'ELham', 'Clinik Jantung', '12345678', NULL, NULL, 0, '2017-09-18', 71, 'Klinik Jantung', '59bf3f8aec43d1505705866.jpg', NULL, NULL),
(24, 'Andik', 'Klinik Ayah Bunda', '0317672049', NULL, NULL, 1, '2017-09-18', 72, 'vfhj', '59bf4313ab7b91505706771.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clinic_photo`
--

CREATE TABLE `clinic_photo` (
  `id` int(11) NOT NULL,
  `url` text NOT NULL,
  `clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clinic_photo`
--

INSERT INTO `clinic_photo` (`id`, `url`, `clinic_id`) VALUES
(1, 'logo1.jpg', 2),
(2, 'logo1.jpg', 8),
(3, 'logo1.jpg', 13),
(4, 'logo1.jpg', 14);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `key_config` varchar(100) NOT NULL,
  `content_type` int(11) NOT NULL,
  `order_config` int(11) NOT NULL,
  `config_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config_category`
--

CREATE TABLE `config_category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `photo` text,
  `bio` text,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `proof_of_permission` text,
  `education` text,
  `experience` text,
  `clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `name`, `photo`, `bio`, `email`, `phone`, `proof_of_permission`, `education`, `experience`, `clinic_id`) VALUES
(1, 'asdasda', '5970514d9bff11500533069.jpg', 'asdjashdg', 'asdjghasdjhg@sagdsa.cm', '21321537', '5970514d9c4281500533069.jpg', 'sajsdajhdg', 'asdgjashdg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `phone`, `user_id`, `office_id`) VALUES
(1, 'MAMATA', '0812314123', 7, 2),
(2, 'a', '1212', 46, 2),
(3, 'test', '12345', 47, 2),
(4, 'a', '123', 48, 2),
(5, 'askdhjasdh', '12321', 49, 7),
(6, 'elhambudianto', '1234', 59, 4),
(7, 'elham', '12345', 60, 2),
(8, 'employee', '1234', 61, 2),
(9, 'elham', '123', 64, 2);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `description`) VALUES
(1, '<p>Q : Apa itu berobat.id ?</p><p>A : Berobat.id adalah sebuah platform kesehatan yang menyediakan voucher kesehatan dengan harga terjangkau.</p><p><br></p><p>Q : Kapan Berobat.id didirikan ?<br></p><p>A: Berobat.id berdiri pada awal tahun 2017<span class=\"redactor-invisible-space\"><br></span></p><p><br></p><p><span class=\"redactor-invisible-space\">Q : Apa keuntungan membeli voucher di berobat.id ?<span class=\"redactor-invisible-space\"><br></span></span></p><p>A : Anda akan mendapatkan harga voucher yang lebih murah dibandingkan dengan harga pasaran. Anda juga dapat melakukan booking .Selain itu, anda juga dapat melakukan re-schedule jika berhalangan. Anda juga dapat mengetahui fasilitas dan produk dari partner.</p><p><br></p><p>Q : Apakah ada syarat untuk re-schedule ?<br></p><p>A: Re-schedule dapat dilakukan 1x24 jam sebelum jadwal seharusnya<span class=\"redactor-invisible-space\"><br></span></p><p><br></p><p><span class=\"redactor-invisible-space\">Q: Bagaimana jika saya melakukan re-schedule lebih dari 1x24 jam ?<span class=\"redactor-invisible-space\"><br></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\">A: Mohon maaf Anda tidak bisa melakukan re-schedule<span class=\"redactor-invisible-space\"><br></span></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><br></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\">Q: Apakah bisa saya mewakilkan voucher tersebut kepada orang lain ?<span class=\"redactor-invisible-space\"><br></span></span></span></span></p><p>A: Maaf Anda tidak bisa mewakilkan voucher tersebut kepada orang lain, karena saat penukaran voucher harus disertai dengan kartu identitas yang masih berlaku.</p><p><br></p><p>Q : Bagaimana cara memesan voucher di berobat.id ?</p><p>A : Anda harus memiliki akun terlebih dahulu di berobat.id<span class=\"redactor-invisible-space\"><br></span></p><p><br></p><p><span class=\"redactor-invisible-space\">Q : Apakah ada biaya untuk menjadi member berobat.id ?<span class=\"redactor-invisible-space\"><br></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\">A : Pendaftaran di berobat.id bersifat Gratis / Tidak dikenakan biaya apapun<span class=\"redactor-invisible-space\"><br></span></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><br></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\">Q : Apakah saya harus menentukan tanggal booking di klinik tersebut ?<span class=\"redactor-invisible-space\"><br></span></span></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\">A : Ya, Anda diwajibkan menentukan tanggal kunjungan ke klinik agar memudahkan klinik.<span class=\"redactor-invisible-space\"><br></span></span></span></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><br></span></span></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\">Q : Bagaimana cara pembayaran ?<span class=\"redactor-invisible-space\"><br></span></span></span></span></span></span></p><p>A : Anda akan mendapatkan informasi nomor rekening Berobat.id dan nominal pembayaran (akan ada nomor unik). Kemudian anda harus melakukan verivikasi pembayaran. Dalam waktu 1x24 jam, voucher akan dikirimkan ke email Anda.<span class=\"redactor-invisible-space\"></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"></span></span></span></span></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"></span></span></span></span></p><p><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"></span></span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `id` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `alamat` text NOT NULL,
  `email` text NOT NULL,
  `telepon` text NOT NULL,
  `fax` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `deskripsi`, `alamat`, `email`, `telepon`, `fax`) VALUES
(1, '<p style=\"text-align: justify;\">Berobat.id adalah sebuah platform kesehatan di bidang pelayanan voucher kesehatan. Di Berobat.id, Anda akan mendapatkan berbagai macam voucher kesehatan dengan harga terjangkau. Berobat.id akan memberikan informasi detail meliputi harga voucher, fasilitas klinik dan pelayanan yang diberikan. Berbagai kategori yang ditawarkan diantaranya gigi, kandungan, jantung, bedah, anak, kulit dan kecantikan, paru, bedah plastik, mata, THT, penyakit dalam serta layanan laboratorium. </p>', '<p>Raya Bangkingan No 5B, Surabaya</p>', 'berobatid@gmail.com', '+62 822 3102 3315', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `value` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `value`) VALUES
(1, 'Male'),
(2, 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `gambar` text,
  `description2` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `help`
--

INSERT INTO `help` (`id`, `description`, `gambar`, `description2`) VALUES
(1, '<p>Jika Anda mengalami permasalahan pada layanan website Berobat.id.<br></p><p>Dapat mengirimkan email ke berobatid@gmail.com.<br></p><p>Email Anda akan dibalas dalam waktu 2x24 Jam</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kebijakan_privasi`
--

CREATE TABLE `kebijakan_privasi` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `gambar` text,
  `description2` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kebijakan_privasi`
--

INSERT INTO `kebijakan_privasi` (`id`, `description`, `gambar`, `description2`) VALUES
(1, '<p style=\"text-align: justify;\">Kebijakan privasi yang dimaksud adalah acuan yang melindungi serta mengatur penggunaan data dan informasi penting Member dan Klinik Partner Berobat.id . Data dan informasi yang telah dikumpulkan pada saat mendaftar, mengakses dan menggunakan berobat,id seperti alamat, nomor handphone, alamat, email, dan lain lain.<strong><br></strong></p><p style=\"text-align: justify;\"><strong>Kebijakan-kebijakan tersebut diantaranya :</strong></p><p style=\"text-align: justify;\">- Berobat.id tunduk terhadap kebijakan perlindungan data Pribadi Member yang diatur dalam Peraturan Menteri Komunikasi dan Informatika Nomor 20 Tahun 2016 tentang Perlindungan Data Pribadi dalam Sistem Elektronik yang mengatur dan melindungi penggunaan data dan informasi penting Member dan Klinik Partner</p><p style=\"text-align: justify;\">- Berobat.id melindungi segala informasi yang diberikan Member dan Klinik Partner saat mendaftar, mengakses dan menggunakan layanan Berobat.id</p><p style=\"text-align: justify;\">- Berobat.id melindungi segala hak pribadi yang muncul atas informasi mengenai suatu produk yang ditampilkan oleh Klinik Partner baik berupa foto, username, logo, harga, dan lain-lain<span class=\"redactor-invisible-space\"></span><span class=\"redactor-invisible-space\"></span><span class=\"redactor-invisible-space\"><br></span></p><p style=\"text-align: justify;\"><span class=\"redactor-invisible-space\">- Berobat.id berhak menggunakan data dan informasi Member serta Klinik Partner demi meningkatkan pelayanan Berobat.id<span class=\"redactor-invisible-space\"></span></span><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"></span></span><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><br></span></span></p><p style=\"text-align: justify;\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\">- Berobat.id hanya dapat memberikan informasi serta data Member dan Klinik Partner bila diwajibkan dan atau diminta oleh institusi yang berwenang berdasarkan hukum yang berlaku, perintah resmi pengadilan, atau perintah resmi dari instansi atau aparat yang bersangkutan<span class=\"redactor-invisible-space\"></span></span></span><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"></span></span></span><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><br></span></span></span></p><p style=\"text-align: justify;\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\"><span class=\"redactor-invisible-space\">- Member dapat melakukan unsubscribe dari berbagai macam informasi penawaran dan promo dari berobat.id jika tidak ingin menerima informasi tersebut<span class=\"redactor-invisible-space\"></span></span></span></span></p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `list_transaction`
--

CREATE TABLE `list_transaction` (
  `id` int(11) NOT NULL,
  `kode_transaksi` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_voucher` int(11) NOT NULL,
  `kode_pembayaran` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_transaction`
--

INSERT INTO `list_transaction` (`id`, `kode_transaksi`, `id_user`, `id_voucher`, `kode_pembayaran`) VALUES
(1, '001', 38, 20, NULL),
(2, '1', 38, 21, NULL),
(3, '001', 38, 22, NULL),
(4, '2', 38, 24, NULL),
(5, '3', 38, 25, NULL),
(6, '4', 38, 26, NULL),
(7, '5', 38, 27, NULL),
(8, '6', 70, 28, NULL),
(9, '7', 70, 29, NULL),
(10, '8', 74, 30, NULL),
(11, '9', 74, 31, NULL),
(12, '10', 74, 32, NULL),
(13, '11', 74, 33, NULL),
(14, '12', 74, 34, NULL),
(15, '13', 74, 35, NULL),
(16, '14', 74, 36, NULL),
(17, '15', 74, 37, NULL),
(18, '16', 74, 38, NULL),
(19, '17', 74, 39, NULL),
(20, '18', 76, 40, NULL),
(21, '19', 76, 41, NULL),
(22, '20', 76, 42, NULL),
(23, '21', 76, 43, NULL),
(24, '22', 38, 44, NULL),
(25, '23', 90, 45, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logo_header`
--

CREATE TABLE `logo_header` (
  `id` int(11) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logo_header`
--

INSERT INTO `logo_header` (`id`, `gambar`) VALUES
(1, 'logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `birtdate` date NOT NULL,
  `city_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `name`, `birtdate`, `city_id`, `user_id`, `gender_id`) VALUES
(1, 'MAtZZZ', '1990-02-01', 2, 10, 1),
(2, 'asdsad', '2017-06-15', 1, 44, 2),
(3, 'Elham', '2017-07-27', 1, 50, 1),
(4, 'asd', '2017-07-29', 1, 51, 2),
(5, 'asd', '2017-07-29', 1, 52, 2),
(6, 'Elham', '2017-07-27', 1, 53, 1),
(7, 'elham', '2017-07-25', 1, 54, 1),
(8, 'user1@gmail.com', '2017-07-25', 1, 55, 2),
(9, 'budi12@gmail.com', '2017-07-20', 1, 56, 2),
(10, 'elhambudianto', '2017-07-24', 1, 57, 2),
(11, 'elham', '2017-08-15', 1, 62, 2),
(12, 'Elham Budianto', '2017-08-07', 1, 63, 1),
(13, 'Anggrian Riska', '1992-09-22', 1, 70, 2),
(14, 'Elham Budi Anto', '2017-10-17', 1, 74, 1),
(15, 'Alfin Maulana Fikri', '2017-10-10', 1, 75, 1),
(16, 'Alfin Maulana Fikri', '2017-10-03', 1, 76, 1),
(17, 'Alfin Maulana Fikri', '2017-10-03', 1, 77, 1),
(18, 'Alfin Maulana Fikri', '2017-10-10', 1, 78, 1),
(19, 'Alfin Maulana Fikri', '2017-10-04', 1, 79, 1),
(20, 'Alfin Maulana Fikri', '2017-10-04', 3, 80, 1),
(21, 'Alfin Maulana Fikri', '2017-10-11', 1, 81, 1),
(22, 'Alfin Maulana Fikri', '2017-10-09', 3, 82, 1),
(23, 'Taufik Arifuddin', '2017-09-28', 3, 83, 1),
(24, 'Taufik Arifuddin', '2017-10-25', 1, 84, 2),
(25, 'Taufik Arifuddin', '2017-10-20', 3, 85, 2),
(26, 'Taufik Arifuddin', '2017-10-20', 3, 86, 2),
(27, 'Ruben Tri Simamora', '1993-03-05', 1, 87, 1),
(28, 'Alfin Maulana Fikri', '2017-10-26', 1, 88, 1),
(29, 'Crash Magrish', '2017-10-12', 1, 89, 2),
(30, 'Alfin Maulana Fikri', '2017-10-04', 3, 90, 1),
(31, 'Alfin Maulana Fikri', '2017-10-13', 2, 91, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1492049547),
('m130524_201442_init', 1492049551);

-- --------------------------------------------------------

--
-- Table structure for table `office`
--

CREATE TABLE `office` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `address` text NOT NULL,
  `longitude` float(11,8) DEFAULT NULL,
  `latitude` float(10,8) DEFAULT NULL,
  `clinic_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `office`
--

INSERT INTO `office` (`id`, `name`, `address`, `longitude`, `latitude`, `clinic_id`, `city_id`) VALUES
(2, 'Kantor 1', 'Jl Sudirman', NULL, NULL, 2, 2),
(4, 'Kantor 2', 'address', NULL, NULL, 13, 2),
(6, 'Kantor 3', 'Surabaya', 112.75209045, -7.25747204, 14, 1),
(7, 'Kantor 4', 'Jl Gunawangsa', NULL, NULL, 2, 3),
(8, NULL, 'Test', NULL, NULL, 18, 1),
(9, NULL, 'Test', NULL, NULL, 19, 3),
(10, NULL, 'kalijudan', NULL, NULL, 20, 3),
(11, NULL, 'Buduran', NULL, NULL, 21, 1),
(12, NULL, 'Surabaya', NULL, NULL, 22, 1),
(13, NULL, 'Kedungturi', NULL, NULL, 23, 1),
(14, NULL, 'Kusuma Bangsa 12', NULL, NULL, 24, 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_voucher`
--

CREATE TABLE `order_voucher` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `grand_total` varchar(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=belum dibayar,1=sudah dibayar',
  `status_validate` tinyint(2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `validate_date` date DEFAULT NULL,
  `image` text,
  `date_used` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_voucher`
--

INSERT INTO `order_voucher` (`id`, `code`, `grand_total`, `status`, `status_validate`, `user_id`, `voucher_id`, `created_date`, `validate_date`, `image`, `date_used`) VALUES
(1, 'dYmQA', NULL, 1, 1, 38, 1, '2017-07-25 00:00:00', '2017-10-04', 'bukti.png', '2017-10-04'),
(2, 'ceSGL', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(3, 'xLuKl', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(4, 'PJXxG', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(5, 'LirIo', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(6, 'wlfiD', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(7, 'ZrCsR', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(8, 'EetSB', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(9, 'fRFoY', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(10, 'fQVvK', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(11, 'rRcWa', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(12, 'cXflM', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(13, 'cgmjx', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(14, 'DcNdW', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(15, 'ncThS', NULL, 0, 0, 38, 4, '2017-07-25 00:00:00', NULL, NULL, NULL),
(16, 'MGEsF', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(17, 'DWmwP', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(18, 'rAspT', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(19, 'yXfKZ', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(20, 'UeTSH', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(21, 'dRQkq', NULL, 0, 0, 38, 1, '2017-07-25 00:00:00', NULL, NULL, NULL),
(22, 'UIhsy', NULL, 0, 0, 38, 3, '2017-07-27 00:00:00', NULL, NULL, NULL),
(23, 'thHAo', NULL, 0, 0, 38, 4, '2017-07-31 00:00:00', NULL, NULL, NULL),
(24, 'KCafW', NULL, 0, 0, 38, 4, '2017-07-31 00:00:00', NULL, NULL, NULL),
(25, 'ZFLdi', NULL, 1, 0, 38, 15, '2017-08-02 00:00:00', NULL, '5981b2ec605a41501672172.jpg', NULL),
(26, NULL, NULL, 0, 0, 38, 15, '2017-08-18 00:00:00', NULL, NULL, NULL),
(27, NULL, NULL, 0, 0, 38, 12, '2017-09-12 00:00:00', NULL, '59b7f3286ad421505227560.jpg', NULL),
(28, 'zwQjM', NULL, 1, 1, 70, 12, '2017-09-18 00:00:00', NULL, NULL, '2017-09-29'),
(29, 'zsZYC', NULL, 1, 1, 70, 17, '2017-09-18 00:00:00', '2017-10-04', '59bf441641dc51505707030.png', '2018-01-31'),
(30, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-04'),
(31, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-19'),
(32, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-18'),
(33, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-19'),
(34, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-25'),
(35, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-10'),
(36, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-26'),
(37, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-12'),
(38, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-23'),
(39, NULL, NULL, 0, 0, 74, 15, '2017-10-03 00:00:00', NULL, NULL, '2017-10-18'),
(40, NULL, NULL, 0, 0, 76, 14, '2017-10-04 00:00:00', NULL, NULL, '2017-06-26'),
(41, NULL, NULL, 0, 0, 76, 14, '2017-10-04 00:00:00', NULL, NULL, NULL),
(42, NULL, NULL, 0, 0, 76, 14, '2017-10-04 00:00:00', NULL, NULL, '2017-10-07'),
(43, NULL, NULL, 0, 0, 76, 14, '2017-10-05 00:00:00', NULL, NULL, '2017-08-30'),
(44, NULL, NULL, 0, 0, 38, 12, '2017-10-07 00:00:00', NULL, NULL, '2017-06-29'),
(45, 'lOvcq', NULL, 1, 0, 90, 12, '2017-10-07 00:00:00', NULL, '59d860ac4455d1507352748.png', '2017-06-15');

-- --------------------------------------------------------

--
-- Table structure for table `order_voucher_has_voucher`
--

CREATE TABLE `order_voucher_has_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pcounter_save`
--

CREATE TABLE `pcounter_save` (
  `save_name` varchar(10) NOT NULL,
  `save_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pcounter_save`
--

INSERT INTO `pcounter_save` (`save_name`, `save_value`) VALUES
('counter', 30),
('day_time', 2458036),
('max_count', 4),
('max_time', 1506340800),
('yesterday', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pcounter_users`
--

CREATE TABLE `pcounter_users` (
  `user_ip` varchar(255) NOT NULL,
  `user_time` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pcounter_users`
--

INSERT INTO `pcounter_users` (`user_ip`, `user_time`) VALUES
('6716eb3527fbe67fc1579b4e27f3713e', 1507524152);

-- --------------------------------------------------------

--
-- Table structure for table `program_clinic`
--

CREATE TABLE `program_clinic` (
  `id` int(11) NOT NULL,
  `judul` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_clinic`
--

INSERT INTO `program_clinic` (`id`, `judul`, `description`) VALUES
(1, 'Program Klinik Partner', '<p>Berobat.id adalah sebuah platform kesehatan pertama di bidang pelayanan kesehatan.<br></p><p>Menawarkan berbagai macam voucher pelayanan kesehatan dengan harga terjangkau.<br></p><p>Masyarakat akan dimudahkan untuk mengetahui harga pelayanan kesehatan, fasilitas serta kualitas dari voucher yang dipilih.<span class=\"redactor-invisible-space\"></span><br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`id`, `name`, `country_id`) VALUES
(1, 'Jawa Timur', 1),
(2, 'Jawa Barat', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'CMS'),
(3, 'Clinic'),
(4, 'Employee'),
(5, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `gambar` text NOT NULL,
  `nama` text,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `gambar`, `nama`, `description`) VALUES
(2, 'slide1.jpg', NULL, NULL),
(3, 'slide2.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id` int(11) NOT NULL,
  `facebook` text,
  `instagram` text,
  `twitter` text,
  `youtube` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `facebook`, `instagram`, `twitter`, `youtube`) VALUES
(1, 'https://www.facebook.com/berobat.id/', 'https://www.instagram.com/berobat.id/', 'http://twitter.com', 'http://youtube.com');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_has_voucher_code`
--

CREATE TABLE `transaction_has_voucher_code` (
  `transaction_id` int(11) NOT NULL,
  `voucher_code_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_telepon` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `gambar` text COLLATE utf8_unicode_ci,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `role_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `no_telepon`, `gambar`, `status`, `role_id`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '1', '1', '1', '', NULL, 10, 1, 1, 1),
(2, 'matius.official@gmail.com', 'k5BQI1ZgmVEUxd13AFV--xjWkYn_KKzg', '$2y$13$jibWUYddLsXPkmOuRFuyBu73Z/TV5cG1B5NVvlyytb/8WZQv53KeK', NULL, 'matius.official@gmail.com', '', NULL, 0, 3, 1492059904, 1495024758),
(7, 'MAMAT123@mail.com', 'ovfBQhvEMVem3UPCfvvyrKRZFUq3_3Fi', '$2y$13$TdCaS4cL.hgTSPbLLx0avO80Cpb8J2/D3UqtSyZLvyJenCo1l2kuO', NULL, 'MAMAT123@mail.com', '', NULL, 0, 4, 11111, 11111),
(10, 'kempel_com@yahoo.com', 'CimQ0fB6gwgQhp5EDfnELIAG1v1HlDDL', '$2y$13$vObbf6TctUNWlM3ANlGViucWEgziUGaOh63a.oyNQtAYoOUNGXNJ.', NULL, 'kempel_com@yahoo.com', '', NULL, 1, 5, 1492666680, 1492667201),
(12, 'test@gmail.com', '_Y37Pajxr0F4WMs1aQsi4NKnsEJtYVNI', '$2y$13$mJK88iRP1uaNa8TLTtkDx.6Dbx/6Z2k1QNCfZ3pMLqQKXUeMk7LJe', NULL, 'test@gmail.com', '', NULL, 1, 1, 1494224102, 1496218015),
(28, 'coba@gamil.com', 'lGT7VqdcitEWj5Cx9NqVKUnvm4NafpZh', '$2y$13$ysmr5bMEkPpk3QBGq0SQWeAMn8WhaKq8CVamoGuOXX.ZJx8g673X.', NULL, 'coba@gamil.com', '', NULL, 0, 3, 1494994996, 1495025454),
(29, 'contac@gmail.com', '2dvAnD1nz9SU9v55yaEvGbz_7D7sQYTe', '$2y$13$Ry6a19XVrVU1NWIvTnEegun30tjRj7cmgY21O2ipn34hGl8AVUETi', NULL, 'contac@gmail.com', '', NULL, 1, 3, 1495599282, 1496214968),
(37, 'admin@gmail.com', 'i96hlM7SgwKB1d94pnJ9OZd1vSWOdcMK', '$2y$13$i5eL2Mj3QF5d6WoxJhA1HuWK6gKqr7aubW6D2712xkV8Em4RbMPkS', NULL, 'admin@gmail.com', '', NULL, 1, 1, 11111, 11111),
(38, 'Berobat', 'YpZAyvF3SvkOMsV4PZMkXBjRPFFqR2iB', '$2y$13$JlOXPEzCnJQ/CI81cScfV.1frQXzHY.l3AT7xHLZKgJPEMUrjtZR6', NULL, 'member@gmail.com', '', '59b7f1a9c76bf1505227177.jpg', 1, 5, 11111, 11111),
(42, 'admin12121@gmail.com', 'IjwGRrVCPAiBakMr-4CKLiju5NgyJ4vm', '$2y$13$Fx2voxKV26zctWR/OnLXQOnQjZVsR4zcuWr0RKanIANky7MDCs8fK', NULL, 'admin12121@gmail.com', '', NULL, 0, 2, 11111, 11111),
(43, 'admin1212@admin.com', 'pVrcV00Q7P4uSNPzIJz3TahxIY0G0phL', '$2y$13$hznye7f2AEVAdcpk7XnfRe.4uC2ny0E02IwADm08VujLxHkkseML6', NULL, 'admin1212@admin.com', '', NULL, 0, 2, 11111, 11111),
(44, 'admin@asd.com', 'OL10gcEe5a4Ob_Em42VNIW5bNF9wKd9W', '$2y$13$6.hDDhqulT4py5FAS3bhauRRzE7wWiw16b9vIDlbt6Y.xdLExCxVi', NULL, 'admin@asd.com', '', NULL, 0, 2, 11111, 11111),
(45, 'clinic@gmail.com', '6orYeozJ4yJ5r5uxs4_Sr-s80gauz8Iw', '$2y$13$lbwHwr9Ews0D/EyTkFbSFulQ0LzhfRSHe983GlBCuNFRhAUqpRF5K', NULL, 'clinic@gmail.com', '', NULL, 1, 3, 11111, 11111),
(46, 'as@gmail.com', 'FKlxQdmlIa06k_Ow_l9Dq-6sb4bL2kdV', '$2y$13$NKmJJyWUXvPXK9Ng87Awl.uojcYUXK3Ug6KuiSaAHGADyePWqTwbe', NULL, 'as@gmail.com', '', NULL, 1, 2, 11111, 11111),
(47, 'test123@gmail.com', '6sy1m10XnEdpsZpAvOQLcb3VnAu1OCT3', '$2y$13$ZwSpVKjdnyVrqmVRzMzaEei6skBSSdycIid/3uTg/HTbUeYTu6gz2', NULL, 'test123@gmail.com', '', NULL, 0, 2, 11111, 11111),
(48, 'asdasd@asdmn', 'qk7jtIIe-bC9V7sqG5X5I8s3fA6xOLA8', '$2y$13$/2PH/XRUUxWngIXvgqgSze8AFvVLHxZSmyzxQTr0lFV7pPh00/dgi', NULL, 'asdasd@asdmn', '', NULL, 0, 4, 11111, 11111),
(49, 'sakdhsad@asdkhkashd', 'BW-kcRPEh_Tp1JA6q4fsmaxEvbo0OfEz', '$2y$13$ZlAkwVq1Y1SJ.YkqmvH3/uowHucXvd11GefZCpTYflSqdm5mNW0Ka', NULL, 'sakdhsad@asdkhkashd', '', NULL, 0, 4, 11111, 11111),
(50, 'elhambudi@gmail.com', '6n6Wa0ussmZ75hLK-hWdltVrpa3hG4QZ', '$2y$13$XCD7VNCAx8DBN7I5jmpGNu8Fe3ZxNvRksyzNXJCh4KUPLf2I5ZMCa', NULL, 'elhambudi@gmail.com', '', NULL, 0, 2, 11111, 11111),
(51, 'admin@asdasd.com', '7Bs2SyEYLeNzrGYzzoBZbeLtAT6VDbdH', '$2y$13$Awnq510TfUYoqpdhryOYsOWIkbFV0Vq5f2A7c0UoddzkmlAL0w50.', NULL, 'admin@asdasd.com', '', NULL, 0, 2, 11111, 11111),
(52, 'admin1@asdasd.com', 'fwLVbK5US3Vo3Nok7yGc4mKZshlAqSo8', '$2y$13$S80Y85scbF.ulZ6u8XJq8.NifnigOlJevJ3gtT2Qi3CLjCXN0qpiu', NULL, 'admin1@asdasd.com', '', NULL, 0, 2, 11111, 11111),
(53, 'asdelhambudi@gmail.com', '3dJiREcq2yL0AXQKQk7CGrGhFpJYhpgZ', '$2y$13$JUD5ZI08AFIQL2Czzfy15OPDoRsA9SNpftLHpZCbpaWO9lAJSqO8W', NULL, 'asdelhambudi@gmail.com', '', NULL, 0, 2, 11111, 11111),
(54, 'elham1@gmail.com', '-m5RiW3USdF2Cb6Fye0OLiql1Ao0o7FZ', '$2y$13$klQJ3OaidTIpnL1GubMkXuG03IL45VfNlGl3WFWGapP8zJLF43.4S', NULL, 'elham1@gmail.com', '', NULL, 1, 5, 11111, 11111),
(55, 'user1@gmail.com', '61BB_92dDKo9G84us8pN-yIBsOIX1Pys', '$2y$13$zNodXJGhWCED0JeT8xAprORq3yB6hkTu75Kldub3WdbXXnwt9PmiK', NULL, 'user1@gmail.com', '', NULL, 1, 5, 11111, 11111),
(56, 'budi12@gmail.com', '9MUdvXU1Kr5rXMU_q2AQSsO-SdWPyhyX', '$2y$13$C6GoJqtK6qL.tZ5Vl.ZlPeLk21mXxFOSdAVUJYCjexD/oZmMCnslG', NULL, 'budi12@gmail.com', '', NULL, 1, 5, 11111, 11111),
(57, 'elham1234@gmail.com', 'zM5IfIV3YfmqKUT08PpL9hR--V4bMx4M', '$2y$13$qmlbXsKsbFr6e6rHw..Dk.KvkCGA1FBLrAz71WIY6BscP6rMAAok6', NULL, 'elham1234@gmail.com', '', NULL, 1, 5, 11111, 11111),
(58, 'Test', '', '$2y$13$LB3o.RS48OWWqHkzLvVanOXalFzbyR6GeU4uG4riMCSEUBkcP7rAy', NULL, 'Test', '', NULL, 0, 3, 1500539181, 0),
(59, 'test123456@gmail', '1EKnE7uoObX0t7OZLR5-XdB9hzt-5Zlw', '$2y$13$ssEZ4tZv7zHGd6s8e6rNG.CcGfbccnsdZx/D1vgeeSvQ9WlmAOPJK', NULL, 'test123456@gmail', '', NULL, 0, 2, 11111, 11111),
(60, 'employee@gmail.com', 'coYqffu09eDfsoaAveYZFz1_NjLsjSiH', '$2y$13$RzDH24maWFlBbE6EFkwxS.V.N6EMJb9lA5aODVrFDhveSNcpoCp7u', NULL, 'employee@gmail.com', '', NULL, 1, 4, 11111, 11111),
(61, 'employee31@gmail.com', '4xFIENdiTUh2wWNNiM5KetN8VcWLL02F', '$2y$13$T84AxqM1EWsXDWff1q6/GuWVelrA56K19ZM0nNsuwBbaoldcg5t0i', NULL, 'employee31@gmail.com', '', NULL, 1, 4, 11111, 11111),
(62, 'elham', 'O3MtLk3dld-SlIt09SlfU0q1L-F6UepL', '$2y$13$t/vLBPdQYpeQZVNqFbYa4uoXoJPjKHjzu6woP.idsPniPPz01J0ry', NULL, 'elham12345@gmail.com', '', NULL, 1, 5, 11111, 11111),
(63, 'Elham Budianto', 'Op6Xo31rEUEz_mg5DY9iwk7fs8ZnPV3D', '$2y$13$z9NrLIx/1dPr51gsutwXU.37G2Ya212lmb6IFXrziAX8y0a1whzxe', NULL, 'elham12wer@gmail.com', '', NULL, 1, 5, 11111, 11111),
(64, 'elham123890@gmail.com', '6xjb5Xn2jkCJBlZ6STdY_p4GgAh3uaHW', '$2y$13$dPfR0Y3hiMrMGhpEPZ3pdOFHCTkT5oZ7JityprjCIybOKLn.4bAEq', NULL, 'elham123890@gmail.com', '', NULL, 1, 4, 11111, 11111),
(65, 'clinic10@gmail.com', '', '$2y$13$5Icm/5jHuohW/Yb4EaY25.3.tI9nE1kgZUoWc61miDSeCuFMqkAAS', NULL, 'clinic10@gmail.com', '', NULL, 1, 3, 1501567977, 0),
(66, 'ruben@gmail.com', '', '$2y$13$gBCMdATfUzWibIElU16b8uycxPMHALUtMqGQ3jhqt4mMS0CO90Jba', NULL, 'ruben@gmail.com', '', NULL, 1, 3, 1501663592, 0),
(67, 'clinicmulut@gmail.com', '', '$2y$13$CgVIQp9m.cU22cJlAf5grORMaAs.tmFeA9SO/VVDn1Fs/UKCMCxiK', NULL, 'clinicmulut@gmail.com', '', NULL, 1, 3, 1501671361, 0),
(68, 'klinikoke@gmail.com', '', '$2y$13$rNDwQWd51XzJ6Wr2nrWF.u4ozuNxlsKfyHV8a0MVWEbJLYPob/OGe', NULL, 'klinikoke@gmail.com', '', NULL, 1, 3, 1502112797, 0),
(69, 'cms@gmail.com', 'i96hlM7SgwKB1d94pnJ9OZd1vSWOdcMK', '$2y$13$i5eL2Mj3QF5d6WoxJhA1HuWK6gKqr7aubW6D2712xkV8Em4RbMPkS', NULL, 'cms@gmail.com', '', NULL, 1, 2, 11111, 11111),
(70, 'Anggrian Riska', '5moBvq3iFeHtM4bhcApsp4QpIwpMiVIc', '$2y$13$1YY.MCPg0rDhLVgF5vD05uo89NZUf36p2GdNfVM/b.7kcHSPRp2ae', NULL, 'angghie@gmail.com', '', '59c876104a5421506309648.jpg', 1, 5, 11111, 11111),
(71, 'elhambudi4567@gmail.com', 'YxD5FkQO8431frVHld3n4lHy3ReVUjsP', '$2y$13$vVWU3wuOv8jL6KwIHx2M8OM0Hy29F/R00SeS/u18KThP9yOr0EvCW', NULL, 'elhambudi4567@gmail.com', '', NULL, 0, 3, 1505705866, 1505705866),
(72, 'klinikayahbundasby@gmail.com', '7gFJFnfA3luqUomObfItubdHPSwvLq9G', '$2y$13$XARfuEtfn08YGQKonHLuF.fo9sU3F.Tk/pR9p3ZhZZgxLvAQzg.46', NULL, 'klinikayahbundasby@gmail.com', '', NULL, 0, 3, 1505706771, 1505706771),
(73, 'Ruben Simamora', 'Vtinxich9r4OB6hsJNjawyAZwLn-F5hD', '$2y$13$nckw4xlR87K9GHniMIUZO.Q6NNsmlxXq57WJ45uQ6WX2emC/nN7bO', NULL, 'trisanjayaruben@gmail.com', '', NULL, 1, 5, 11111, 11111),
(74, 'Elham Budi Anto', 'UviPqsLyt9Ifcu76CrIqjXY9bPfeYc94', '$2y$13$tV5u2M6UtB87aaAOsgvgWO.45bVPvlN8gr34ofbnXb2SttXdqvTk.', NULL, 'ahmadsantoso31@yahoo.co.id', '', NULL, 1, 5, 11111, 11111),
(75, 'alfin', 'yQVYqVlTijjJ6OmXLiyZHCBbLAPtUPy6', '$2y$13$Z5CnhxqMhV1p/mkrAUAPo.xwh.jey8ypqsTgLGugI4O6mLdDCuIKa', NULL, 'alfin@gmail.com', '', '59d448acc27e51507084460.jpg', 1, 5, 11111, 11111),
(76, 'Alfin M', 'S6euaRINbFAzI38J6xubP0T213eP54Ps', '$2y$13$.FOn2iLW47Nhl7u0I7uosu/p36huDClObMpMYggcHWlOzwGDAc6NG', NULL, 'alfinm@gmail.com', '', NULL, 1, 5, 11111, 11111),
(77, 'Alfin Mau', 'fddIY3yJaqSF6m2fz2OwYioLm2m0SxS_', '$2y$13$qHfiWZrJ0S/pWdCbuqBlCO97yfQ0yugY9PSTvoAvr3oLIW3qCzuuW', NULL, 'alfinma@gmail.com', '', NULL, 1, 5, 11111, 11111),
(78, 'Alfin Mauu', '3kAYJf0QwlFN_s28veemoHGp00VBl8My', '$2y$13$urvC0lawpCPbo1H4Wb6jpuxRYoU0OwyBpJX4WtIvAC6HhBznKojtW', NULL, 'alfinmau@gmail.com', '', NULL, 1, 5, 11111, 11111),
(79, 'Alfin Maul', 'SVrk0pG7TEHEmovsIRC1GuaKTtxN6Ame', '$2y$13$LADZQ.wkSDq9J/KtdwHgz.H.pqu/AVv9q.v5So8nnMY8k7..zdFLW', NULL, 'alfinmaul@gmail.com', '', NULL, 1, 5, 11111, 11111),
(80, 'Alfin Maula', 'FjD0p6RmDUIz4Jk8vG_apBp-aYbi6U7N', '$2y$13$NE.012GEBDa5gQHzmQROMeEKj20fDmEFF8o57yWhso/vev8g9X3ly', NULL, 'alfinmaula@gmail.com', '', NULL, 1, 5, 11111, 11111),
(81, 'Alfin Maulana', 'QIWaoVR9WVKqWiOtp99AOIYaAw7aBRtc', '$2y$13$jo1Glj9HjkFkXrVTnu9HxufR0L7faqs6UHZFh6ap1fjEPaUX9p1Wq', NULL, 'alfinmaulan@gmail.com', '', NULL, 1, 5, 11111, 11111),
(82, 'Alfin Maulana F', 'P4gW7UfncktZsJOLspcFPoAfi_jQCf3a', '$2y$13$70c.sHX47nZRrW8YzkRLLuPnK.b6FiCm2anJa4x/.nNksPo8jNNRa', NULL, 'alfinmaulana@gmail.com', '', NULL, 1, 5, 11111, 11111),
(83, 'asdasd', '8RGvQ4cRRbueVavBOb-G_Mfq_7GIHOwy', '$2y$13$g5BbBehrI1viC6sOBBeHTu.ZxYxiWQaBEmCNlyXxMA.4jxLdP9KQC', NULL, 'asdasd', '', NULL, 1, 5, 11111, 11111),
(84, 'adasdasdasd', '5rrrXdcBm4vDELZLsuqgt2gDiJNpVudO', '$2y$13$.MvmYcb24EFXAKJsrl5uLuvDF4F4MOPZtyRlZ5TAaWEXmmkSdPYay', NULL, 'adasdasd', '', NULL, 1, 5, 11111, 11111),
(85, 'adasdasd', 'cPILnfJzgr66hZ92bIQV4KASMjKHDLiA', '$2y$13$JRvpJX4zgF5KxJ.amk9co.l.3Eqp.4yDw286oXG8WPcw821HiS6zW', NULL, 'asdasdasd', '', NULL, 1, 5, 11111, 11111),
(86, 'asdasdasd', 'Coi8C3sQB7TY2CWSwu63pLEilRivkR6A', '$2y$13$nwyiHoYsx2cvU2M1G5fEMukzm0KogugorhinR.gTR72lkg1dqL/ku', NULL, 'da12312ex1x2e', '', NULL, 1, 5, 11111, 11111),
(87, 'Ruben Tri Simamora', 'T0Gpjg6gwgS9ZXrx1x0KkPW6toF0QzDB', '$2y$13$8/81ZTaueQnRTiNEq42hnOevRm8iZP5Gjqu.s.qcCnBhlIxkoADPu', NULL, 'trisanjaya_ruben@yahoo.com', '', NULL, 1, 5, 11111, 11111),
(88, 'Alfinm', 'RO7--q3pBRb0jk3qONLe64SsXg9jtjyg', '$2y$13$dbbk1nn8q7vpx9Hc1m4PDuLFV0ilb5sIakY9GImYKcONneJOXfTC2', NULL, 'alf@gmail.com', '', NULL, 1, 5, 11111, 11111),
(89, 'Crash Magrish', 'SbsEsLitO4vauzIw_z718pTvA4rPMjkJ', '$2y$13$plkEOV72EuXDCNOGXpS03.nz5g8BwGHmvkYxnxCiwbr9/ei2wCpkO', NULL, 'crashmagrish@gmail.com', '', NULL, 1, 5, 11111, 11111),
(90, 'Alfin Fikri', 'gT38v6_-St6cTJ1fCGWys71EtmYotrSw', '$2y$13$5DL.MMgc9OshtswAlpBKcug71FYRGFhIVmxa1DafYz73vE6c0/rgi', NULL, 'alfa@gmail.com', '087758881111', '59db06d4bc8bb1507526356.jpg', 1, 5, 11111, 11111),
(91, 'Alfin Maulana Fikri', 'JyX_hDYR4W8HIJ4Awovvq8Nm4630fAht', '$2y$13$ZHONbC7AwxPCdHKFI9/fheu5g3S2KclJGbvbup3mrTML3qb1u3mn2', NULL, 'alfinmaulanafikri@gmail.com', '087758881111', '59db2a68c86441507535464.jpg', 1, 5, 11111, 11111);

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `min_can_be_purchased` int(11) NOT NULL,
  `real_price` varchar(11) NOT NULL,
  `voucher_price` varchar(11) NOT NULL,
  `discount` varchar(45) NOT NULL,
  `expire_date` date NOT NULL,
  `image` text,
  `stock` int(11) NOT NULL DEFAULT '0',
  `status_all_office` tinyint(1) DEFAULT NULL,
  `created_date` date NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `category_id` int(11) NOT NULL,
  `clinic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `name`, `description`, `min_can_be_purchased`, `real_price`, `voucher_price`, `discount`, `expire_date`, `image`, `stock`, `status_all_office`, `created_date`, `status`, `category_id`, `clinic_id`) VALUES
(1, 'Voucher 1', 'Deskripsi voucher 1', 5, '500000', '300000', '40', '2017-06-23', 'pen-bg-2.jpg', 0, NULL, '2017-07-27', 1, 2, 2),
(2, 'Voucher 2', 'Deskripsi voucher 2', 5, '500000', '300000', '40', '2017-06-23', 'item2.jpg', 11, NULL, '2017-06-08', 1, 2, 0),
(3, 'Voucher 3', 'Deskripsi voucher 3', 5, '500000', '300000', '40', '2017-06-23', 'item3.jpg', 10, NULL, '2017-06-08', 1, 3, 2),
(4, 'Voucher 4', 'Deskripsi voucher 4', 5, '500000', '300000', '40', '2017-06-23', 'item4.jpg', 8, NULL, '2017-06-08', 1, 4, 2),
(5, 'Voucher 5', 'Deskripsi voucher 5', 5, '500000', '300000', '40', '2017-06-23', 'item5.jpg', 11, NULL, '2017-06-08', 1, 5, 0),
(12, 'asdasd', NULL, 123213, '500000', '150000', '81.233311420061', '2017-06-29', 'photo-big.jpg', 119, NULL, '2017-07-27', 1, 4, 2),
(13, 'Vooucher 34', NULL, 12, '12000', '1200', '90', '2017-08-01', 'photo-big.jpg', 12, NULL, '2017-08-01', 0, 2, 19),
(14, 'Mamo 1', NULL, 3, '130000', '100000', '30', '2017-08-30', '18738367_1263834183730499_8781745869970987870_o.jpg', 1, NULL, '2017-08-02', 1, 2, 20),
(15, 'Voucher clinic mulut', NULL, 2, '150000', '120000', '25', '2017-08-21', '18738367_1263834183730499_8781745869970987870_o.jpg', 0, NULL, '2017-08-02', 1, 6, 21),
(17, 'EZ Accu Shot', NULL, 1, '300000', '100000', '66.666666666667', '2017-12-31', 'EZ ACCU SHOT SELECT 672x320.jpg', 99, NULL, '2017-09-18', 1, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `voucher_code`
--

CREATE TABLE `voucher_code` (
  `id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=not active\n1=active\n2=used',
  `used_date` datetime DEFAULT NULL,
  `voucher_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_voucher_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `voucher_has_office`
--

CREATE TABLE `voucher_has_office` (
  `voucher_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher_has_office`
--

INSERT INTO `voucher_has_office` (`voucher_id`, `office_id`) VALUES
(1, 2),
(1, 4),
(2, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aturan_penggunaan`
--
ALTER TABLE `aturan_penggunaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_admin`
--
ALTER TABLE `bank_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_clinic`
--
ALTER TABLE `bank_clinic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bank_id` (`bank_id`),
  ADD KEY `clinic_id` (`clinic_id`);

--
-- Indexes for table `benefit`
--
ALTER TABLE `benefit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_has_clinic`
--
ALTER TABLE `category_has_clinic`
  ADD PRIMARY KEY (`category_id`,`clinic_id`),
  ADD KEY `fk_category_has_clinic_clinic1_idx` (`clinic_id`),
  ADD KEY `fk_category_has_clinic_category1_idx` (`category_id`);

--
-- Indexes for table `category_has_doctor`
--
ALTER TABLE `category_has_doctor`
  ADD PRIMARY KEY (`doctor_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_city_province1_idx` (`province_id`);

--
-- Indexes for table `clinic`
--
ALTER TABLE `clinic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_clinic_user1_idx` (`user_id`);

--
-- Indexes for table `clinic_photo`
--
ALTER TABLE `clinic_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clinic_id` (`clinic_id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_config_config_category1_idx` (`config_category_id`);

--
-- Indexes for table `config_category`
--
ALTER TABLE `config_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clinic_id` (`clinic_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_employee_user1_idx` (`user_id`),
  ADD KEY `fk_employee_office1_idx` (`office_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kebijakan_privasi`
--
ALTER TABLE `kebijakan_privasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_transaction`
--
ALTER TABLE `list_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_voucher` (`id_voucher`);

--
-- Indexes for table `logo_header`
--
ALTER TABLE `logo_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_member_city1_idx` (`city_id`),
  ADD KEY `fk_member_user1_idx` (`user_id`),
  ADD KEY `fk_member_gender1_idx` (`gender_id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `office`
--
ALTER TABLE `office`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_office_clinic1_idx` (`clinic_id`),
  ADD KEY `fk_office_city1_idx` (`city_id`);

--
-- Indexes for table `order_voucher`
--
ALTER TABLE `order_voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `voucher_id` (`voucher_id`);

--
-- Indexes for table `order_voucher_has_voucher`
--
ALTER TABLE `order_voucher_has_voucher`
  ADD PRIMARY KEY (`order_voucher_id`,`voucher_id`),
  ADD KEY `voucher_id` (`voucher_id`);

--
-- Indexes for table `pcounter_save`
--
ALTER TABLE `pcounter_save`
  ADD PRIMARY KEY (`save_name`);

--
-- Indexes for table `pcounter_users`
--
ALTER TABLE `pcounter_users`
  ADD PRIMARY KEY (`user_ip`);

--
-- Indexes for table `program_clinic`
--
ALTER TABLE `program_clinic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_province_country1_idx` (`country_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transaction_user1_idx` (`user_id`);

--
-- Indexes for table `transaction_has_voucher_code`
--
ALTER TABLE `transaction_has_voucher_code`
  ADD PRIMARY KEY (`transaction_id`,`voucher_code_id`),
  ADD KEY `fk_transaction_has_voucher_code_voucher_code1_idx` (`voucher_code_id`),
  ADD KEY `fk_transaction_has_voucher_code_transaction1_idx` (`transaction_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `clinic_id` (`clinic_id`);

--
-- Indexes for table `voucher_code`
--
ALTER TABLE `voucher_code`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_voucher_code_voucher1_idx` (`voucher_id`),
  ADD KEY `fk_voucher_code_user1_idx` (`user_id`),
  ADD KEY `order_voucher_id` (`order_voucher_id`);

--
-- Indexes for table `voucher_has_office`
--
ALTER TABLE `voucher_has_office`
  ADD PRIMARY KEY (`voucher_id`,`office_id`),
  ADD KEY `fk_voucher_has_office_office1_idx` (`office_id`),
  ADD KEY `fk_voucher_has_office_voucher1_idx` (`voucher_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `aturan_penggunaan`
--
ALTER TABLE `aturan_penggunaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bank_admin`
--
ALTER TABLE `bank_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `bank_clinic`
--
ALTER TABLE `bank_clinic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `benefit`
--
ALTER TABLE `benefit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `clinic`
--
ALTER TABLE `clinic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `clinic_photo`
--
ALTER TABLE `clinic_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config_category`
--
ALTER TABLE `config_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kebijakan_privasi`
--
ALTER TABLE `kebijakan_privasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `list_transaction`
--
ALTER TABLE `list_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `logo_header`
--
ALTER TABLE `logo_header`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `office`
--
ALTER TABLE `office`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `order_voucher`
--
ALTER TABLE `order_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `program_clinic`
--
ALTER TABLE `program_clinic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `voucher_code`
--
ALTER TABLE `voucher_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bank_clinic`
--
ALTER TABLE `bank_clinic`
  ADD CONSTRAINT `bank_clinic_ibfk_1` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
  ADD CONSTRAINT `bank_clinic_ibfk_2` FOREIGN KEY (`clinic_id`) REFERENCES `clinic` (`id`);

--
-- Constraints for table `category_has_clinic`
--
ALTER TABLE `category_has_clinic`
  ADD CONSTRAINT `fk_category_has_clinic_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_category_has_clinic_clinic1` FOREIGN KEY (`clinic_id`) REFERENCES `clinic` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `category_has_doctor`
--
ALTER TABLE `category_has_doctor`
  ADD CONSTRAINT `category_has_doctor_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`),
  ADD CONSTRAINT `category_has_doctor_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `fk_city_province1` FOREIGN KEY (`province_id`) REFERENCES `province` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `clinic`
--
ALTER TABLE `clinic`
  ADD CONSTRAINT `fk_clinic_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `clinic_photo`
--
ALTER TABLE `clinic_photo`
  ADD CONSTRAINT `clinic_photo_ibfk_1` FOREIGN KEY (`clinic_id`) REFERENCES `clinic` (`id`);

--
-- Constraints for table `config`
--
ALTER TABLE `config`
  ADD CONSTRAINT `fk_config_config_category1` FOREIGN KEY (`config_category_id`) REFERENCES `config_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctor`
--
ALTER TABLE `doctor`
  ADD CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`clinic_id`) REFERENCES `clinic` (`id`);

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `fk_employee_office1` FOREIGN KEY (`office_id`) REFERENCES `office` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employee_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `list_transaction`
--
ALTER TABLE `list_transaction`
  ADD CONSTRAINT `list_transaction_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `list_transaction_ibfk_2` FOREIGN KEY (`id_voucher`) REFERENCES `order_voucher` (`id`);

--
-- Constraints for table `member`
--
ALTER TABLE `member`
  ADD CONSTRAINT `fk_member_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_member_gender1` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_member_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `office`
--
ALTER TABLE `office`
  ADD CONSTRAINT `fk_office_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_office_clinic1` FOREIGN KEY (`clinic_id`) REFERENCES `clinic` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_voucher`
--
ALTER TABLE `order_voucher`
  ADD CONSTRAINT `order_voucher_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `order_voucher_ibfk_2` FOREIGN KEY (`voucher_id`) REFERENCES `voucher` (`id`);

--
-- Constraints for table `order_voucher_has_voucher`
--
ALTER TABLE `order_voucher_has_voucher`
  ADD CONSTRAINT `order_voucher_has_voucher_ibfk_1` FOREIGN KEY (`order_voucher_id`) REFERENCES `order_voucher` (`id`),
  ADD CONSTRAINT `order_voucher_has_voucher_ibfk_2` FOREIGN KEY (`voucher_id`) REFERENCES `voucher` (`id`);

--
-- Constraints for table `province`
--
ALTER TABLE `province`
  ADD CONSTRAINT `fk_province_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `fk_transaction_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction_has_voucher_code`
--
ALTER TABLE `transaction_has_voucher_code`
  ADD CONSTRAINT `fk_transaction_has_voucher_code_transaction1` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transaction_has_voucher_code_voucher_code1` FOREIGN KEY (`voucher_code_id`) REFERENCES `voucher_code` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Constraints for table `voucher`
--
ALTER TABLE `voucher`
  ADD CONSTRAINT `voucher_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `voucher_code`
--
ALTER TABLE `voucher_code`
  ADD CONSTRAINT `fk_voucher_code_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_voucher_code_voucher1` FOREIGN KEY (`voucher_id`) REFERENCES `voucher` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `voucher_code_ibfk_1` FOREIGN KEY (`order_voucher_id`) REFERENCES `order_voucher` (`id`);

--
-- Constraints for table `voucher_has_office`
--
ALTER TABLE `voucher_has_office`
  ADD CONSTRAINT `fk_voucher_has_office_office1` FOREIGN KEY (`office_id`) REFERENCES `office` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_voucher_has_office_voucher1` FOREIGN KEY (`voucher_id`) REFERENCES `voucher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
