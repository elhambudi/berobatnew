<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-clinic',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'employee\controllers',
    'bootstrap' => ['log'],
	'defaultRoute' => 'site/login',
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-employee',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-employee', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the employee
            'name' => 'advanced-employee',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];
