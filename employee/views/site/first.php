<?php

use common\models\Constant;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use yii\bootstrap\Modal;
?>
<div class="row">
	<div class="col-md-7">
		<div class="box-box">
			<div class="desc">
				<div class="the-title">Selamat datang di Dashboard Employee berobat.id!</div>
				<div class="the-desc">Kami senang Anda telah bergabung bersama kami. Untuk mengakses semua fitur di klinik dashboard, Anda diharuskan untuk melengkapi info berikut ini terlebih dahulu.</div>
			</div>
		</div>
	</div>

	<div class="col-md-5">
		<div class="img-welcome vcenter">
			<img src="<?= Yii::$app->request->baseUrl ?>/images/nurse.png">
		</div>
	</div>	
</div>