<div class="topbar" ng-controller="EmployeesController">
	<div class="row narrow">
		<div class="col-sm-2">
			<div class="form-group">
				<select class="form-control">
                    <option>Semua Kategori</option>
                    <option>Dentist</option>
                    <option>Orthopedist</option>
                    <option>Pediatrician</option>
                </select>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<select class="form-control">
                    <option>Semua Status</option>
                    <option>Expired</option>
                    <option>Stok Habis</option>
                    <option>Stok Hampir Habis</option>
                </select>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group">
				<input type="text" name="" placeholder="Pencarian" class="form-control">
			</div>
		</div>
		<div class="col-sm-5">
			<div class="pull-right">
				<div class="btn btn-default">Total : 14</div>&nbsp;&nbsp;
				<button type="button" class="btn btn-primary" onclick="openRight()">Buat Voucher</button>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="row operation">
    <div class="col-sm-12 left" ng-controller="ListVoucherController">
        <table class="table table-responsive header-table tb-list-voucher">
          	<thead>
	            <tr>
	              	<th>Nama</th>
	              	<th>Kategori</th>
	              	<th class="text-center">Harga Voucher</th>
	              	<th class="text-center">In Stok</th>
	              	<th class="text-center">Expired Date</th>
	              	<th class="text-center">Status</th>
	            </tr>
          	</thead>
        </table>
        <div class="box-box v3 vtable">
            <table class="table table-responsive table-clickable tb-list-voucher">
	          	<tbody>
		            <tr onclick="openRight()">
		              	<td>Nama Nama Nama Nama NamaNamaNamaNama Nama Nama Nama</td>
		              	<td>Kategori</td>
		              	<td class="text-right">Rp. 200.000</td>
		              	<td class="text-center">10</td>
		              	<td class="text-center">20/10/2016</td>
		              	<td>Expired</td>
		            </tr>
	          	</tbody>
	        </table>
        </div>
    </div>
</div>