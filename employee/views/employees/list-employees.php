<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

<div class="topbar" ng-controller="VoucherController">
	<div class="row narrow">
		<div class="col-sm-2">
			<div class="form-group">
				<select class="form-control">
                    <option>Semua Kategori</option>
                </select>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<select class="form-control">
                    <option>Semua Status</option>
                </select>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group">
				<input type="text" name="" placeholder="Pencarian" class="form-control">
			</div>
		</div>
		<div class="col-sm-5">
			<div class="pull-right">
				<div class="btn btn-default">Total : <?=$count_data?></div>&nbsp;&nbsp;
				<button type="button" class="btn btn-primary" onclick="openRight()">Tambah Employee</button>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="row operation">
    <div class="col-sm-12 left" ng-controller="ListVoucherController">
        <table class="table table-responsive header-table tb-list-voucher">
          	<thead>
	            <tr>
	              	<th>Nama</th>
	              	<th>Telephone</th>
	              	<th>Email</th>
	              	<th>Kantor</th>
	            </tr>
          	</thead>
        </table>
        <div class="box-box v3 vtable">
            <table class="table table-responsive table-clickable tb-list-voucher">
	          	<tbody>
					<?php foreach($data as $d){?>
		            <tr onclick="openRight()">
		              	<td><?php echo $d['name'];?></td>
		              	<td><?php echo $d['phone'];?></td>
		              	<td class="text-right"><?php
							$user = \common\models\User :: find()->where(['id'=>$d['user_id']])->all();
							foreach($user as $u){
								echo $u['email'];
							}
						?></td>
		              	<td class="text-center"><?php
							$user = \common\models\Office :: find()->where(['id'=>$d['office_id']])->all();
							foreach($user as $u){
								echo $u['address'];
							}
						?></td>
		            </tr>
					<?php
						}
					?>
	          	</tbody>
	        </table>
        </div>
    </div>
	<?php $form = ActiveForm::begin([
				'action' => Url::to(['employees/daftar']),
                'method' => 'POST'
				]); ?>
    <div class="col-sm-6 right">
    	<div class="head-vform">
    		<button type="button" onclick="closeRight()" class="btn pull-right btn-default btn-sm">
    			Tutup <i class="fa fa-angle-right"></i>
    		</button>
    	</div>
        <div class="box-box v4 vform">
            <section class="sub-form">
                <div class="row narrow">
                  	<div class="col-sm-12">
	                    <div class="form-group">
	                        <label class="control-label">Nama</label>
	                        <input type="text" name="Employee[name]" class="form-control" ng-model="form.real_price" placeholder="Nama Lengkap">
	                    </div>
                  	</div>
					<div class="col-sm-12">
	                    <div class="form-group">
	                        <label class="control-label">Telephone</label>
	                        <input type="text" name="Employee[phone]" class="form-control" ng-model="form.real_price" placeholder="Nomor Telephone">
	                    </div>
                  	</div>
					<div class="col-sm-12">
	                    <div class="form-group">
	                        <label class="control-label">Email</label>
	                        <input type="text" name="User[email]" class="form-control" ng-model="form.real_price" placeholder="Email">
	                    </div>
                  	</div>
					<div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Kantor</label>
                            <select name="Employee[office_id]" class="form-control" ng-model="form.category_id">
                                <?php foreach($office as $o){?>
									<option value="<?=$o['id']?>"><?= $o['name']?> - <?= $o['address']?></option>
								<?php 
									}
								?>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="fixed-form-footer">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-submit">Simpan</button>
            </div>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
</div>