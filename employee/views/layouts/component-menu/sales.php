<?php 

use yii\helpers\Html;
use yii\widgets\Menu;
?>

<nav class="sub-navbar">
	<div class="container">
		<?php 
        echo Menu::widget([
            'items' => [
                ['label' => 'List Penjualan', 'url' => ['sales/list-sales']],
                ['label' => 'Laporan', 'url' => ['sales/report']],
            ],
            'options' => [
                'class' => 'nav navbar-nav',
            ],
        ]);
        ?>
	</div>
</nav>