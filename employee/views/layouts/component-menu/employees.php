<?php 

use yii\helpers\Html;
use yii\widgets\Menu;
?>

<nav class="sub-navbar">
	<div class="container">
		<?php 
        echo Menu::widget([
            'items' => [
                ['label' => 'List Pegawai', 'url' => ['employees/list-employees']],
            ],
            'options' => [
                'class' => 'nav navbar-nav',
            ],
        ]);
        ?>
	</div>
</nav>