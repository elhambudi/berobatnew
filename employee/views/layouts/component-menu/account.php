<?php 

use yii\helpers\Html;
use yii\widgets\Menu;
?>

<nav class="sub-navbar">
	<div class="container">
		<?php 
        echo Menu::widget([
            'items' => [
                ['label' => 'Pemilik Akun', 'url' => ['account/owner']],
                // ['label' => 'Profil Publik', 'url' => ['account/public']],
                // ['label' => 'Dokter', 'url' => ['account/doctors']],
                // ['label' => 'Lokasi Klinik', 'url' => ['account/clinic-location']],
                // ['label' => 'Galeri', 'url' => ['account/galleries']],
            ],
            'options' => [
                'class' => 'nav navbar-nav',
            ],
        ]);
        ?>
	</div>
</nav>