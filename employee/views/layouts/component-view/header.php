<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;
use common\models\Clinic;

$clinicDetail = Clinic::findOne(['user_id' => Yii::$app->user->identity->id]);
?>

<nav class="navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand">
                <?= 
                    Html::a( "<img src='". Yii::$app->request->baseUrl ."/images/logo.png'>", 
                        ['/']) ;
                ?>
            </span>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php 
            echo Menu::widget([
                'items' => [
                    ['label' => 'DASHBOARD', 'url' => ['site/index']],
                    ['label' => 'LIST VOUCHER', 'url' => ['vouchers/index']],
                ],
                'options' => [
                    'class' => 'nav navbar-nav',
                ],
            ]);
            ?>
            <ul class="nav navbar-nav navbar-right">
				<?php
					 if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role_id == 4) {
				?>
				<li class="dropdown user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span><i class="fa fa-user"></i></span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="logo">
                                        <img src="<?= Url::to('@web/img/person.png');?>" class="img-circle" alt="User Image">
                                    </div>
                                </div>
                                <div class="col-xs-8">
                                    <div class="name"><?= !is_null($clinicDetail) ? $clinicDetail->name_clinic : "" ?></div>
                                    <div class="email"><?= Yii::$app->user->identity->email ?></div>
                                </div>
                            </div>
                        </li>
                        <li class="user-body">
                            <ul>
                                <li>
                                    <?= Html::a('<i class="fa fa-sign-out"></i> Logout', ['/site/logout'], [
                                        'data' => [
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
				<?php } else { ?>
					<li id="iframe-login" class="iframe-login"><a href="<?=Url::to(['site/login']);?>"><i class="fa fa-sign-in"></i><span> Login</span></a></li>
				<?php }?>
                
            </ul>
        </div><!--/.nav-collapse -->
    </div>        
</nav>