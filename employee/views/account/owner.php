<?php

use yii\widgets\ActiveForm;

$isHasFlashError = Yii::$app->getSession()->hasFlash('error');
if( $isHasFlashError ){
    $data = Yii::$app->getSession()->getFlash('error');
}
?>
<div class="row">
    <div class="col-md-6">
        <div class="box-box">
            <section class="sub-form">
                <div class="title for-main">
                    <span class="the-title">Pemilik Akun</span>
                </div>
                <?php $form = ActiveForm::begin(['id' => 'form1']);?>
                    <?php if( $isHasFlashError ): ?>
                    <div class="alert alert-<?= $data['error'] ? 'danger' : 'success' ?>">
                        <?= $data['msg'] ?>
                    </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <?= $form->field($clinicModel,'name_contact')->textInput([
                            'class' => 'form-control',
                            'placeholder' => 'Nama pemilik klinik'
                        ])->label('Nama') ?>
                    </div>

                    <div class="form-group pull-right">
                        <button id="account-btn" type="submit" class="btn btn-primary btn-submit">
                            Simpan
                        </button>
                    </div>       
                    <div class="clear" />

                    <?php ActiveForm::end(); ?>
                        <hr class="devider"></hr>                        
                    <?php $form = ActiveForm::begin(['id' => 'form1']);?>

                    <div class="form-group">
                        <label>Ganti Password</label>
                        <?= $form->field($changePasswordModel,'oldpass')->passwordInput([
                            'class' => 'form-control',
                            'placeholder' => 'Password lama'
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($changePasswordModel,'newpass')->passwordInput([
                                    'class' => 'form-control',
                                    'placeholder' => 'Password lama'
                                ]) ?>
                            </div>
                            <div class="col-md-6">						
                                <?= $form->field($changePasswordModel,'repeatnewpass')->passwordInput([
                                    'class' => 'form-control',
                                    'placeholder' => 'Password lama'
                                ]) ?>
                            </div>
                        </div>
                    </div>				
                    
                    <hr class="devider"></hr>

                    <div class="form-group pull-right">
                        <button id="account-btn" type="submit" class="btn btn-primary btn-submit">
                            Simpan
                        </button>
                    </div>
                <?php ActiveForm::end();?>
            </section>
        </div>
    </div>
</div>