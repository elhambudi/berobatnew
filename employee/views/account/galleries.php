<div class="row operation">
    <div class="col-sm-12 left">
        <div class="fixed-title">
            <span class="the-title">Galeri</span>
        </div>
        <div class="box-box v2">
            <section class="sub-form">
                <div class="list-galeri">
                    <div class="box image" onclick="openRight()">
                        <div class="the-image"><img src="<?= Yii::$app->request->baseUrl ?>/web/images/nurse.png"></div>
                    </div>
                    <div class="box add" onclick="openRight()">
                        <div class="icon"><i class="fa fa-image"></i></div>
                        <div class="explainer">TAMBAH <i class="fa fa-plus-square"></i></div>
                    </div>
                </div>
                <br>
            </section>
        </div>
    </div>
    <div class="col-sm-6 right">
        <div class="fixed-title">
            <span class="the-title">Tambah Galeri</span>
        </div>
        <div class="box-box v3 vform">
            <section class="sub-form">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                        <div class="image-preview-input">
                          <!-- <img src="<?= Yii::$app->request->baseUrl ?>/web/images/logo1.jpg"> -->
                          <i class="fa fa-image"></i>
                        </div>
                        <label class="btn btn-warning btn-file btn-block">
                          <span class="title"><i class="fa fa-upload"></i> Upload Gambar</span> <input type="file" hidden>
                      </label>
                      <span class="help-block" ng-if=""></span>   
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Caption</label>
                            <textarea rows="4" type="text" class="form-control" ng-model="" placeholder="Mengenai gambar galeri ini"></textarea>
                            <span class="help-block" ng-if=""></span>   
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </section>
        </div>
        <div class="fixed-form-footer">
            <div class="pull-left">
                <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
            </div>
            <div class="pull-right">
                <button onclick="closeRight()" class="btn btn-default">Kembali</button>
                <button class="btn btn-primary btn-submit">Simpan</button>
            </div>
        </div>
    </div>
</div>