<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Situs Beli Voucher Treatment Kesehatan Online Mudah Dan Terpecaya";
/* @var $this yii\web\View */
?>
<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span></span></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
          <div class="box box-info">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Name</th>
				  <th>Code</th>
                  <th>Status Bayar</th>
                  <th>Status Validate</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
					<?php 
						$i=1;
						foreach($voucher as $v){
					?>
					<tr>
						<th><?=$i++?></th>
						<th><?php
								$voucher = \common\models\Voucher::findOne($v['voucher_id']);
								echo $voucher->name;
						?></th>
						<th><?=$v['code']?></th>
						<th><?php
							if($v['status']==0){
								echo '<span class="label label-danger">Not Paid</span>';
							}else{
								echo '<span class="label label-success">Paid</span>';
							}?></th>
						<th><?php
							if($v['status_validate']==0){
								echo '<span class="label label-danger">Not Validate</span>';
							}else{
								echo '<span class="label label-success">Validated</span>';
							}?></th>
						<th>
							<?php
								if($v['status_validate']==0){
							?>
						   <a href="<?=Url::to(['vouchers/validate','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-check"></span> Validate</a>
						   <?php
								}elseif($v['status_validate']==1){
						   ?>
						   <!--<a href="<?=Url::to(['vouchers/unvalidate','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-danger"><span class="fa fa-check"></span> Unvalidate</a>-->
						   <a type="button" class="btn active btn-success"><span class="fa fa-check"></span> Validated</a>
						   <?php
							}
						   ?>
						</th>
					</tr>
					<?php
						}
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>
</div>