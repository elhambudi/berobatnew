<?php

namespace clinic\controllers;

class CustomersController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->redirect(['list-customers']);
    }

    public function actionListCustomers()
    {
        return $this->render('list-customers');
    }
}
