<?php

namespace clinic\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\Employee;
use common\models\User;

class EmployeesController extends \yii\web\Controller
{
    public function actionIndex()
    {
		$data = Employee::find()->all();
        return $this->redirect(['list-employees']);
    }

    public function actionListEmployees()
    {
		$data = Employee::find()->all();
		$count_data = Employee::find()->count();
		$office = \common\models\Office::find()->all();
        return $this->render('list-employees',[
			'data' => $data ,
			'count_data' => $count_data ,
			'office' => $office
		]);
    }
	public function actionDaftar(){
		$model = new User();
		$username = Yii::$app->request->post('User');
        $model->username = $username['email'];
        $model->email = $username['email'];
        $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash('initial');
		//var_dump($model->password_hash);die();
        $model->generateAuthKey();
		$model->role_id = 2;
		$model->status=0;
		$model->created_at=11111;
		$model->updated_at=11111;
		$model->save();
		if($model->save()){
		   $modelmember = new Employee();
		   $member = Yii::$app->request->post('Employee');
		   //var_dump($member);die();
		   $modelmember->name = $member['name'];
		   $modelmember->phone = $member['phone'];
		   $modelmember->office_id = $member['office_id'];
		   $modelmember->user_id = $model->getPrimaryKey();
		   $modelmember->save();
		}else{
			var_dump( $model->errors);
		   die();
		}
		return $this->redirect(['list-employees']);
	}
}
