<?php

namespace clinic\controllers;
use Yii;
use common\models\User;
use common\models\Clinic;

class AccountController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->redirect(['owner']);
    }

    public function actionOwner()
    {
        $userModel = User::findOne( Yii::$app->user->identity->id );        
        $clinicModel = Clinic::findOne(['user_id' => $userModel->id]);
        $changePassModel = new \common\models\ChangePassword();

        if( Yii::$app->request->post() ){
            $errors = [
                'error' => false,
                'msg' => "Data berhasil diubah"
            ];
            $postData = Yii::$app->request->post();
                       

            if( isset($postData['Clinic']) ){
                $clinicModel->load($postData);
                $clinicModel->save();
                Yii::$app->getSession()->setFlash('error',$errors);                
            }else{
                $changePassModel->load($postData);
                if( $changePassModel->validate() ){
                    $userModel->password_hash = Yii::$app->security->generatePasswordHash($changePassModel->newpass);
                    $userModel->save();                    
                    Yii::$app->getSession()->setFlash('error',$errors);
                }
            }

            if( Yii::$app->getSession()->hasFlash('error') ){
                return $this->redirect(['owner']);
            }
        }

        return $this->render('owner',[
            'model' => $userModel,
            'clinicModel' => $clinicModel,
            'changePasswordModel' => $changePassModel
        ]);
    }

    public function actionPublic()
    {
        return $this->render('public');
    }

    public function actionDoctors()
    {
        return $this->render('doctors');
    }

    public function actionClinicLocation()
    {
        return $this->render('clinic-location');
    }

	public function actionGalleries()
    {
        return $this->render('galleries');
    }    
}
