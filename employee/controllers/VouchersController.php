<?php

namespace employee\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\Voucher;
use common\models\VoucherHasOffice;
use common\models\Office;
use common\models\Clinic;
use common\models\Category;
use common\models\CategoryHasClinic;
use common\models\OrderVoucher;
use common\models\Employee;

class VouchersController extends \yii\web\Controller
{
    public function actionIndex()
    {
		$employee = Employee :: find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
		foreach($employee as $e){
			$id_office = Office::find()->where(['id'=>$e['office_id']])->all();
			foreach($id_office as $o){
				$id_clinic = $o['clinic_id'];
			}
		}
		//var_dump($id_clinic);die();
		$listvoucher = Voucher :: find()->where(['clinic_id'=>$id_clinic])->all();
		foreach($listvoucher as $list){
			$id_voucher[] = $list['id'];
		}
		$voucher = OrderVoucher :: find()->where(['in','voucher_id' ,$id_voucher])->andWhere(['status'=>1])->all();
		//$office = Office :: find()->where()->all();
		//var_dump($voucher);die();
        return $this->render('index',[
			'voucher'=>$voucher,
		]);
    }
	
	public function actionValidate($id_voucher){
		$model = OrderVoucher :: findOne($id_voucher);
		$model->status_validate =1;
		$model->validate_date =date("Y-m-d");
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher has been Validated.');
		return $this->redirect(['vouchers/index']);
	}

	public function actionUnvalidate($id_voucher){
		$model = OrderVoucher :: findOne($id_voucher);
		$model->status_validate =0;
		$model->validate_date =NULL;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher not been Validated.');
		return $this->redirect(['vouchers/index']);
	}

}	
