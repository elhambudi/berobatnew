<?php
namespace employee\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Clinic;
use common\models\Doctor;
use common\models\Office;
use common\models\BankClinic;
use common\models\Constant;
use common\models\Bank;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
				'only' => ['logout','index','first'],
                'rules' => [
                    [
                        'actions' => ['logout','index','first'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		return $this->redirect('first');
//        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout ="main-login";
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(\yii\helpers\Url::to(['site/first']));
        }

        $model = new LoginForm();
		
        if ($model->load(Yii::$app->request->post()) && $model->login() && Yii::$app->user->identity->role_id == 4) {
			//var_dump('test');die();
            return $this->redirect(\yii\helpers\Url::to(['site/first']));
        } else {            
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        if( Yii::$app->request->cookies->has('data') ){
            Yii::$app->response->cookies->remove('data');
        }
		Yii::$app->user->logout();

        //return $this->goHome();
        return $this->redirect(\yii\helpers\Url::to(['site/login']));
    }

    public function actionFirst(){
        
        return $this->render('first',[
        ]);
    }

    public static function clinicCheck($idUser){
        
        $clinic = Clinic::findOne(['user_id' => $idUser]);                
        $bankClinic = BankClinic::find()->where(['clinic_id' => $clinic->id])->count();        
        $clinicChecked = ['logo','description','working_hour_start','working_hour_end','npwp'];

        foreach($clinic as $k => $v){
            if( in_array($k,$clinicChecked) && is_null($v)){
                return ['step' => Constant::STEP_APOTIK_PROFILE,'msg' => Constant::ERROR_STEP_APOTIK_PROFILE];
            }
        }

        if( $bankClinic == 0 ){
            return ['step' => Constant::STEP_BANK,'msg' => Constant::ERROR_STEP_BANK];
        }

        $doctor = Doctor::find()->where(['clinic_id' => $clinic->id])->count();
        if( $doctor == 0 ){
            return ['step' => Constant::STEP_DOCTOR,'msg' => Constant::ERROR_STEP_DOCTOR];
        }

        $office = Office::find()->where(['clinic_id' => $clinic->id])->count();
        if( $office == 0 ){
            return ['step' => Constant::STEP_OFFICE,'msg' => Constant::ERROR_STEP_OFFICE];
        }       

        return false;
    }    
}
