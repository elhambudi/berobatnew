<?php

namespace clinic\controllers;

class SalesController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->redirect(['list-sales']);
    }

    public function actionListSales()
    {
        return $this->render('list-sales');
    }

    public function actionReport()
    {
        return $this->render('report');
    }
}
