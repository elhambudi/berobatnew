<?php

namespace employee\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans',
        'https://fonts.googleapis.com/css?family=Montserrat|Roboto',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css',
        'css/style.css',
        'css/animation.css',
        'css/custom.css',
		'templates/plugins/datatables/dataTables.bootstrap.css',
		'templates/plugins/font-awesome-4.7.0/css/font-awesome.min.css',
    ];
    public $js = [
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.min.js',
//        'https://use.fontawesome.com/d3bfd1ea60.js',
        'js/main.js',        
//        'https://maps.googleapis.com/maps/api/js?key=AIzaSyBA8lZxrwUg6updFvET6L2qg6BJZhaOPTM&callback=initMap',
        'js/script.js',	
		'templates/plugins/datatables/jquery.dataTables.min.js',
		'templates/plugins/datatables/dataTables.bootstrap.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
