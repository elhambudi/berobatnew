$(function(){
    $('.file-input').change(function(){
        var previewId = $(this).data('container');
        getReviewImage(this,previewId);
    })

    $('#renewpassword').keyup(function(){
        var newPass = $('#newpassword').val();
        if( newPass != $(this).val() ){
            $('#account-btn').attr('disabled','disabled');
            $('#notif-notmatch').css('display','block');
        }else{
            $('#account-btn').removeAttr('disabled')
            $('#notif-notmatch').css('display','none');            
        }
    })
})

function getReviewImage(obj,container){

    if (obj.files && obj.files[0]) {        
        var reader = new FileReader();
        reader.onload = function (e) {
            $(container).attr('src', e.target.result);
        }

        reader.readAsDataURL(obj.files[0]);
    }
}

