<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'assetManager' => [
            'basePath' => '@webroot/site/web/assets/',
            'baseUrl' => '@web/site/web/assets/',
        ],		
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
		'authClientCollection' => [
			'class' => 'yii\authclient\Collection',
			'clients' => [
				'facebook' => [
					'class' => 'yii\authclient\clients\Facebook',
					'clientId' => '1467025086717562',
					'clientSecret' => 'bd90e4416c46c6b551770bc38a013b4f',
					'attributeNames' => ['name', 'email', 'first_name', 'last_name'],
				],
				'google' => [
					'class' => 'yii\authclient\clients\Google',
					'clientId' => '242870596650-5cugtmlu68srbm8jmg0javgt2sbfum6g.apps.googleusercontent.com',
					'clientSecret' => 'amrVOnaaW9tln0ygdq2MHhrH',
					//'returnUrl' => 'http://berobat.elhambudianto.com/site/auth?authclient=google'
					
				],
			],
		],
    ],
    'params' => $params,
];
