<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="become-clinic-partner">
	<section class="form-register">
		<div class="container">
			<h2>Data Profil</h2>
			<?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['member/editprofil','id'=>$model->id]),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
				<div class="form-group">
				<?php
					if($model->gambar!=NULL){
				?>
				<img class="profile-user-img img-responsive" src="<?= Yii::getAlias('@uploadsWeb') ?>/<?=  $model->gambar ?>" style="height:300px; width:300px;">
				<?php
					}else{
				?>
				<img class="profile-user-img img-responsive" src="<?= Yii::getAlias('@uploadsWeb/person.png') ?>" height="100px">
				<?php
					}
				?>
				</div>
				<div class="form-group">
					<?= $form->field($uploadModel,'uploadedFile')->fileInput([
							'class' => "form-control",
							'accept' => 'image/jpg,image/jpeg,image/png,application/pdf'
						])->label('Gambar Profil') ?>
				</div>
				<div class="form-group">
					<?= $form->field($model,'username')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label("Nama")?>
				</div>
				<br>
				<br>
				<div class="form-group">
					<?= $form->field($model,'email')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label("Email")?>
				</div>
				<br>
				<br>
				<div class="form-group">
					<?= $form->field($model,'no_telepon')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label("No Telepon")?>
				</div>
				<br>
				<br>
				<div class="form-group">
					<?=
						$form->field($model,'password_hash')->passwordInput([
							'class' => 'form-control',
							'placeholder' => 'Password'							
						])->label('Password');
					?>
				</div>
				<br>
				<br>
				<div class="form-group">
					<button class="btn btn-warning">										
						<span><i class="fa fa-upload"></i> Edit</span>
					</button>
				</div>
			<?php yii\widgets\ActiveForm::end(); ?>
		</div>
	</section>
</div>