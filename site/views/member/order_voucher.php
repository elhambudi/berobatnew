<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use common\models\Country;
use common\models\Province;
use yii\helpers\ArrayHelper;

$country = Country::find()->all();

$countries = ArrayHelper::map(
	$country,'id','name'
);
?>
<div class="become-clinic-partner">
	<section class="form-register">
		<div class="container">
			<h2>Re-Schedule Penggunaan Voucher</h2>
			<?php $form = ActiveForm::begin([
					'action' => Url::to(['member/change','id'=>$model->id]),
					'method' => 'POST'
			]); ?>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-shopping-cart"></i></span>
					<input type="text" readonly value="<?=$listVoucher->name?>" class="form-control" placeholder="Full Name" aria-describedby="basic-addon1">
				</div>
			</div>
			<div class="row">
			  <div class="col-sm-12">
				<div class="form-group">
					<?= $form->field($model, 'date_used')->widget(DatePicker::classname(), [
								'options' => ['placeholder' => 'Tanggal Penggunaan ...'],
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'yyyy-mm-dd',
									'endDate' => '2017-10-01',
								]
							])->label(false);		
					?>
				</div>
			  </div>
			</div>
			<div class="row">
			  <div class="col-sm-6">
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-full">Save</button>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
					<a href="<?=Url::to(['member/index'])?>" class="btn btn-danger btn-full">Cancel</a>
				</div>
			  </div>
			  <p style="color:red;">*Re-Schedule hanya dapat dilakukan satu kali</p>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
		
	</section>
</div>