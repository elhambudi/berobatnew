<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Situs Beli Voucher Treatment Kesehatan Online Mudah Dan Terpecaya";
?>
<?php
function TanggalIndo($date){
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

	$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
	return($result);
}
?>
<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Voucher Has Purchased</span></div>
			</div>
			<div class="section-action">
				<div class="clear"></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
          <div class="box box-info">
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
				  <th>Nama Voucher</th>
				  <th>Kode Booking</th>
				  <th>Tanggal Penggunaan</th>
				  <th>Status Bayar</th>
				  <th>Status Penggunaan</th>
				  <th>Cara Transfer</th>
                </tr>
                </thead>
                <tbody>					
					<?php 
						$i=1;
						foreach($voucher as $v){
					?>
					<tr>
						<td><?=$i++?></td>
						<td><?php
								$voucher = \common\models\Voucher::findOne($v['voucher_id']);
								echo $voucher->name;
						?></td>
						<td><?php
							if($v['code']==NULL){
								echo '-';
							}else{
								echo $v['code'];
							}
							?></td>
						<td><?php
							if($v['date_used']==NULL){
								echo '-';
							}else{
								echo TanggalIndo($v['date_used']);
							}
						?></td>
						<td><?php
							if($v['status']==0){
								echo '<span class="label label-danger">Belum Dibayar</span>';
							}else{
								echo '<span class="label label-success">Sudah Dibayar</span>';
							}?></td>
						<td><?php
							if($v['status_validate']==0){
								echo '<span class="label label-danger">Belum Digunakan</span>';
							}else{
								echo '<span class="label label-success">Sudah Digunakan</span>';
							}?></td>
						<td>	
							<?php
								if($v['status_validate']==0){?>
							<a href="<?=Url::to(['member/edittanggal','id_voucher'=>$v['voucher_id'],'id_user'=>Yii::$app->user->identity->id,'id'=>$v['id']]);?>" type="button" class="btn active btn-warning"><span class="fa fa-calendar"></span> Re-Schedule</a>
							<?php
								}
							?>
							<a href="<?=Url::to(['member/info','id'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-info"></span> Info Pembayaran</a>
							<?php
								if($v['image']==NULL){?>
							<a href="<?=Url::to(['member/tambah','id'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-money"></span> Upload Bukti Transfer</a>
							<?php
								}else{
							?>
							<a href="<?=Url::to(['member/edit','id'=>$v['id']]);?>" type="button" class="btn active btn-warning"><span class="fa fa-money"></span> Lihat Bukti Transfer</a>
							<?php
								}
							?>
							<!--<a data-toggle="modal" data-id="ISBN-001122" title="Add this item" class="open-AddBookDialog btn btn-primary" href="#addBookDialog">test</a>
							<!--<button data-id="" data-toggle="modal" data-target="#buktiModal">Bukti</button>-->
						</td>	
					</tr>
					<?php
						}
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
			</div>
		</section>
	</div>
</div>