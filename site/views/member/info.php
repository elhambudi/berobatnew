<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<div class="voucher-list">
	<div class="container-fluid">
		<section>

      <div class="row">
        <div class="col-xs-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<div class="pull-right">
						<?= Html::a( "<i class='fa fa-angle-left'></i> Back", ['member/index'], ['class'=>"btn-back"]) ?>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-4">
							<div class="row">
								<div class="col-md-12 col-sm-6">
									<div class="detail-klinik__logo"><img class="profile-user-img img-responsive" src="<?= Url::to('@picfrontend/voucher_photo/'.$listVoucher['image']); ?>" height="100px"></div>
								</div>
								<div class="border-space-15"></div>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="detail-klinik__name">
								<strong>Nama Voucher : <br /></strong>
								<p><?=$listVoucher->name?></p>
							</div>
							<div class="detail-klinik__desc">
								<strong>Voucher Dari Klinik : <br /></strong>
								<p><a href="<?=Url::to(['site/detailklinik','id'=>$listVoucher->clinic_id]);?>" class="the-clinic"><?php
								$clinic = \common\models\Clinic::findOne($listVoucher->clinic_id);
								echo $clinic->name_clinic;
								?></a></p>
							</div>
							<div class="detail-klinik__desc">
								<strong>Deskripsi Voucher : <br /></strong>
								<p><?=$listVoucher->description?></p>
							</div>
							<div class="detail-klinik__desc">
								<strong>Harga Sesungguhnya : <br /></strong>
								<p><?='Rp '.number_format($listVoucher->real_price)?></p>
							</div>		
							<div class="detail-klinik__desc">
								<strong>Harga Voucher : <br /></strong>
								<p><?='Rp '.number_format($listVoucher->voucher_price)?></p>
							</div>		
							<div class="detail-klinik__desc">
								<strong>Diskon : <br /></strong>
								<!--<p><?='Rp '.number_format($listVoucher->discount)?></p>-->
								<p><?=ceil($listVoucher->discount).'%'?></p>
							</div>	
								<hr>							
							<div class="detail-klinik__desc">
								<strong>Transfer ke : <br /></strong>
								<?php
									$bank = \common\models\BankAdmin ::find()->all();
								?>
								<p><?php
									foreach($bank as $b){
										echo 'Bank : '.$b['bank'].'<br>';
										echo 'No Rekening : '.$b['no_rekening'].'<br>';
										echo 'Atas Nama : '.$b['name'].'<br>';
										echo '--------------------------------'.'<br>';
									}
								?></p>
							</div>	
							<div class="detail-klinik__desc">
								<strong>Sejumlah : <br /></strong>
								<?php
									foreach($transaction as $b){
									$p_voucher= \common\models\OrderVoucher::findOne($b['id_voucher']);
									$voucher= \common\models\Voucher::findOne($p_voucher->voucher_id);
									//var_dump($p_voucher);die();
								?>
								<p><?php
									$total = (int)$b['kode_transaksi']+(int)$voucher->voucher_price;
									echo 'Rp '.number_format($total).',-';?></p>
								<?php
									}
								?>
							</div>	
						</div>
					</div>
				</div>
			</div>

		</div>
				</section>
	</div>
</div>