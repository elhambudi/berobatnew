<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use common\models\Country;
use common\models\Province;
use yii\helpers\ArrayHelper;

$model = new common\models\Member();
$country = Country::find()->all();

$countries = ArrayHelper::map(
	$country,'id','name'
);
?>
<div class="become-clinic-partner">
	<section class="form-register">
		<div class="container">
			<h2>Register Your Account</h2>
			<?php $form = ActiveForm::begin([
					'action' => Url::to(['site/daftarmembersocial']),
					'method' => 'POST'
			]); ?>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
					<input type="text" name="User[username]" readonly value="<?=$name?>" class="form-control" placeholder="Full Name" aria-describedby="basic-addon1">
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon2"><i class="fa fa-envelope"></i></span>
					<input type="email" name="User[email]" readonly value="<?=$email?>" class="form-control" placeholder="Email. Contoh: name@example.com" aria-describedby="basic-addon2">
				</div>
			</div>
			<div class="form-group">
					<div class="input-group">
					  	<span class="input-group-addon" id="basic-addon2"><i class="fa fa-phone"></i></span>
					  	<input type="number" name="User[no_telepon]" class="form-control" placeholder="No Telepon. Contoh: 082232566162" aria-describedby="basic-addon2">
					</div>
				</div>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock"></i></span>
					<input type="password" name="User[password_hash]" required class="form-control" placeholder="Password. Minimal 6 karakter" aria-describedby="basic-addon2">
				</div>
			</div>
			<div class="form-group">
				<div class="radio">
					<?php 
						$code = common\models\Gender::find()->all();
						foreach($code as $data){
							echo '<label><input type="radio" name="Member[gender_id]" id="optionsRadios1" value="'.$data['id'].'" checked>'.$data['value'].'</label>&nbsp;&nbsp;&nbsp;';
						}
					?>
				</div>
			</div>
			<div class="form-group">
			<!--					<div class="input-group"> -->
					<?= $form->field($model, 'birtdate')->widget(DatePicker::classname(), [
								'options' => ['placeholder' => 'Enter birth date ...'],
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'yyyy-mm-dd'
								]
							])->label(false);		
					?>
			<!--				  	<span class="input-group-addon" id="basic-addon2"><i class="fa fa-calendar"></i></span>
					<input type="text" id="datepicker" class="form-control" placeholder="Tanggal lahir" aria-describedby="basic-addon2"> -->
			<!--					</div> -->
			</div>
			<div class="form-group">
				<fieldset class="group-address">
					<legend>Address</legend>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<?= 								
									Html::dropDownList(null,'',$countries,[
										'id' => 'country-list',
										'class' => 'form-control',
										'prompt'=>'Select Country...'
									]);
								?>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<?=
									DepDrop::widget([
										'name' => 'province-list',
										'options' => [
											'id' => 'province-list',
											'class' => 'form-control',
										],
										'pluginOptions' => [
											'placeholder' => 'Select Province',
											'url' => Url::to(['location/get-province-by-country']),
											'depends' => ['country-list'],
										]
									])									
								?>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<?=
									$form->field($model,'city_id')->widget(DepDrop::className(),[
										'pluginOptions' => [
											'placeholder' => 'Select City',
											'url' => Url::to(['location/get-city-by-province']),
											'depends' => ['province-list'],
										]
									])->label(false);
								?>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="lr-input__desc">
				Dengan klik Register, kamu telah menyetujui
				<?= Html::a( "Aturan Penggunaan", ['/site/aturanpenggunaan']) ?> dan <?= Html::a( "Kebijakan Privasi", ['/site/kebijakanprivasi']) ?> dari Berobat.id
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-full">Register</button>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</section>
</div>