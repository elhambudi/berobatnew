<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
$this->title = "Situs Beli Voucher Treatment Kesehatan Online Mudah Dan Terpecaya";
?>

<div id="clinic-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Klinik Patner</span></div>
			</div>
			<div class="section-action">
				<div class="pull-right">
					<?= Html::a( "Menjadi Klinik Patner Kami", ['/site/become-clinic-partner'], ['class'=>"btn btn-primary btn-sm"]) ?>
					<!--<a class="btn btn-default btn-sm">Lihat Semua</a>-->
				</div>
				<div class="clear"></div>
			</div>
			<div class="row">
				<?php 
					foreach($clinic as $cli){
				?>
				<div class="col-md-2 col-sm-3 col-xs-12">
					<a href="<?=Url::to(['site/detailklinik','id'=>$cli['id']]);?>" class="the-clinic">
						<div class="logo-clinic" style="background-image: url('<?= Yii::$app->request->baseUrl ?>/site/web/uploads/<?= $cli['logo']?>')"></div>
					</a>
				</div>
				<?php }?>
			</div>
			<?php
				echo LinkPager::widget([
					'pagination' => $pages,
				]);
			?>
		</section>
	</div>
</div>