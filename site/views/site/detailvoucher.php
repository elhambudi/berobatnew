<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
 $jumlahstock =number_format($listVoucher->stock);

?>
<div class="voucher-list">
	<div class="container-fluid">
		<section>
    <?php $form = ActiveForm::begin([
				    'action' => Url::to(['site/purchase','id_voucher'=>$listVoucher->id]),
					'method' => 'POST'
	]); ?>
      <div class="row">
        <div class="col-xs-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<div class="pull-right">
						<?= Html::a( "<i class='fa fa-angle-left'></i> Back", ['site/index'], ['class'=>"btn-back"]) ?>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-4">
							<div class="row">
								<div class="col-md-12 col-sm-6">
									<div class="detail-klinik__logo"><img class="profile-user-img img-responsive" src="<?= Yii::$app->request->baseUrl; ?>/site/web/voucher_photo/<?=$listVoucher['image']?>" height="100px"></div>
								</div>
								<div class="border-space-15"></div>
								<?php
									 if (!Yii::$app->user->isGuest && Yii::$app->user->identity->role_id == 5) {
									       
									        if($jumlahstock!=0){
								?>
								<div class="col-md-12 col-sm-6">
									<!--<a href="<?=Url::to(['site/purchasevoucher','id_voucher'=>$listVoucher->id,'id_user'=>Yii::$app->user->identity->id]);?>" class="btn btn-success btn-block btn-pad">Purchase Voucher</a>-->
									<button type="submit" class="btn btn-success btn-full btn-block btn-pad">Purchase Voucher</button>
								</div>
								<?php
									    }
									}else if( Yii::$app->user->isGuest && $jumlahstock != 0 ){
								?>
								    <div class="col-md-12 col-sm-6">
										<a href="#" class="iframe-login btn btn-success btn-block btn-pad">Purchase Voucher</a>
									</div>
								<?php } ?>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="detail-klinik__name">
								<strong>Voucher Name : <br /></strong>
								<p><?=$listVoucher->name?></p>
							</div>
							<div class="detail-klinik__name">
								<strong>Voucher Dari Klinik : <br /></strong>
								<p><a href="<?=Url::to(['site/detailklinik','id'=>$listVoucher->clinic_id]);?>" class="the-clinic"><?php
								$clinic = \common\models\Clinic::findOne($listVoucher->clinic_id);
								echo $clinic->name_clinic;
								?></a></p>
							</div>
							<div class="detail-klinik__desc">
								<strong>Deskripsi Detail : <br /></strong>
								<p><?=$listVoucher->description?></p>
							</div>
							<div class="detail-klinik__desc">
								<strong>Real Price : <br /></strong>
								<p><?='Rp '.number_format($listVoucher->real_price)?></p>
							</div>		
							<div class="detail-klinik__desc">
								<strong>Voucher Price : <br /></strong>
								<p><?='Rp '.number_format($listVoucher->voucher_price)?></p>
							</div>		
							<div class="detail-klinik__desc">
								<strong>Discount : <br /></strong>
								<!--<p><?='Rp '.number_format($listVoucher->discount)?></p>-->
								<p><?=ceil($listVoucher->discount).'%'?></p>
							</div>	
							<div class="detail-klinik__desc">
								<strong>Stock : <br /></strong>
								<p><?=number_format($listVoucher->stock)?></p>
							</div>
							<div class="detail-klinik__desc">
								<strong>Tanggal Kadaluarsa : <br /></strong>
								<p><?=$listVoucher->expire_date ?></p>
							</div>
							<div class="detail-klinik__desc">
								<strong>Tanggal Penggunaan : <br /></strong>
								<p>
								    <?= $form->field($model, 'date_used')->widget(DatePicker::classname(), [
								            'options' => ['placeholder' =>                      'Tanggal Penggunaan ...'],
								            
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'yyyy-mm-dd',
									'endDate' => $listVoucher->expire_date
                								]
                							])->label(false);		
                					?>
                				
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
				</section>
				<?php ActiveForm::end(); ?>
	</div>
</div>