<?php

use yii\helpers\Html;
$this->title = "Hasil Pencarian";
?>

<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Hasil Pencarian</span></div>
			</div>
			<div class="section-action">
				<div class="pull-right">
					<a class="btn btn-default btn-sm">Lihat Semua</a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="row">
				<?php foreach( $listVoucher as $v ): ?>
				<div class="col-md-3 col-sm-6">
					<div class="item">
						<div class="box">
							<div class="image" style="background-image:url('<?= Yii::getAlias('@uploadsWeb') ?>/<?= $v->image ?>')">
								<div class="wrap-hover"><a href="#">Lihat Detail</a></div>
								<div class="wrap-clinic">
									<a class="the-clinic"><i class="fa fa-map-marker"></i> 
										<?= $v->clinic->name_clinic ?>
									</a>
								</div>
							</div>
							<div class="price">
								<div class="the-price">
									<div class="voucher">Rp <?= $v->voucher_price ?></div>
									<div class="real">Rp <?= $v->real_price ?></div>
								</div>
								<div class="the-discount">
									<div class="text">Save up to</div>
									<div class="nominal"><?= $v->discount ?><span class="the-percent">%</span></div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="name">
								<?= $v->name ?>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</section>
	</div>
</div>