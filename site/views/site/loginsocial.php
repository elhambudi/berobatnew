<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use common\models\Country;
use common\models\Province;
use yii\helpers\ArrayHelper;

$model = new common\models\Member();
$country = Country::find()->all();

$countries = ArrayHelper::map(
	$country,'id','name'
);
?>
<div class="become-clinic-partner">
	<section class="form-register">
		<div class="container">
			<h2>Login to Your Account</h2>
			<?php $form = ActiveForm::begin([
						'action' => Url::to(['site/loginmember']),
						'method' => 'POST'
				]); ?>
				<div class="form-group">
					<div class="input-group">
					  	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
					  	<input type="text" name="LoginForm[username]" value="<?=$email?>" required readonly class="form-control" placeholder="Email Anda" aria-describedby="basic-addon1">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  	<span class="input-group-addon" id="basic-addon2"><i class="fa fa-lock"></i></span>
					  	<input type="password" name="LoginForm[password]" required class="form-control" placeholder="Password Anda" aria-describedby="basic-addon2">
					</div>
				</div>
				<div class="lr-input__desc">
					<div class="clear"></div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-full">Login</button>
				</div>
				<?php ActiveForm::end(); ?> 
		</div>
	</section>
</div>