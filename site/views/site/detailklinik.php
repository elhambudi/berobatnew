<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>
<div class="voucher-list">
	<div class="container-fluid">
		<section>

		<div class="row">
			<div class="col-xs-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<div class="pull-right">
							<?= Html::a( "<i class='fa fa-angle-left'></i> Back", ['site/index'], ['class'=>"btn-back"]) ?>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-sm-4">
								<div class="row">
									<div class="col-md-12 col-sm-6">
										<div class="detail-klinik__logo"><img class="profile-user-img img-responsive" src="<?= Yii::$app->request->baseUrl; ?>/site/web/clinic_photo/<?=$listClinic['logo']?>" height="100px"></div>
									</div>
								</div>
							</div>
							<div class="col-sm-8">
								<div class="detail-klinik__name">
									<strong>Nama Klinik : <br /></strong>
									<p><?=$listClinic->name_clinic?></p>
								</div>		
								<div class="detail-klinik__name">
									<strong>Kategori </strong> : <?php 
										$code = common\models\CategoryHasClinic::find()->where(['clinic_id'=>$listClinic->id])->all();
										foreach($code as $co){
											$code1 = common\models\Category::find()->where(['id'=>$co['category_id']])->all();
											foreach($code1 as $c){
												echo '<span class="detail-klinik__status label label-primary">'.$c['name'].'</span> ';
											}
										}
									?>
								</div>
								<div class="detail-klinik__name">
									<strong>Deskripsi Klinik : <br /></strong>
									<?=$listClinic->description?> 
								</div>			
								<div class="detail-klinik__name">
									<table>
									<tr>
											<th>Email</th>
											<td>:</td>
											<td><?php 
												$code = common\models\User::find()->where(['id'=>$listClinic->user_id])->all();
												foreach($code as $co){
													echo $co['email'];
												}
											?></td>
										</tr>
										<tr>
											<th>Phone</th>
											<td>:</td>
											<td><?=$listClinic->phone?></td>
										</tr>
									</table>
								</div>
								<div class="detail-klinik__office">
									<div class="the-title"><strong>Alamat Klinik: </strong></div>
									<ul>
										<?php 
											$code = common\models\Office::find()->where(['clinic_id'=>$listClinic->id])->all();
											foreach($code as $co){
												$code1 = common\models\City::find()->where(['id'=>$co['city_id']])->all();
												foreach($code1 as $c){
													echo '<li>
															<div class="the-mark"><i class="fa fa-map-marker"></i>
															<span>'.$co['address'].'</span><br>
																<span>'.$c['name'].'</span><br>
															</div>
														<div class="clear"></div>
													</li>';
												}
											}
										?>
									</ul>
								</div>							
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Vouchers</span></div>
			</div>
			<div class="row">
				<?php foreach($listVoucher as $v): ?>
				<div class="col-md-3 col-sm-6">
					<div class="item">
						<div class="box">
							<div class="image" style="background-image:url('<?= Yii::$app->request->baseUrl; ?>/site/web/voucher_photo/<?=$v['image']?>')">
								<div class="wrap-hover"><a href="<?=Url::to(['site/detail','id'=>$v['id']]);?>">View Detail</a></div>
								
							</div>
							<div class="price">
								<div class="the-price">
									<div class="voucher">Rp <?=number_format($v->voucher_price) ?></div>
									<div class="real">Rp <?= number_format($v->real_price) ?></div>
								</div>
								<div class="the-discount">
									<div class="text">Save up to</div>
									<div class="nominal"><?= round($v->discount) ?><span class="the-percent">%</span></div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</section>
				
	</div>
</div>