<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
$this->title = "Situs Beli Voucher Treatment Kesehatan Online Mudah Dan Terpecaya";
?>

<div class="voucher-list">
	<div class="container-fluid">
		<section>
			<div class="section-title">
				<div class="section-title__border"></div>
				<div class="section-title__header"><span>Voucher Hari Ini</span></div>
			</div>
			<div class="section-action">
				<div class="pull-right">
					
				</div>
				<div class="clear"></div>
			</div>
			<div class="row">
				<?php
					foreach($voucher as $v){
				?>
				<div class="col-md-3 col-sm-6">
					<div class="item">
						<div class="box">
							<div class="image" style="background-image:url('<?= Yii::$app->request->baseUrl; ?>/site/web/voucher_photo/<?=$v['image']?>')">
								<div class="wrap-hover"><a href="<?=Url::to(['site/detail','id'=>$v['id']]);?>">Lihat Detail</a></div>
							
							</div>
							<div class="price">
								<div class="the-price">
									<div class="voucher">Rp <?=number_format($v['voucher_price'])?></div>
									<div class="real">Rp <?=number_format($v['real_price'])?></div>
								</div>
								<div class="the-discount">
									<div class="text">Save up to</div>
									<div class="nominal"><?= ceil($v->discount) ?><span class="the-percent">%</span></div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
				<?php
					}
				?>
			</div>
			<?php
				echo LinkPager::widget([
					'pagination' => $pages,
				]);
			?>
		</section>
	</div>
</div>