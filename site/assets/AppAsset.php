<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/site/web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans',
        'https://fonts.googleapis.com/css?family=Montserrat|Roboto',
  //      'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        'css/style.css',
        'css/header.css',
        'css/footer.css',
        'css/animation.css',
        'css/login-register.css',
	    'jquery-ui/jquery-ui.css',
		'templates/plugins/datatables/dataTables.bootstrap.css',
		'templates/plugins/font-awesome-4.7.0/css/font-awesome.min.css',
    ];
    public $js = [
//		'http://code.jquery.com/jquery-1.11.0.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'https://use.fontawesome.com/d3bfd1ea60.js',
        'js/script.js',     
		'jquery-ui/jquery-ui.min.js',
		'jquery-ui/jquery-ui.js',
		'templates/plugins/datatables/jquery.dataTables.min.js',
		'templates/plugins/datatables/dataTables.bootstrap.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
