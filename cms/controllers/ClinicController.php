<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Clinic;
use common\models\Employee;
use common\models\ClinicSearch;
use common\models\Constant;
use common\models\Office;
use common\models\Voucher;
use yii\helpers\Url;

/**
 * Site controller
 */
class ClinicController extends Controller
{
    
public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
				'only' => ['index','detail','approve','nonapprove','remove'],
                'rules' => [
                    [
                        'actions' => ['index','detail','approve','nonapprove','remove'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'remove' => ['post']
                ],
            ],
        ];
    }    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ClinicSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$data = Clinic::find()->all();
		return $this->render('index',[
            'search' => $searchModel,
            'provider' => $dataProvider,
			'model' => $data
		]);
    }

    public function actionDetail($id)
    {
		$data = Clinic::findOne($id);

        $searchModel = new \common\models\ClinicSearch();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\Voucher::find()->where([
                'clinic_id' => $data->id
            ])
        ]);
		
		$clinic = Clinic::findOne($id);
		$office = Office::find()->where(['clinic_id'=>$clinic['id']])->all();
		$id_office = array();
		foreach($office as $o){
			$id_office[] = $o['id'];
		}
		$listOfVoucher = Voucher::find()->where(['clinic_id' => $clinic->id])->all();
		//var_dump($data);die();
        return $this->render('detail',[
			'data'=>$data,
			'voucher'=>$listOfVoucher,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
		]);
    }
	
	public function actionAccept($id_voucher){
		$model = Voucher :: findOne($id_voucher);
		$model->status =1;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher has been Accepted.');
		return $this->redirect(['clinic/detail','id'=>$model->clinic_id]);
	}
	public function actionNonaccept($id_voucher){
		$model = Voucher :: findOne($id_voucher);
		$model->status =0;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher has been Accepted.');
		return $this->redirect(['clinic/detail','id'=>$model->clinic_id]);
	}
	public function actionAddemployee($id)
    {
		$model = new \common\models\Employee();
		$office = \common\models\Office::find()->where(['clinic_id'=>$id])->all();
        return $this->render('addemployee',[
			'model'=>$model,
			'id' =>$id,
			'office'=>$office,
		]);
    }
	public function actionUbahemployee($id)
    {
		$model = \common\models\Employee::findOne($id);
		$id_user = $model->user_id;
		$office = \common\models\Office::findOne($model->office_id);
		$clinic = \common\models\Clinic::find()->where(['id'=>$office->clinic_id])->all();
		foreach($clinic as $c){
			$id_clinic = $c->id;
		}
		//var_dump($office);die();
		$model2 = \common\models\User::findOne($id_user);		
		$listoffice = \common\models\Office::find()->where(['clinic_id'=>$id_clinic])->all();
		//var_dump($office);die();
        return $this->render('editemployee',[
			'model'=>$model,
			'model2'=>$model2,
			'id' =>$id_clinic,
			'id_user' =>$id_user,
			'office'=>$listoffice,
		]);
    }
	public function actionApprove($id)
    {
		$model = Clinic::findOne($id);
		$data = Clinic::find()->all();
		$model->status_approval =1;
		$model->save(false);
		Yii::$app->getSession()->setFlash('success', '<i class="fa fa-edit"></i> Status Approval berhasil di ubah.');
        return $this->redirect(Url::to(['/clinic']));
    }
	public function actionNonapprove($id)
    {
		$model = Clinic::findOne($id);
		$data = Clinic::find()->all();
		$model->status_approval = Constant::STATUS_NONACTIVE;
		$model->save(false);
		Yii::$app->getSession()->setFlash('success', '<i class="fa fa-edit"></i> Status Approval berhasil di ubah.');
        return $this->redirect(Url::to(['/clinic']));
    }

    public function actionRemove($id){
        Clinic::findOne($id)->delete();
        return json_encode(['status' => 1]);
    }
	
	public function actionCreateemployee($id){
		$model = new User();
		$username = Yii::$app->request->post('User');
        $model->username = $username['email'];
        $model->email = $username['email'];
        $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash('initial');
		//var_dump($model->password_hash);die();
        $model->generateAuthKey();
		$model->role_id = Constant::ROLE_EMPLOYEE;
		$model->status=0;
		$model->created_at=11111;
		$model->updated_at=11111;
		$model->save();
		if($model->save()){
		   $modelmember = new Employee();
		   $member = Yii::$app->request->post('Employee');
		   //var_dump($member);die();
		   $modelmember->name = $member['name'];
		   $modelmember->phone = $member['phone'];
		   $modelmember->office_id = $member['office_id'];
		   $modelmember->user_id = $model->getPrimaryKey();
		   $modelmember->save();
		}else{
			var_dump( $model->errors);
		   die();
		}
		return $this->redirect(['list','id'=>$id]);
	}
	public function actionEditemployee($id,$id_user){
		$em = \common\models\Employee::findOne($id);
		$office = \common\models\Office::findOne($em->office_id);
		$clinic = \common\models\Clinic::find()->where(['id'=>$office->clinic_id])->all();
		foreach($clinic as $c){
			$id_clinic = $c->id;
		}
		
		$model = User::findOne($id_user);
		$username = Yii::$app->request->post('User');
        $model->username = $username['email'];
        $model->email = $username['email'];
        $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash('initial');
		//var_dump($model->password_hash);die();
        $model->generateAuthKey();
		$model->role_id = Constant::ROLE_EMPLOYEE;
		$model->status=0;
		$model->created_at=11111;
		$model->updated_at=11111;
		$model->save();
		if($model->save()){
		   $modelmember = Employee::findOne($id);
		   $member = Yii::$app->request->post('Employee');
		   //var_dump($member);die();
		   $modelmember->name = $member['name'];
		   $modelmember->phone = $member['phone'];
		   $modelmember->office_id = $member['office_id'];
		   $modelmember->user_id = $model->getPrimaryKey();
		   $modelmember->save();
		}else{
			var_dump( $model->errors);
		   die();
		}
		return $this->redirect(['list','id'=>$id_clinic]);
	}
	
	public function actionDeleteemployee($id){
		$em = \common\models\Employee::findOne($id);
		$office = \common\models\Office::findOne($em->office_id);
		$clinic = \common\models\Clinic::find()->where(['id'=>$office->clinic_id])->all();
		foreach($clinic as $c){
			$id_clinic = $c->id;
		}
		
		$model = Employee::findOne($id);
		$user_id = $model->user_id;
        $model->delete();
		User::findOne($user_id)->delete();
        return $this->redirect(['list','id'=>$id_clinic]);
    }
	
	public function actionList($id)
    {
		$data = Office::find()->where(['clinic_id'=>$id])->all();
		//$list_office=array();
		foreach($data as $d){
			$list_office[] = $d['id'];
		}
		//var_dump($list_office);die();
        $searchModel = new \common\models\EmployeeSearch();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\Employee::find()->where([
                'office_id' => $list_office
            ])
        ]);
		$employee = \common\models\Employee ::find()->where(['in','office_id',$list_office])->all();
        return $this->render('listemployee',[
			'data'=>$data,
			'employee'=>$employee,
			'id' => $id,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
		]);
    }

}
