<?php
namespace cms\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\LogoHeader;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class HeaderController extends Controller {

    /**
     * @var boolean whether to enable CSRF validation for the actions in this controller.
     * CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
     */
    //public $enableCsrfValidation = false;
    
    public function actionIndex() {
		$this->layout="main";
		$logo = LogoHeader::findOne(['id' => 1]);
	    return $this->render('edit', [
			'model' => $logo
		]);
    }
	public function actionEditform($id) {
		$logo = LogoHeader::findOne(['id' => $id]);
		return $this->render('edit', [
			'model' => $logo,
		]);
    }
	public function actionEdit($id) {
            $model = $this->findModel($id);
			if (Yii::$app->request->isPost) {
				if ($model->load(Yii::$app->request->post())) {
					$uploadModel = UploadedFile::getInstance($model, 'gambar');
					if ($uploadModel) {
						$namaLogo = $uploadModel->baseName;
						if ($uploadModel->extension == "jpg" || $uploadModel->extension == "jpeg" || $uploadModel->extension == "png") {
							$uploadModel->saveAs('../../site/web/logo_header/' . $namaLogo . '.' . $uploadModel->extension);
							$model->gambar = $namaLogo . '.' . $uploadModel->extension;
						} else {
							return $this->render('index', [
								'model' => $model,
							]);
						}
					}
					
					if ($model->save()) {         
						//var_dump('test');die();
						Yii::$app->getSession()->setFlash('success', '<i class="fa fa-edit"></i> Logo Header berhasil di ubah.');
						return $this->redirect('index');
					} else {                    
						return $this->render('index', [
									'model' => $model,
						]);
					}
				}
			} else {
				return $this->render('create', [
							'model' => $model,
				]);
			}
            if( !$model->save() ){
               var_dump( $model->errors);
               die();
            }
           return $this->redirect(['index']);
    } 
	public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	protected function findModel($id)
    {
        if (($model = LogoHeader::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            echo "error";
        }
    }
	
}
