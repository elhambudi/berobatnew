<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\Cakrawala;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class CakrawalaController extends Controller {
    /**
     * @var boolean whether to enable CSRF validation for the actions in this controller.
     * CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
     */
    //public $enableCsrfValidation = false;
    
    public function actionIndex() {
		$this->layout="main";
		$logo = Cakrawala::find()->all();
	    return $this->render('index', [
			'model' => $logo
		]);
    }
	public function actionCreate() {
		$logo = new Cakrawala();
		return $this->render('create', [
			'model' => $logo,
		]);
    }
	public function actionEditform($id) {
		$logo = Cakrawala::findOne(['id' => $id]);
		return $this->render('edit', [
			'model' => $logo,
		]);
    }
	public function actionEditform1($id) {
		$logo = Cakrawala::findOne(['id' => $id]);
		return $this->render('edit_file', [
			'model' => $logo,
		]);
    }
	public function actionSubmit() {
            $model = new Cakrawala();
			$model->date_created = date("Y-m-d");
			if (Yii::$app->request->isPost) {
				if ($model->load(Yii::$app->request->post())) {
					$uploadModel = UploadedFile::getInstance($model, 'file');
					if ($uploadModel) {
						$namaLogo = $uploadModel->baseName;
						if ($uploadModel->extension == "doc" || $uploadModel->extension == "docx" || $uploadModel->extension == "pdf") {
							$uploadModel->saveAs('../../frontend/web/cakrawala/' . $namaLogo . '.' . $uploadModel->extension);
							$model->file = $namaLogo . '.' . $uploadModel->extension;
						} else {
							return $this->render('index', [
								'model' => $model,
							]);
						}
					}
					
					if ($model->save()) {         
						//var_dump('test');die();
						Yii::$app->getSession()->setFlash('success', '<i class="fa fa-save"></i> Data Cakrawala berhasil di tambah.');
						return $this->redirect('index');
					} else {                    
						return $this->render('index', [
									'model' => $model,
						]);
					}
				}
			} else {
				return $this->render('create', [
							'model' => $model,
				]);
			}
            if( !$model->save() ){
               var_dump( $model->errors);
               die();
            }
           return $this->redirect(['index']);
    } 
	public function actionEdit($id) {
            $model = $this->findModel($id);
			if (Yii::$app->request->isPost) {
				if ($model->load(Yii::$app->request->post())) {
					$uploadModel = UploadedFile::getInstance($model, 'file');
					if ($uploadModel) {
						$namaLogo = $uploadModel->baseName;
						if ($uploadModel->extension == "doc" || $uploadModel->extension == "docx" || $uploadModel->extension == "pdf") {
							$uploadModel->saveAs('../../frontend/web/cakrawala/' . $namaLogo . '.' . $uploadModel->extension);
							$model->file = $namaLogo . '.' . $uploadModel->extension;
						} else {
							return $this->render('index', [
								'model' => $model,
							]);
						}
					}
					
					if ($model->save()) {         
						//var_dump('test');die();
						Yii::$app->getSession()->setFlash('success', '<i class="fa fa-edit"></i> Data Cakrawala berhasil di ubah.');
						return $this->redirect('index');
					} else {                    
						return $this->render('index', [
									'model' => $model,
						]);
					}
				}
			} else {
				return $this->render('create', [
							'model' => $model,
				]);
			}
            if( !$model->save() ){
               var_dump( $model->errors);
               die();
            }
           return $this->redirect(['index']);
    } 
	public function actionDelete($id)
    {
        $this->findModel($id)->delete();
		Yii::$app->getSession()->setFlash('success', '<i class="fa fa-trash"></i> Data Cakrawala berhasil di hapus.');
        return $this->redirect(['index']);
    }
	
	protected function findModel($id)
    {
        if (($model = Cakrawala::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            echo "error";
        }
    }
	
}
