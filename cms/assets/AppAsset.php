<?php

namespace cms\assets;

use yii\web\AssetBundle;

/**
 * Main cms application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
		'templates/bootstrap/css/bootstrap.min.css',
		'templates/font-awesome-4.7.0/css/font-awesome.min.css',
		'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
		'templates/plugins/datatables/dataTables.bootstrap.css',
		'templates/dist/css/AdminLTE.min.css',
		'templates/dist/css/skins/_all-skins.min.css',
		'templates/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
    ];
    public $js = [
		//'templates/plugins/jQuery/jquery-2.2.3.min.js',
		'templates/bootstrap/js/bootstrap.min.js',
		'templates/plugins/datatables/jquery.dataTables.min.js',
		'templates/plugins/datatables/dataTables.bootstrap.min.js',
		'templates/plugins/slimScroll/jquery.slimscroll.min.js',
		'templates/plugins/datepicker/bootstrap-datepicker.js',
		'templates/plugins/fastclick/fastclick.js',
		'templates/dist/js/app.min.js',
		'templates/dist/js/demo.js',
		'templates/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
		'templates/plugins/canvasjs/canvasjs.min.js',
		'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
