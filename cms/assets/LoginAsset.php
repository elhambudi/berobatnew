<?php

namespace cms\assets;

use yii\web\AssetBundle;

/**
 * Main cms application asset bundle.
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans|Roboto',
        '../../common/assets/plugins/tooltipster/css/tooltipster.bundle.min.css',        
        'template_login/css/custom.css',
    ];
    public $js = [
		'template_login/js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
