<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use kartik\file\FileInput;
use kartik\select2\Select2;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\search\BeaconSearch $searchModel
 */
$this->title = 'Tambah Data Buku';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Slider
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Slider</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Slider</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['slider/submit']),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
				<div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Gambar</label>
                  <?= $form->field($model,'gambar')->fileInput()->label(false);?>
                </div>
              </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</button>
				  <a href="<?= Url::to(["slider/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->