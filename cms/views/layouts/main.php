<?php

//$segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));

/* @var $this \yii\web\View */
/* @var $content string */

use cms\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Berobat</title>
    <?php $this->head() ?>

</head>
<body class="hold-transition skin-red layout-boxed sidebar-mini">
<?php $this->beginBody() ?>
   <div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>CMS Berobat</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?= Url::to(['site/logout'])?>" data-method="post">
			  <span class="fa fa-sign-out"> Logout</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= Yii::$app->urlManager->createUrl(["/templates/dist/img/user8-128x128.jpg"]); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
	  
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
		<?php if(Yii::$app->controller->id == 'site'  && Yii::$app->controller->action->id == 'index'){?>
			<li class="active"><a href="<?= Url::to(['site/index'])?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
		<?php }else{ ?>
			<li><a href="<?= Url::to(['site/index'])?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
		<?php } ?>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Header</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                <li><a href="<?= Url::to(['slider/index'])?>"><i class="fa fa-database"></i> <span>Slider</span></a></li>
		        <li><a href="<?= Url::to(['header/index'])?>"><i class="fa fa-database"></i> <span>Logo</span></a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Footer</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                <li><a href="<?= Url::to(['footer/index'])?>"><i class="fa fa-database"></i> <span>Deskripsi Berobat</span></a></li>
		        <li><a href="<?= Url::to(['socialmedia/index'])?>"><i class="fa fa-database"></i> <span>Social Media</span></a></li>
          </ul>
        </li>
		<li><a href="<?= Url::to(['aboutus/index'])?>"><i class="fa fa-laptop"></i> <span>Data Halaman About Us</span></a></li>
		<li><a href="<?= Url::to(['help/index'])?>"><i class="fa fa-laptop"></i> <span>Data Halaman Help</span></a></li>
		<li><a href="<?= Url::to(['faq/index'])?>"><i class="fa fa-laptop"></i> <span>Data Halaman FAQ</span></a></li>
		<li><a href="<?= Url::to(['aturanpenggunaan/index'])?>"><i class="fa fa-laptop"></i> <span>Data Halaman Aturan</span></a></li>
		<li><a href="<?= Url::to(['kebijakanprivasi/index'])?>"><i class="fa fa-laptop"></i> <span>Data Halaman Kebijakan</span></a></li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Halaman Become Clinic</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                <li><a href="<?= Url::to(['benefit/index'])?>"><i class="fa fa-database"></i> <span>Benefit</span></a></li>
                <li><a href="<?= Url::to(['programclinic/index'])?>"><i class="fa fa-database"></i> <span>Program Clinic</span></a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

        <?= $content; ?>

	<footer class="main-footer">
		<div class="pull-right hidden-xs">
		  <b>Version</b> 2.3.7
		</div>
		<strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.Admin LTE</strong> All rights
		reserved.
	  </footer>

	</div>
<?php $this->endBody() ?>
    <!-- this page specific inline scripts -->
	
		<script>
	  $(function () {
		$("#example1").DataTable();
		$('#example2').DataTable({
		  "paging": true,
		  "lengthChange": false,
		  "searching": false,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});
	  });
	  $(function(){
			$(".datepicker").datepicker({
				dateFormat:"yy-mm-dd"
			});
			$(".datepicker2").datepicker({
				dateFormat:"yy-mm-dd"
			});
			$(".datepicker3").datepicker({
				dateFormat:"yy-mm-dd"
			});
		});
	</script>
</body>
</html>
<?php $this->endPage() ?>
