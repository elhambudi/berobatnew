<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Data Buku';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Buku
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Buku</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <a href="<?= Url::to(['buku/create'])?>" class="btn btn-flat " ><span class="fa fa-plus"></span> Tambah Buku
			  </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Nama Buku</th>
                  <th>Nama Pengarang</th>
                  <th>Nama Penerbit</th>
                  <th>Kategori</th>
                  <th>Gambar</th>
                  <th>Deskripsi</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
				<?php
					$i = 1;
					foreach ($buku as $row) {
				?>
                <tr>
                  <td><?= $i++ ?></td>
                  <td><?= $row['nama_buku'] ?></td>
                  <td><?= $row['nama_pengarang'] ?></td>
                  <td><?= $row['nama_penerbit'] ?></td>
                  <td><?= $row['nama_kategori'] ?></td>
                  <td><img src="<?= Yii::$app->urlManager->createUrl(["../../frontend/web/upload/buku/".$row['gambar']]); ?>" width="100px" height="100px"></td>
                  <td><?= $row['deskripsi'] ?></td>
                  
				  <td>
				   <a href="<?= Url::to(['buku/editform','id'=>$row['id']])?>" type="button" class="btn active btn-warning"><span class="fa fa-pencil"></span> Edit</a>
				   <a href="<?= Url::to(['buku/delete','id'=>$row['id']])?>" type="button" class="btn active btn-danger "><span class="fa fa-trash"></span> Hapus</a>
				  </td>
                </tr>
				<?php
					}
				?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->