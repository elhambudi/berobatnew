<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\file\FileInput;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\search\BeaconSearch $searchModel
 */
$this->title = 'Edit Data Buku';

//var_dump($model);die();
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Program Clinic
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Footer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Program Clinic</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['programclinic/edit','id'=>$model->id]),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
			 <div class="box-body">
				<div class="form-group">
                  <label for="idkategori">ID Program Clinic</label>
                  <input type="text" class="form-control" value="<?= $model->id?>" readonly>
                </div>
            </div>
			 <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Judul</label>
					<?= $form->field($model,'judul')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Deskripsi</label>
					<?= $form->field($model, 'description')->widget(\yii\redactor\widgets\Redactor::className())->label(false) ?>
                </div>
              </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</button>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->