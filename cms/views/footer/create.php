<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\file\FileInput;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\search\BeaconSearch $searchModel
 */
$this->title = 'Tambah Data Buku';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Logo Header
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Logo Header</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Buku</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['buku/submit']),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
			 <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Nama Buku</label>
                  <?= $form->field($model,'nama_buku')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
			<div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Kategori</label>
				  <select class="form-control" name="Buku[id_kategori]">
					<?php 
						foreach($kategori as $data){	
					?>
						<option value="<?= $data['id_kategori']?>"><?= $data['nama_kategori']?></option>
					<?php
						}
					?>
					</select>
                </div>
            </div>
			<div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Penerbit</label>
                   <select class="form-control" name="Buku[id_penerbit]">
					<?php 
						foreach($penerbit as $data){	
					?>
						<option value="<?= $data['id_penerbit']?>"><?= $data['nama_penerbit']?></option>
					<?php
						}
					?>
					</select>
                </div>
              </div> 
			  <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Pengarang</label>
                   <select class="form-control" name="Buku[id_pengarang]">
					<?php 
						foreach($pengarang as $data){	
					?>
						<option value="<?= $data['id_pengarang']?>"><?= $data['nama_pengarang']?></option>
					<?php
						}
					?>
					</select>
                </div>
              </div> 
			  <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Gambar</label>
                  <?= $form->field($model,'gambar')->widget(FileInput::classname(), [
						'options' => ['accept' => 'image/*'],
					])->label(false);
				  
				  ?>
                </div>
              </div>
			  <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Deskripsi</label>
                  <?= $form->field($model,'deskripsi')->textArea([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</button>
				  <a href="<?= Url::to(["peminjaman/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->