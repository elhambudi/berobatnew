var getLoadingText = function(){
    return "<i class='fa fa-spinner fa-pulse'/>";
}

$(function(){
    $('#remove-clinic').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var url = $(this).attr('href');
        var text = $(this).text(); 
        $.ajax({
            url : url,
            type : 'post',
            beforeSend : function(){
                $(this).html(getLoadingText());
            },
            success : function(response){
                var data = JSON.parse(response);
                window.location.href="index";
            },
            error : function(){
                $(this).html(text);
            }
        })
    })
})

