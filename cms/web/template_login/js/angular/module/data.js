angular.module('data',[
])
.factory('JoinArrayToStringFactory',function(){
	
	var service = {};
	
	service.join = function(arr,separator = ","){
		return arr.join(',');
	}
	
	service.objectJoin = function(arr,separator = ",",objectName = "index"){
		return arr.map(function(elem){
			return elem[objectName]
		}).join(separator);
	}
	
	return service;
})

