$(document).ready(function () {
	function setTooltip(){
		$('[data-tooltip="yes"]').tooltipster({
		   animation: 'grow',
		   theme: 'optimus',
		   side: 'bottom',
		   maxWidth: 200,
		   delay: [300,100]
		   // trigger: 'click'
		});
	}
	setTooltip();
});