var app = angular.module('clinic', [
	'rest-yii2',
	'data',
	'ui.bootstrap',
	'datatables'
]);

app.controller('ClinicIndexController', function ($scope, $rootScope, RestFactory, API_URL, JoinArrayToStringFactory, $uibModal) {

	RestFactory.rest(API_URL, 'city').getAll({}, {}, function (response) {
		var arr = [];
		response.data.forEach(function (elem) {
			arr[elem.id] = elem;
		})
		$scope.office = arr;
	})

	RestFactory.rest(API_URL, 'clinic').getAll({}, {}, function (response) {
		$scope.listClinic = response.data;
	})

	$scope.joinArrayToString = function (arr) {
		return JoinArrayToStringFactory.objectJoin(arr, ",", 'name');
	}

	$scope.openModal = function (index) {
		$uibModal.open({
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: $rootScope.baseUrl + 'template/clinic-approv.html',
			controller: function ($scope, $uibModal, $rootScope, $resource, listClinic, index,
				JoinArrayToStringFactory, office, uploadUrl) {

				$scope.uploadUrl = uploadUrl;
				$scope.detailClinic = listClinic[index];
				$scope.categories = JoinArrayToStringFactory.objectJoin($scope.detailClinic.categories, ",", "name");

				$scope.getCityName = function (id) {
					return office[id].name;
				}


				var confirm = function (fn) {
					console.log(fn);
					$uibModal.open({
						ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						templateUrl: $rootScope.baseUrl + 'template/confirmation.html',
						controller: function ($scope, $uibModalInstance) {
							$scope.confirm = function (status) {
								fn(status == 1);
								$uibModalInstance.dismiss();
							}
						},
						resolve: {
							fn: function () {
								return fn;
							}
						}
					});

				}

				$scope.remove = function () {
					confirm(function (resp) {
						if (resp) {
							RestFactory.rest(API_URL, 'clinic').remove({
								id: $scope.detailClinic.id
							}, {}, function (response) {
								if (response.status === 1) {
									delete $scope.detailClinic;
									listClinic.splice(index, 1);
								}
							});
						}
					})
				}


				$scope.activate = function () {
					confirm(function (resp) {
						if (resp) {
							var rest = $resource("", {}, {
								approv: {
									method: "GET",
									url: API_URL + "clinic/approve",
								}
							}).approv({
								id: $scope.detailClinic.id
							}, {}, function (response) {
								if (response.status == 1) {
									$scope.detailClinic.status_approval = 1;
								}
							});
						}
					})
				}

				$scope.nonactive = function () {
					confirm(function (resp) {
						if (resp) {
							var rest = $resource("", {}, {
								approv: {
									method: "GET",
									url: API_URL + "clinic/deactive",
								}
							}).approv({
								id: $scope.detailClinic.id
							}, {}, function (response) {
								if (response.status == 1) {
									$scope.detailClinic.status_approval = 0;
								}
							});
						}
					})
				}


			},
			resolve: {
				listClinic: function () {
					return $scope.listClinic;
				},
				index: function () {
					return index;
				},
				office: function () {
					return $scope.office;
				},
				uploadUrl: function () {
					return UPLOADS_FOLDER_URL;
				}
			}
		});
	}

})
