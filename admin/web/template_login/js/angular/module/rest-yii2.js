var app = angular.module('rest-yii2',[]);

app.factory('RestFactory', function ($resource, $http) {

    var service = {};

    var getRestResponse = function (apiUrl,controllerName, custom,prefix) {

        if (typeof prefix == 'undefined')
            prefix = "";
		
		var defaultRest = {
            'getDetail': {
                url: prefix + apiUrl + controllerName + "/view",
                method: 'GET',
            },
            'getAll': {
                url: prefix + apiUrl + controllerName + "/index",
                method: 'GET',
            },
            'update': {
                url: prefix + apiUrl + controllerName + "/update",
                method: 'PUT',
            },
            'remove': {
                url: prefix + apiUrl + controllerName + "/delete",
                method: 'DELETE',
            },
            'create': {
                url: prefix + apiUrl + controllerName + "/create",
                method: 'POST',
            }
        };
		
		if( typeof custom != 'undefined'){		
			if( typeof defaultRest == 'undefined' ){
				defaultRest = {};
			}
			
			for (var k in custom) {				
				defaultRest[k] = {
					url : custom[k].url,
					method : custom[k].method
				} 
				
			}
		}
        return $resource("", {}, defaultRest);		
    }

    var uploadFileHandler = function (apiUrl,link, file, fn) {

        var DEFAULT_MESSAGE_FAIL_UPLOAD = "File gagal di upload";

        if (typeof file === 'undefined') {
            fn(false, "", DEFAULT_MESSAGE_FAIL_UPLOAD)
        }

        var formData = new FormData();
        formData.append('file', file);


        return $http.post(apiUrl + link, formData, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (response) {
            console.log(response);
            if (response.status === 1) {
                fn(true, response.name);
            } else {
                fn(false, null, DEFAULT_MESSAGE_FAIL_UPLOAD);
            }

        })
    }

    service.rest = getRestResponse;
    service.uploadFile = uploadFileHandler;

    return service;

})

app.factory('DataAttributFactory', function () {
    var service = {};

    var removeData = function (errorData) {
        if (typeof errorData !== 'undefined')
            delete errorData;
    }

    var trimData = function (data) {

        if (typeof data === 'undefined')
            return {};

        for (var k in data) {
            if (typeof data[k] === 'string')
                if (data[k].trim() == "") {
                    delete data[k];
                }
            if (typeof data[k] === 'array') {
                for (var i = 0; i < data[k].length; i++) {
                    if (data[k][i].trim() == "") {
                        delete data[k][i];
                    }
                }
            }

        }
        return data;
    }
    
    service.remove = removeData;
    service.trim = trimData;
	
	var isUndefined = function (data) {
        return typeof data == 'undefined';
    }
	
    service.isUndefined = isUndefined;

    return service;
})


app.factory("ERROR", function () {

    var service = {};

    var collect = function (errors) {
        for (var k in errors) {
            if (typeof errors[k] === 'object' && errors[k].length > 0)
                errors[k] = errors[k][0];
        }
        return errors;
    }

    service.collect = collect;

    return service;
})

app.factory('ErrorHandlerFactory', function () {

    var service = {};


    var errorData = function (data) {
        var error = {};
        for (var i = 0; i < data.baseResponse.data.length; i++) {
            error[data.baseResponse.data[i].field] = data.baseResponse.data[i].defaultMessage;
        }
        return error;
    }

    var defaultResponseHandler = function (response, fn) {
        isSuccess(response, function (isSuccess, data) {
            fn(isSuccess, data);
        });
    }

    var isSuccess = function (data, fn) {
        var dataResponse = data.baseResponse;


        if (typeof dataResponse === 'undefined') {
            fn(false, null);
            return;
        }


        if (dataResponse.error === false) {
            if (typeof dataResponse.data != 'undefined') {
                fn(true, dataResponse.data);
            } else {
                fn(true);
            }

        } else {
            if (typeof dataResponse.data != 'undefined') {
                fn(false, errorData(data));
            } else {
                fn(false);
            }
        }
    }

    service.getErrorData = errorData;
    service.responseHandler = isSuccess;
    service.defaultResponseHandler = defaultResponseHandler;

    return service;

})

app.factory('ParserFactory', function () {
    return {
        parse: function (data) {
            var dataParse = data.baseResponse;
            return {
                data: dataParse.data,
                msg: dataParse.msg,
                isError: dataParse.error
            }
        }
    };
})

