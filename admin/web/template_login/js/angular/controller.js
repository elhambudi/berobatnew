var app = angular.module('app');
app.controller('LoginAdminController', function ($scope, $resource, API_URL) {

	var resource = $resource("", {}, {
		'login': {
			url: API_URL + "login/admin",
			method: 'POST',
		},
		'loginHandler': {
			url: ADMIN_BASE_URL + "login/handler",
			method: 'POST'
		}
	});

	$scope.submit = function (form) {

		resource.login({}, form, function (response) {
			delete $scope.error;
			console.log(response);
			if (response.status == 1) {
				resource.loginHandler({}, response.data, function (response) {
					if (response.status == 1) {
						$scope.success = true;
						window.location.href=ADMIN_BASE_URL+"site";
					} else {
						$scope.error = "Invalid Data";
					}
				})
			} else {
				var data = response.errors;
				if (data.length > 0) {
					$scope.error = data[0];
				}
			}
		});
	}

})