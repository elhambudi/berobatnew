<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\Voucher;
use common\models\VoucherHasOffice;
use common\models\Office;
use common\models\Clinic;
use common\models\Category;
use common\models\CategoryHasClinic;
use common\models\OrderVoucher;
use common\models\Employee;

class VoucherController extends \yii\web\Controller
{
    public function actionIndex()
    {
		/*$employee = Employee :: find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
		foreach($employee as $e){
			$id_office = Office::find()->where(['id'=>$e['office_id']])->all();
			foreach($id_office as $o){
				$id_clinic = $o['clinic_id'];
			}
		}
		//var_dump($id_clinic);die();
		$listvoucher = Voucher :: find()->where(['clinic_id'=>$id_clinic])->all();
		foreach($listvoucher as $list){
			$id_voucher[] = $list['id'];
		}*/
		//$voucher = OrderVoucher :: find()->where(['in','voucher_id' ,$id_voucher])->all();
		$voucher = OrderVoucher :: find()->orderBy(['id'=>SORT_DESC])->all();
		//$office = Office :: find()->where()->all();
		//var_dump($voucher);die();
        return $this->render('index',[
			'voucher'=>$voucher,
		]);
    }
	public function actionSearch()
    {
		$postData= Yii::$app->request->post();
		//var_dump($postData['tanggal1']);die();
		$voucher = OrderVoucher::find()->where(['between', 'created_date', $postData['tanggal1'], $postData['tanggal2'] ])->all();
		//var_dump($voucher);die();
		//$voucher = OrderVoucher :: find()->orderBy(['id'=>SORT_DESC])->all();
        return $this->render('index',[
			'voucher'=>$voucher,
		]);
    }
	public function actionProof($id)
    {
		$voucher = OrderVoucher :: findOne($id);
	    return $this->render('lihat_bukti',[
			'model'=>$voucher,
		]);
    }
	
	public function actionValidate($id_voucher){
		$model = OrderVoucher :: findOne($id_voucher);
		$model->status_validate =1;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher has been Validated.');
		return $this->redirect(['voucher/index']);
	}
	

	public function actionUnvalidate($id_voucher){
		$model = OrderVoucher :: findOne($id_voucher);
		$model->status_validate =0;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher not been Validated.');
		return $this->redirect(['voucher/index']);
	}
	
	public function actionPaid($id_voucher){
		//generate code
		$seed = str_split('abcdefghijklmnopqrstuvwxyz'
                     .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
		shuffle($seed); // probably optional since array_is randomized; this may be redundant
		$rand = '';
		foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
		$model = OrderVoucher :: findOne($id_voucher);
		$model->code =$rand;
		$model->status =1;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher has been Paid.');
		return $this->redirect(['voucher/index']);
	}
	
	public function actionUnpaid($id_voucher){
		$model = OrderVoucher :: findOne($id_voucher);
		$model->status =0;
		$model->code =NULL;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher has been not Paid.');
		return $this->redirect(['voucher/index']);
	}

}	
