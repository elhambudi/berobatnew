<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\Voucher;
use common\models\VoucherHasOffice;
use common\models\Office;
use common\models\Clinic;
use common\models\Category;
use common\models\CategoryHasClinic;
use common\models\OrderVoucher;
use common\models\Employee;
use common\models\User;

class MemberController extends \yii\web\Controller
{
    public function actionIndex()
    {
		$member = User :: find()->where(['role_id'=>5])->all();
        return $this->render('index',[
			'member'=>$member,
		]);
    }
	
	public function actionActivate($id){
		$model = User :: findOne($id);
		$model->status =1;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Member has been Activated.');
		return $this->redirect(['member/index']);
	}

	public function actionUnactivate($id){
		$model = User :: findOne($id);
		$model->status =0;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Member not been Activated.');
		return $this->redirect(['member/index']);
	}
	
	public function actionPaid($id_voucher){
		$model = OrderVoucher :: findOne($id_voucher);
		$model->status =1;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher has been Paid.');
		return $this->redirect(['voucher/index']);
	}
	
	public function actionUnpaid($id_voucher){
		$model = OrderVoucher :: findOne($id_voucher);
		$model->status =0;
		$model->save();
		
		if(!$model->save()){
			var_dump($model->errors);
			die();
		}
		Yii::$app->session->setFlash('success', 'Voucher has been not Paid.');
		return $this->redirect(['voucher/index']);
	}

}	
