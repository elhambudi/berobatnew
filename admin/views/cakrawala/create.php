<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\file\FileInput;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\search\BeaconSearch $searchModel
 */
$this->title = 'Tambah Data Buku';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Cakrawala
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Cakrawala</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Cakrawala</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['cakrawala/submit']),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
			  <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Judul</label>
                  <?= $form->field($model,'judul')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
			  <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Nama</label>
                  <?= $form->field($model,'nama')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
			  <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Keterangan</label>
                  <?= $form->field($model,'keterangan')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
			  <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Deskripsi</label>
				  <?= $form->field($model, 'deskripsi')->widget(\yii\redactor\widgets\Redactor::className())->label(false) ?>
                </div>
              </div>
			  <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">File</label>
                  <input type="file" name="Cakrawala[file]" accept="application/msword, application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                </div>
              </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</button>
				  <a href="<?= Url::to(["cakrawala/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->