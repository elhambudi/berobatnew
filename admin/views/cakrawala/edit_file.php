<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\file\FileInput;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\search\BeaconSearch $searchModel
 */
$this->title = 'Edit Data Buku';

//var_dump($model);die();
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Cakrawala
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Cakrawala</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Cakrawala</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['cakrawala/edit','id'=>$model->id]),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
			 <div class="box-body">
				<div class="form-group">
                  <label for="idkategori">ID Cakrawala</label>
                  <input type="text" class="form-control" value="<?= $model->id?>" readonly>
                </div>
            </div>
			<div class="box-body">
                <div class="form-group">
                  <label for="namakategori">File</label>
                  <input type="file" name="Cakrawala[file]" accept="application/msword, application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                </div>
              </div>
			  <?= $form->field($model,'judul')->hiddenInput()->label(false)?>
			  <?= $form->field($model,'deskripsi')->hiddenInput()->label(false)?>
			  <div class="box-footer">
                <button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</button>
				  <a href="<?= Url::to(["cakrawala/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->