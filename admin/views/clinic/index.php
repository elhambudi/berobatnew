<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Data Clinic';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Clinic
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Clinic</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <!--<a href="<?= Url::to(['clinic/create'])?>" class="btn btn-flat " ><span class="fa fa-plus"></span> Tambah Clinic
			  </a>-->
            </div>
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Name</th>
				  <th>Kategori</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
				<?php
					$i = 1;
					foreach ($model as $row) {
				?>
                <tr>
                  <td><?= $i++ ?></td>
                  <td><?= $row['name_clinic'] ?></td>
                  <td><?php
						$listClinics = \common\models\CategoryHasClinic::find()->where(['clinic_id'=>$row['id']])->all();
						$listOfCategory = [];
						foreach($listClinics as $v){
							$listOfCategory[] = $v->category->name;
						}
						echo implode(",",$listOfCategory);
				  ?></td>
                  
				  <td>
				   <a href="<?= Url::to(['clinic/detail','id'=>$row['id']])?>" type="button" class="btn active btn-default"><span class="fa fa-pencil"></span> Detail Clinic</a>
				   <a href="<?= Url::to(['clinic/list','id'=>$row['id']])?>" type="button" class="btn active btn-default"><span class="fa fa-image"></span> List Employee</a>
				  </td>
                </tr>
				<?php
					}
				?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->