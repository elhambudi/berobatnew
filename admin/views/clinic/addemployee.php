<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\models\Constant;
use fedemotta\datatables\DataTables;

$this->title = "Add Employee";
?>

<div class="box box-default">
	<div class="box-header with-border">
		<span class="icon-title"><i class="fa fa-hospital-o"></i></span>
        <h3 class="box-title"><?= $this->title ?></h3>
		<div class="pull-right">
        	<a href="<?=Url::to(['clinic/list','id'=>$id])?>" class="btn btn-default"><i class="fa fa-angle-left"></i> Back</a>
        </div>
    </div>
	<div class="box-body">
		<?php $form = ActiveForm::begin([
				'action' => Url::to(['clinic/createemployee','id'=>$id]),
                'method' => 'POST'
				]); ?>
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="fa fa-group"></i></span>
				<input type="text" name="Employee[name]" class="form-control" placeholder="Nama" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="fa fa-phone"></i></span>
				<input type="text" name="Employee[phone]" class="form-control" placeholder="Telephone" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
				<input type="text" name="User[email]" class="form-control" placeholder="Email" aria-describedby="basic-addon1">
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="fa fa-building"></i></span>
				<select name="Employee[office_id]" class="form-control" ng-model="form.category_id">
					<?php foreach($office as $o){?>
						<option value="<?=$o['id']?>"><?= $o['name']?> - <?= $o['address']?></option>
					<?php 
						}
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<button type="submit" class="btn btn-primary btn-submit">Simpan</button>	
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
	<!-- /.box-body -->
</div>