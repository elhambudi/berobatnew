<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\file\FileInput;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\search\BeaconSearch $searchModel
 */
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Lihat Bukti Transfer
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Lihat Bukti Transfer</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Lihat Bukti Transfer</h3>
            </div>
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['berita/editgambar','id'=>$model->id]),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
              <div class="box-body">
                <div class="form-group">
					<img src="<?= Yii::getAlias('@uploadsWeb') ?>/<?=  $model->image ?>" height="50%">
                </div>
              </div>
			  <div class="box-footer">
				<a href="<?= Url::to(["voucher/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Back</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->