<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\date\DatePicker;
/* @var $this yii\web\View */

$this->title = 'Data Clinic';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Order Voucher
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Order Voucher</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box box-info">
            <div class="box-header">
              <i class="fa fa-search"></i>

              <h3 class="box-title">Quick Search</h3>
            </div>
            <div class="box-body">
              <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['voucher/search']),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
				]);  ?>
                <div class="form-group">
					<div class="col-sm-3">
						<?php
							echo DatePicker::widget([
								'name' => 'tanggal1',
								'options' => [
									'placeholder' => 'Dari Tanggal ...',
									'required'=>true,
								],
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'yyyy-mm-dd'
								]
							]);
						?>
					</div>
					<div class="col-sm-3">
						<?php
							echo DatePicker::widget([
								'name' => 'tanggal2',
								'options' => [
									'placeholder' => 'Sampai Tanggal ...',
									'required'=>true,
								],
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'yyyy-mm-dd'
								]
							]);
						?>
					</div>
					<div class="col-sm-3">
						<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
					</div>
                </div>
              <?php yii\widgets\ActiveForm::end(); ?>
            </div>
          </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Name</th>
				  <th>Code</th>
                  <th>Status Validate</th>
                  <th>Status Bayar</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
					<?php 
						$i=1;
						foreach($voucher as $v){
					?>
					<tr>
						<th><?=$i++?></th>
						<th><?php
								$voucher = \common\models\Voucher::findOne($v['voucher_id']);
								echo $voucher->name;
						?></th>
						<th><?=$v['code']?></th>
						<th><?php
							if($v['status_validate']==0){
								echo '<span class="label label-danger">Not Validate</span>';
							}else{
								echo '<span class="label label-success">Validated</span>';
							}?></th>
						<th><?php
							if($v['status']==0){
								echo '<span class="label label-danger">Not Paid</span>';
							}else{
								echo '<span class="label label-success">Paid</span>';
							}?></th>
						<td>
							<?php
								if($v['status']==0 && $v['status_validate']==0){
							?>
						   <a href="<?=Url::to(['voucher/paid','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-check"></span> Paid</a>
						   <a href="<?=Url::to(['voucher/validate','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-check"></span> Validate</a>
						   <?php
								}elseif($v['status']==1 && $v['status_validate']==0 ){
						   ?>
						   <a href="<?=Url::to(['voucher/unpaid','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-danger"><span class="fa fa-check"></span> Not Paid</a>
						   <a href="<?=Url::to(['voucher/validate','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-check"></span> Validate</a>
						   <?php
							}elseif($v['status_validate']==1 && $v['status']==1){
							?>
							<a href="<?=Url::to(['voucher/unpaid','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-danger"><span class="fa fa-check"></span> Not Paid</a>
						   <a href="<?=Url::to(['voucher/unvalidate','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-danger"><span class="fa fa-check"></span> Unvalidate</a>
						   <?php
								}elseif($v['status']==0 && $v['status_validate']==1){
						   ?>
						   <a href="<?=Url::to(['voucher/paid','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-check"></span> Paid</a>
						   <a href="<?=Url::to(['voucher/unvalidate','id_voucher'=>$v['id']]);?>" type="button" class="btn active btn-danger"><span class="fa fa-check"></span> Unvalidate</a>
						   <?php
							}
						   ?>
						   	<?php
								if($v['image']!=NULL){?>
							<a href="<?=Url::to(['voucher/proof','id'=>$v['id']]);?>" type="button" class="btn active btn-warning"><span class="fa fa-money"></span> Lihat Bukti Transfer</a>
							<?php
								}
							?>
						</td>
					</tr>
					<?php
						}
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->