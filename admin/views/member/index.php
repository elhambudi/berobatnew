<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Data Clinic';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Member
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Member</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Name</th>
				  <th>Email</th>
                  <th>Status</th>
                  <!--<th>Action</th>-->
                </tr>
                </thead>
                <tbody>
					<?php 
						$i=1;
						foreach($member as $v){
					?>
					<tr>
						<th><?=$i++?></th>
						<th><?=$v['username']?></th>
						<th><?=$v['email']?></th>
						<th><?php
							if($v['status']==0){
								echo '<span class="label label-danger">Not Activate</span>';
							}else{
								echo '<span class="label label-success">Activate</span>';
							}?></th>
						<!--<td>
							<?php
								if($v['status']==0){
							?>
						   <a href="<?=Url::to(['member/activate','id'=>$v['id']]);?>" type="button" class="btn active btn-default"><span class="fa fa-check"></span> Activate</a>
						   <?php
								}elseif($v['status']==1 ){
						   ?>
						   <a href="<?=Url::to(['member/unactivate','id'=>$v['id']]);?>" type="button" class="btn active btn-danger"><span class="fa fa-check"></span> Not Activate</a>
						   <?php
								}
							?>
						</td>-->
					</tr>
					<?php
						}
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->