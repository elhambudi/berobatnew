<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Constant;
use fedemotta\datatables\DataTables;
$this->title = "Detail Clinic";
?>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Clinic
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Clinic</li>
      </ol>
    </section>
	
	<section class="content">
      <div class="row">
        <div class="col-xs-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<span class="icon-title"><i class="fa fa-hospital-o"></i></span>
					<h3 class="box-title"><?= $this->title ?></h3>
					<div class="pull-right">
						<?= Html::a( "<i class='fa fa-angle-left'></i> Back", ['/clinic'], ['class'=>"btn-back"]) ?>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-4">
							<div class="row">
								<div class="col-md-12 col-sm-6">
									<?php
										$clinic = common\models\ClinicPhoto::find()->where(['clinic_id'=>$data['id']])->all();
										foreach($clinic as $c){
									?>
									<div class="detail-klinik__logo"><img class="profile-user-img img-responsive img-circle" src="<?= Url::to('@picfrontend/clinic_photo/'.$c['url']); ?>" height="100px"></div>
									<?php
										}
									?>
								</div>
								<div class="col-md-12 col-sm-6">
									<?php
										if($data['status_approval']==Constant::STATUS_NONACTIVE){
									?>
										<a href="<?=Url::to(['clinic/approve','id'=>$data['id']]);?>" class="btn btn-success btn-block btn-pad">Aktifkan Klinik</a>
									<?php
										}else{
									?>
										<a href="<?=Url::to(['clinic/nonapprove','id'=>$data['id']]);?>" class="btn btn-danger btn-block btn-pad">Nonaktifkan Klinik</a>
									<?php
										}
									?>
									<button data-toggle="modal" data-target="#voucherModal" class="btn btn-default btn-block btn-pad"><i class="fa fa-ticket"></i> Lihat vouchernya</button>
									<div class="border-space-15"></div>
									<br>
									<a id="remove-clinic" href="<?= Url::to(['clinic/remove','id' => $data['id']]) ?>"
									class="btn btn-danger btn-block btn-pad">Hapus Permanen</a>
								</div>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="detail-klinik__name">
								<?=$data['name_clinic']?> 
								<?php
									if($data['status_approval']==Constant::STATUS_NONACTIVE){
										echo '<span class="detail-klinik__status label label-danger">Not Approval</span><br>';
									}else{
										echo '<span class="detail-klinik__status label label-success">Approval</span><br>';
									}
								?>
							</div>
							<div class="detail-klinik__category">
								<strong>Categories</strong> : <?php 
									$code = common\models\CategoryHasClinic::find()->where(['clinic_id'=>$data['id']])->all();
									foreach($code as $co){
										$code1 = common\models\Category::find()->where(['id'=>$co['category_id']])->all();
										foreach($code1 as $c){
											echo '<span class="detail-klinik__status label label-primary">'.$c['name'].'</span> ';
										}
									}
								?>
							</div>
							<div class="detail-klinik__desc">
								<strong>Deskripsi Detail : <br /></strong>
								<?=$data['description']?> 
							</div>
							<div class="detail-klinik__proof">
								<strong>Bukti Izin Menjalankan Klinik : <br /></strong>
								<a href="<?= Yii::getAlias('@uploadsWeb')."/".$data['proof_of_permission'] ?>">Bukti</a>
							</div>				
							<div class="detail-klinik__owner">
								<table>
									<tr>
										<th width="50">Owner</th>
										<td width="10">:</td>
										<td><?=$data['name_contact']?></td>
									</tr>
									<tr>
										<th>Email</th>
										<td>:</td>
										<td><?php 
											$code = common\models\User::find()->where(['id'=>$data['user_id']])->all();
											foreach($code as $co){
												echo $co['email'];
											}
										?></td>
									</tr>
									<tr>
										<th>Phone</th>
										<td>:</td>
										<td><?=$data['phone']?></td>
									</tr>
								</table>
							</div>
							<div class="detail-klinik__office">
								<div class="the-title">Office : </div>
								<ul>
									<?php 
										$code = common\models\Office::find()->where(['clinic_id'=>$data['id']])->all();
										foreach($code as $co){
											$code1 = common\models\City::find()->where(['id'=>$co['city_id']])->all();
											foreach($code1 as $c){
												echo '<li>
														<div class="the-mark"><i class="fa fa-map-marker"></i>
														<span>'.$co['address'].'</span><br>
															<span>'.$c['name'].'</span><br>
														</div>
													<div class="clear"></div>
												</li>';
											}
										}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="voucherModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">List Voucher</h4>
      </div>
      <div class="modal-body">
	  		<?= DataTables::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [
					['class' => 'yii\grid\SerialColumn'],
					'name',
					[
						'label' => 'Harga Voucher',
						'value' => function($model){
							return "Rp.".number_format($model->voucher_price);
						}
					],
					'stock',					
					[
						'label' => 'Expire Date',
						'value' => function($model){
							$date = strtotime($model->expire_date); 
							return date( 'd-m-Y', $date );
						}
					]
				],
			]);?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>