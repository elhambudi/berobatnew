<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\file\FileInput;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\search\BeaconSearch $searchModel
 */
$this->title = 'Edit Data Buku';

//var_dump($model);die();
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Bank Admin
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Bank Admin</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Bank Admin</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['bank/edit','id'=>$model->id]),
                'method' => 'POST',
				'options' => ['enctype' => 'multipart/form-data'],
			 ]);  ?>
			 <div class="box-body">
				<div class="form-group">
                  <label for="idkategori">ID Bank Admin</label>
                  <input type="text" class="form-control" value="<?= $model->id?>" readonly>
                </div>
            </div>
			<div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Bank</label>
                  <?= $form->field($model,'bank')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
			<div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Atas Nama</label>
                  <?= $form->field($model,'name')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
			<div class="box-body">
                <div class="form-group">
                  <label for="namakategori">No Rekening</label>
                  <?= $form->field($model,'no_rekening')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i> Edit</button>
				<a href="<?= Url::to(["bank/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->