<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Data Buku';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Bank Admin
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Bank Admin</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <a href="<?= Url::to(['bank/create'])?>" class="btn btn-flat " ><span class="fa fa-plus"></span> Tambah Bank Admin
			  </a>
            </div>
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Nama Bank</th>
                  <th>No Rekening</th>
                  <th>Atas Nama</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
				<?php
					$i = 1;
					foreach ($model as $row) {
				?>
                <tr>
                  <td><?= $i++ ?></td>
                  <td><?= $row['bank'] ?></td>
                  <td><?= $row['no_rekening'] ?></td>
                  <td><?= $row['name'] ?></td>
                  
				  <td>
				   <a href="<?= Url::to(['bank/editform','id'=>$row['id']])?>" type="button" class="btn active btn-warning"><span class="fa fa-pencil"></span> Edit Teks</a>
				   <?= Html::a('<i class="fa fa-trash"> Hapus</i>',
					  ['bank/delete', 'id'=>$row['id']],
					  [
						  'class'=>'btn btn-danger',
						  'data-confirm' => Yii::t('yii', 'Anda yakin mau menghapus data ini?'),
					  ]);
					?>
				  </td>
                </tr>
				<?php
					}
				?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->