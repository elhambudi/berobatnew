<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';
$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<br><br><br>
<div class="login-box" > 
    <div class="login-logo">
        <div class="row">
            <div class="col-sm-5">
                <div class="the-logo">
                    <img src="<?= Yii::$app->urlManager->createUrl(["/template_login/images/logo.png"]); ?>">
                </div>
            </div>
            <div class="col-sm-7">
                <div class="the-desc">
                    <div class="the-desc__top">SIGN IN TO ADMIN</div>
                    <div class="the-desc__bottom">Sign in to start your session</div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
		<?php $form = ActiveForm::begin([
				'action' => Url::to(['site/login']),
                'method' => 'POST'
		]); ?>
            <div class="form-group has-feedback">
				<?= $form->field($model, 'username')->textInput([
					'autofocus' => true,
					'class' => 'form-control',
					'placeholder' => 'Email. Contoh: name@example.com',
					'required' => true
				])->label(false) ?>
                <span class='glyphicon glyphicon-envelope form-control-feedback'></span>
            </div>

            <div class='form-group has-feedback'>
                <?= $form->field($model, 'password')->passwordInput([
					'autofocus' => true,
					'class' => 'form-control',
					'placeholder' => 'Masukkan password',
					'required' => true
				])->label(false) ?>
                <span class='glyphicon glyphicon-lock form-control-feedback'></span>
            </div>
            <div class="row">
                <div class="col-xs-12   ">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" 
                    ng-disabled="objForm.$invalid">
                        <span>LOG IN</span>
                    </button>
                </div>
                <!-- /.col -->
            </div>
            <div class="text-center add-ons-link-help">
                <a href="#">Saya lupa password saya</a>
            </div>
        <?php ActiveForm::end(); ?>       
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
