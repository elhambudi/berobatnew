<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Data Buku';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data  Kategori Clinic
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data  Kategori Clinic</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <a href="<?= Url::to(['kategoriclinic/create'])?>" class="btn btn-flat " ><span class="fa fa-plus"></span> Tambah  Kategori Clinic
			  </a>
            </div>
			<div class="box-body">
				<?php foreach (Yii::$app->session->getAllFlashes() as $key => $message)
							echo '<div class="alert alert-' . $key . ' ">' . $message . '</div>';
					?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
				  <th>Nama Kategori</th>
                  <th>Gambar</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
				<?php
					$i = 1;
					foreach ($model as $row) {
				?>
                <tr>
                  <td><?= $i++ ?></td>
                  <td><?= $row['name'] ?></td>
                  <td><img src="<?= Url::to('@rootWeb/site/web/category_photo/'.$row['image']); ?>" height="100px"></td>
                  
				  <td>
				   <a href="<?= Url::to(['kategoriclinic/editform','id'=>$row['id']])?>" type="button" class="btn active btn-warning"><span class="fa fa-edit"></span> Edit</a>
				   <?= Html::a('<i class="fa fa-trash"> Hapus</i>',
					  ['kategoriclinic/delete', 'id'=>$row['id']],
					  [
						  'class'=>'btn btn-danger',
						  'data-confirm' => Yii::t('yii', 'Anda yakin mau menghapus data ini?'),
					  ]);
					?>
				  </td>
                </tr>
				<?php
					}
				?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->