<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\date\DatePicker;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\search\BeaconSearch $searchModel
 */
$this->title = 'Edit Data Anggota';
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Edit
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tabel</a></li>
        <li class="active">Data Anggota</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Peminjaman</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = yii\widgets\ActiveForm::begin([
                'action' => Url::to(['peminjaman/edit','id'=>$model->id_peminjaman]),
                'method' => 'POST'
			 ]);  ?>
			<div class="box-body">
				<div class="form-group">
                  <label for="idkategori">ID Peminjaman</label>
                  <input type="text" class="form-control" value="<?= $model->id_peminjaman?>" readonly>
                </div>
            </div>
			<div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Nama Anggota</label>
				  <select class="form-control" name="Peminjaman[id_anggota]">
					<?php 
						foreach($anggota as $data){	
					?>
						<option value="<?= $data['id_anggota']?>"><?= $data['nama_anggota']?></option>
					<?php
						}
					?>
					</select>
                </div>
            </div>
			<div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Nama Buku</label>
                   <select class="form-control" name="Peminjaman[id_buku]">
					<?php 
						foreach($buku as $data){	
					?>
						<option value="<?= $data['id']?>"><?= $data['nama_buku']?></option>
					<?php
						}
					?>
					</select>
                </div>
              </div> 
			  <div class="box-body">
                <div class="form-group">
                  <!--<label for="namakategori">Tanggal Lahir</label>-->
                  <?= $form->field($model, 'tanggal_pinjam')->widget(DatePicker::classname(), [
						'readonly'=>true,
						'pluginOptions' => [
							'autoclose'=>true,
							'format' => 'yyyy/mm/dd'
						]
					]);?>
					
                </div>
              </div> 
			  <div class="box-body">
                <div class="form-group">
                  <!--<label for="namakategori">Tanggal Lahir</label>-->
                  <?= $form->field($model, 'tanggal_harus_kembali')->widget(DatePicker::classname(), [
						'readonly'=>true,
						'pluginOptions' => [
							'autoclose'=>true,
							'format' => 'yyyy/mm/dd'
						]
					]);?>
					
                </div>
              </div>
			  <div class="box-body">
                <div class="form-group">
                  <label for="namakategori">Keterangan</label>
                  <?= $form->field($model,'keterangan')->textInput([
							'class' => 'form-control col-md-7 col-xs-12'
						])->label(false)?>
                </div>
              </div>
			  <div class="box-footer">
                <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
				  <a href="<?= Url::to(["peminjaman/index"]); ?>" type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
              </div>

			<?php yii\widgets\ActiveForm::end(); ?>
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->