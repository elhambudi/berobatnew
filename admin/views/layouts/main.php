<?php

//$segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Berobat</title>
    <?php $this->head() ?>

</head>
<body class="hold-transition skin-green layout-boxed sidebar-mini">
<?php $this->beginBody() ?>
   <div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>B</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Berobat</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?= Url::to(['site/logout'])?>" data-method="post">
			  <span class="fa fa-sign-out"> Logout</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= Yii::$app->urlManager->createUrl(["/templates/dist/img/user8-128x128.jpg"]); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
	  
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
		<?php if(Yii::$app->controller->id == 'site'  && Yii::$app->controller->action->id == 'index'){?>
			<li class="active"><a href="<?= Url::to(['site/index'])?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
		<?php }else{ ?>
			<li><a href="<?= Url::to(['site/index'])?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
		<?php } ?>
		
		<li><a href="<?= Url::to(['clinic/index'])?>"><i class="fa fa-database"></i> <span>Data Clinic</span></a></li>
		<li><a href="<?= Url::to(['member/index'])?>"><i class="fa fa-database"></i> <span>Data Member</span></a></li>
		<li><a href="<?= Url::to(['voucher/index'])?>"><i class="fa fa-database"></i> <span>Data Voucher</span></a></li>
		<li><a href="<?= Url::to(['bank/index'])?>"><i class="fa fa-database"></i> <span>Data Bank Admin</span></a></li>
		<li><a href="<?= Url::to(['kategoriclinic/index'])?>"><i class="fa fa-database"></i> <span>Data Kategori Clinic</span></a></li>
		
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

        <?= $content; ?>

	<footer class="main-footer">
		<div class="pull-right hidden-xs">
		  <b>Version</b> 2.3.7
		</div>
		<strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.Admin LTE</strong> All rights
		reserved.
	  </footer>

	</div>
<?php $this->endBody() ?>
    <!-- this page specific inline scripts -->
	
		<script>
	  $(function () {
		$("#example1").DataTable();
		$('#example2').DataTable({
		  "paging": true,
		  "lengthChange": false,
		  "searching": false,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});
	  });
	  $(function(){
			$(".datepicker").datepicker({
				dateFormat:"yy-mm-dd"
			});
			$(".datepicker2").datepicker({
				dateFormat:"yy-mm-dd"
			});
			$(".datepicker3").datepicker({
				dateFormat:"yy-mm-dd"
			});
		});
	</script>
</body>
</html>
<?php $this->endPage() ?>
